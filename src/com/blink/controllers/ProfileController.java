package com.blink.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.Area;
import com.blink.entity.Counselor;
import com.blink.entity.Equipment;
import com.blink.entity.PrivateMessage;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.User;
import com.blink.managers.AreaManager;
import com.blink.managers.CounselorsManager;
import com.blink.managers.EquipmentAndCarsManager;
import com.blink.managers.ProjectManager;
import com.blink.managers.PersonalMessagesManager;
import com.blink.managers.ProjectDetailsManager;
import com.blink.managers.UserManager;
import com.blink.utils.HomePageData;
import com.blink.utils.Utils;

@Controller
public class ProfileController {

	@RequestMapping(value = "/personalProjects.action", method = RequestMethod.GET)
	public ModelAndView showPersonalProjects(HttpServletRequest request) {

		ModelAndView mav = new ModelAndView("personalProjects");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				mav.addObject("isAuthenticated", isAuthenticated);
				mav.addObject("userId", userId);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);

				List<ProjectPermissions> projectPermissionsList = ProjectDetailsManager
						.getInstance().getProjectsPermissions(userId);
				mav.addObject("projectPermissionsList", projectPermissionsList);
			}
		}

		return mav;
	}

	@RequestMapping(value = "/personalEquipments.action", method = RequestMethod.GET)
	public ModelAndView showPersonalEquipments(HttpServletRequest request) {

		ModelAndView mav = new ModelAndView("personalEquipments");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				mav.addObject("isAuthenticated", isAuthenticated);
				mav.addObject("userId", userId);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);

				List<Equipment> equipmentList = EquipmentAndCarsManager
						.getInstance().getEquipmentsByUserId(userId);
				mav.addObject("equipmentList", equipmentList);
			}
		}

		return mav;
	}

	@RequestMapping(value = "/personalAreas.action", method = RequestMethod.GET)
	public ModelAndView showPersonalAreas(HttpServletRequest request) {

		ModelAndView mav = new ModelAndView("personalAreas");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				mav.addObject("isAuthenticated", isAuthenticated);
				mav.addObject("userId", userId);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);

				List<Area> areasList = AreaManager.getInstance()
						.getAreasByUserId(userId);
				mav.addObject("areasList", areasList);

			}
		}

		return mav;
	}

	@RequestMapping(value = "/personalCounselors.action", method = RequestMethod.GET)
	public ModelAndView showPersonalCounselors(HttpServletRequest request) {

		ModelAndView mav = new ModelAndView("personalCounselors");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				mav.addObject("isAuthenticated", isAuthenticated);
				mav.addObject("userId", userId);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);

				List<Counselor> counselorsList = CounselorsManager
						.getInstance().getCounselorsByUserId(userId);
				mav.addObject("counselorsList", counselorsList);
			}
		}

		return mav;
	}

	@RequestMapping(value = "/inbox.action", method = RequestMethod.GET)
	public ModelAndView showInbox(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		ModelAndView mav = new ModelAndView("inbox");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				mav.addObject("isAuthenticated", isAuthenticated);
				mav.addObject("userId", userId);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
			}
			int userId = Utils.getUserIdByHash(cookies);
			mav.addObject("userId", userId);
			List<PrivateMessage> privateMessagesList = PersonalMessagesManager
					.getInstance().getPrivateMessagesList(userId);
			mav.addObject("privateMessagesList", privateMessagesList);
		}

		String counselorAgreeText = request.getParameter("counselorAgree");
		int counselorAgree = -1;
		try {
			counselorAgree = Integer.valueOf(counselorAgreeText);
		} catch (NumberFormatException e) {
		}
		if (counselorAgree > 0) {
			PersonalMessagesManager.getInstance()
					.counselorAgree(counselorAgree);
			response.sendRedirect("inbox.action");
		}

		String areaAgreeText = request.getParameter("areaAgree");
		int areaAgree = -1;
		try {
			areaAgree = Integer.valueOf(areaAgreeText);
		} catch (NumberFormatException e) {
		}
		if (areaAgree > 0) {
			PersonalMessagesManager.getInstance().areaAgree(areaAgree);
			response.sendRedirect("inbox.action");
		}

		String equipmentAgreeText = request.getParameter("equipmentAgree");
		int equipmentAgree = -1;
		try {
			equipmentAgree = Integer.valueOf(equipmentAgreeText);
		} catch (NumberFormatException e) {
		}
		if (equipmentAgree > 0) {
			PersonalMessagesManager.getInstance()
					.equipmentAgree(equipmentAgree);
			response.sendRedirect("inbox.action");
		}
		String counselorInviteText = request.getParameter("counselorInvite");
		int counselorInvite = -1;
		try {
			counselorInvite = Integer.valueOf(counselorInviteText);
		} catch (NumberFormatException e) {
		}
		if (counselorInvite > 0) {
			PersonalMessagesManager.getInstance().counselorInvite(
					counselorInvite);
			response.sendRedirect("inbox.action");
		}

		String areaInviteText = request.getParameter("areaInvite");
		int areaInvite = -1;
		try {
			areaInvite = Integer.valueOf(areaInviteText);
		} catch (NumberFormatException e) {
		}
		if (areaInvite > 0) {
			PersonalMessagesManager.getInstance().areaInvite(areaInvite);
			response.sendRedirect("inbox.action");
		}

		String equipmentInviteText = request.getParameter("equipmentInvite");
		int equipmentInvite = -1;
		try {
			equipmentInvite = Integer.valueOf(equipmentInviteText);
		} catch (NumberFormatException e) {
		}
		if (equipmentInvite > 0) {
			PersonalMessagesManager.getInstance().equipmentInvite(
					equipmentInvite);
			response.sendRedirect("inbox.action");
		}
		String userInviteText = request.getParameter("userInvite");
		int userInvite = -1;
		try {
			userInvite = Integer.valueOf(userInviteText);
		} catch (NumberFormatException e) {
		}
		if (userInvite > 0) {
			PersonalMessagesManager.getInstance().userInvite(userInvite);
			response.sendRedirect("inbox.action");
		}

		String deleteIdText = request.getParameter("deleteId");
		int deleteId = -1;
		try {
			deleteId = Integer.valueOf(deleteIdText);
		} catch (NumberFormatException e) {
		}
		if (deleteId > 0) {
			PersonalMessagesManager.getInstance().deleteMessage(deleteId);
			response.sendRedirect("inbox.action");
		}
		String privateDeleteIdText = request.getParameter("privateDeleteId");
		int privateDeleteId = -1;
		try {
			privateDeleteId = Integer.valueOf(privateDeleteIdText);
		} catch (NumberFormatException e) {
		}
		if (privateDeleteId > 0) {
			PersonalMessagesManager.getInstance().trashByReceiver(
					privateDeleteId);
			response.sendRedirect("inbox.action");
		}

		return mav;
	}

	@RequestMapping(value = "/outbox.action", method = RequestMethod.GET)
	public ModelAndView showOutbox(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		ModelAndView mav = new ModelAndView("outbox");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				mav.addObject("isAuthenticated", isAuthenticated);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
			}
			int userId = Utils.getUserIdByHash(cookies);
			mav.addObject("userId", userId);
			List<PrivateMessage> privateMessagesList = PersonalMessagesManager
					.getInstance().getOutBox(userId);
			mav.addObject("privateMessagesList", privateMessagesList);
		}

		String trashBySenderText = request.getParameter("trashBySender");
		int trashBySender = -1;
		try {
			trashBySender = Integer.valueOf(trashBySenderText);
		} catch (NumberFormatException e) {
		}
		if (trashBySender > 0) {
			PersonalMessagesManager.getInstance().trashBySender(trashBySender);
			response.sendRedirect("outbox.action");
		}

		return mav;
	}

	@RequestMapping(value = "/newPersonalMessage.action", method = RequestMethod.GET)
	public ModelAndView newPersonalMessageGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return writePersonalLetter(request, response);
	}

	@RequestMapping(value = "/newPersonalMessage.action", method = RequestMethod.POST)
	public ModelAndView newPersonalMessagePost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return writePersonalLetter(request, response);
	}

	ModelAndView writePersonalLetter(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		ModelAndView mav = new ModelAndView("newPersonalMessage");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userName", userName);
				mav.addObject("isAuthenticated", isAuthenticated);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				List<User> users = UserManager.getInstance().getUsersList();
				mav.addObject("users", users);
				String receiverUserIdText = request.getParameter("id");
				String message = request.getParameter("message");
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
				int receiverUserId;
				try {
					receiverUserId = Integer.valueOf(receiverUserIdText);
				} catch (NumberFormatException e) {
					return mav;
				}
				if (receiverUserId > 0) {
					mav.addObject("id", receiverUserId);
					if (message != null) {
						int senderUserId = Utils.getUserIdByHash(cookies);
						PrivateMessage privateMessage = new PrivateMessage();
						User sender = new User();
						User receiver = new User();
						sender.setId(senderUserId);
						receiver.setId(receiverUserId);
						privateMessage.setSender(sender);
						privateMessage.setReceiver(receiver);
						privateMessage.setMessage(message);
						String date = Utils.getShortDate();
						privateMessage.setType(2);
						privateMessage.setCreationDate(date);
						PersonalMessagesManager.getInstance().addNewMessage(
								privateMessage);

						response.sendRedirect("outbox.action");
						return mav;
					}
				}

			}

		}

		return mav;
	}

	@RequestMapping(value = "/deletedBox.action", method = RequestMethod.GET)
	public ModelAndView showDeletedBox(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		ModelAndView mav = new ModelAndView("deletedBox");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		int userId = -1;
		if (isAuthenticated) {
			if (userName != null) {
				userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				mav.addObject("isAuthenticated", isAuthenticated);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
			}
			mav.addObject("userId", userId);
		
		}

		String deletedBySenderText = request.getParameter("deletedBySender");
		int deletedBySender = -1;
		try {
			deletedBySender = Integer.valueOf(deletedBySenderText);
		} catch (NumberFormatException e) {
		}
		if (deletedBySender > 0) {
			PersonalMessagesManager.getInstance().deletedBySender(
					deletedBySender);
		}

		String deletedByReceiverText = request
				.getParameter("deletedByReceiver");
		int deletedByReceiver = -1;
		try {
			deletedByReceiver = Integer.valueOf(deletedByReceiverText);
		} catch (NumberFormatException e) {
		}
		if (deletedByReceiver > 0) {
			PersonalMessagesManager.getInstance().deletedByReceiver(
					deletedByReceiver);
		}
		List<PrivateMessage> privateMessagesList = PersonalMessagesManager
				.getInstance().getDeletedBox(userId);
		mav.addObject("privateMessagesList", privateMessagesList);

		return mav;
	}

	@RequestMapping(value = "/userRedact.action", method = RequestMethod.GET)
	public ModelAndView doGetRedact(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return userRedact(request, response);
	}

	@RequestMapping(value = "/userRedact.action", method = RequestMethod.POST)
	public ModelAndView doPostRedact(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return userRedact(request, response);
	}

	private ModelAndView userRedact(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("userRedact");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		int userId = -1;
		if (isAuthenticated) {
			if (userName != null) {
				userId = Utils.getUserIdByHash(cookies);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				
				User user = UserManager.getInstance().getUserById(userId);
				request.setAttribute("user", user);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		String username = request.getParameter("username");
		String email = request.getParameter("email");
		String lastName = request.getParameter("lastName");
		String firstName = request.getParameter("firstName");
		String middleName = request.getParameter("middleName");
		String dateOfBirth = request.getParameter("dateOfBirth");
		String organization = request.getParameter("organization");
		String position = request.getParameter("position");
		String phoneNumber = request.getParameter("phoneNumber");

		if ((username != null) || (email != null) || (lastName != null)
				|| (firstName != null) || (middleName != null)
				|| (dateOfBirth != null) || (organization != null)
				|| (position != null) || (phoneNumber != null) && (userId > 0)) {

			User userUpdate = new User();
			userUpdate.setId(userId);
			userUpdate.setUsername(username);
			userUpdate.setEmail(email);
			userUpdate.setLastName(lastName);
			userUpdate.setFirstName(firstName);
			userUpdate.setMiddleName(middleName);
			userUpdate.setDateOfBirth(dateOfBirth);
			userUpdate.setOrganization(organization);
			userUpdate.setPosition(position);
			userUpdate.setPhoneNumber(phoneNumber);

			UserManager.getInstance().userUpdate(userUpdate);

			try {
				response.sendRedirect("index.action");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return mav;
	}

	@RequestMapping(value = "/changePassword.action", method = RequestMethod.GET)
	public ModelAndView doGetPassword(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return changePassword(request, response);
	}

	@RequestMapping(value = "/changePassword.action", method = RequestMethod.POST)
	public ModelAndView doPostPassword(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return changePassword(request, response);
	}

	private ModelAndView changePassword(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("changePassword");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		int userId = -1;
		User user = null;
		if (isAuthenticated) {
			if (userName != null) {
				userId = Utils.getUserIdByHash(cookies);
				user = UserManager.getInstance().getUserById(userId);
				request.setAttribute("user", user);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		String oldPassword = request.getParameter("oldPassword");
		String newPassword1 = request.getParameter("newPassword1");
		String newPassword2 = request.getParameter("newPassword2");

		if ((oldPassword != null) && (newPassword1 != null) && (user != null)
				&& (userId > 0)) {

			if (oldPassword.equals(user.getPassword())) {

				if (newPassword1.equals(newPassword2)) {

					user.setPassword(newPassword1);
					UserManager.getInstance().changePassword(user);

					try {
						response.sendRedirect("index.action");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}

		}
		return mav;
	}

	@RequestMapping(value = "/changeAvatar.action", method = RequestMethod.GET)
	public ModelAndView doGetAvatar(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return changeAvatar(request, response);
	}

	@RequestMapping(value = "/changeAvatar.action", method = RequestMethod.POST)
	public ModelAndView doPostAvatar(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return changeAvatar(request, response);
	}

	private ModelAndView changeAvatar(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("changeAvatar");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		int userId = -1;
		User user = null;
		if (isAuthenticated) {
			if (userName != null) {
				userId = Utils.getUserIdByHash(cookies);
				user = UserManager.getInstance().getUserById(userId);
				request.setAttribute("user", user);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		return mav;
	}
}
