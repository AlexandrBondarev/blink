package com.blink.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blink.managers.UserManager;
import com.blink.utils.Utils;

@Controller
public class HomeConroller {

	@RequestMapping(value = "/index.action", method = RequestMethod.GET)
	public ModelAndView signinGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("home");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		request.setAttribute("isAuthenticated", isAuthenticated);
		return mav;

	}

	@RequestMapping(value = "/index.action", method = RequestMethod.POST)
	public ModelAndView signUpVerification(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "password", required = true) String password)
			throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("home");

		boolean isUserRegistered = UserManager.getInstance().isUserRegistered(
				email, password);
		if (isUserRegistered) {
			String userHash = UserManager.getInstance().getUserHash(email);
			Cookie userHashCookie = new Cookie("userhash", userHash);
			userHashCookie.setMaxAge(60 * 60);
			response.addCookie(userHashCookie);
			request.setAttribute("isAuthenticated", true);

			if (page == null) {
				response.sendRedirect("index.action");

			} else {
				response.sendRedirect(page);
			}
		} 
		else {
			return mav;

		}
		return mav;

	}

}