package com.blink.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.Counselor;
import com.blink.entity.Country;
import com.blink.entity.InvitedCounselors;
import com.blink.entity.PrivateMessage;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.User;
import com.blink.managers.AreaManager;
import com.blink.managers.CounselorsManager;
import com.blink.managers.PermissionsManager;
import com.blink.managers.PersonalMessagesManager;
import com.blink.managers.ProjectDetailsManager;
import com.blink.managers.ProjectManager;
import com.blink.managers.UserManager;
import com.blink.utils.HomePageData;
import com.blink.utils.Utils;

@Controller
public class CounselorsController {

	@RequestMapping(value = "/counselors.action", method = RequestMethod.GET)
	public ModelAndView showCounselors(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "search", required = false) String searchText,
			@RequestParam(value = "countryId", required = false) String countryIdText)
			throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("counselors");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				mav.addObject("userName", userName);
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
			}
			HomePageData homePageData = ProjectManager.getInstance()
					.getHomePageData(Utils.getUserIdByHash(cookies));
			mav.addObject("homePageData", homePageData);

		}

		mav.addObject("isAuthenticated", isAuthenticated);

		List<Country> countriesList = CounselorsManager.getInstance()
				.getCountriesList();
		mav.addObject("countriesList", countriesList);
		if (searchText != null) {
			if (searchText.length() > 0) {
				Set<Counselor> counselorsList = CounselorsManager.getInstance()
						.getFoundCounselorsList(searchText);
				mav.addObject("counselorsBean", counselorsList);
				return mav;
			}
		}
		
		int countryId = -1;
		try {
			countryId = Integer.valueOf(countryIdText);
		} catch (NumberFormatException e) {
		}
		if (countryId > -1) {
			mav.addObject("countryId", countryId);

			List<Counselor> counselorsList = CounselorsManager.getInstance()
					.getCounselorsListByCountry(countryId);
			mav.addObject("counselorsBean", counselorsList);

		} else {
			List<Counselor> counselorsList = CounselorsManager.getInstance()
					.getCounselorsList();
			mav.addObject("counselorsBean", counselorsList);
		}
		return mav;
	}

	@RequestMapping(value = "/inviteCounselor.action", method = RequestMethod.GET)
	public ModelAndView inviteCounselor(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "counselorId", required = false) String counselorIdText,
			@RequestParam(value = "projectId", required = false) String projectIdText)
			throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("inviteCounselor");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				request.setAttribute("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);
			}
		} else {
			response.sendRedirect("signin.action");
		}
		request.setAttribute("isAuthenticated", isAuthenticated);

		if (counselorIdText == null) {
			return mav;
		}
		int counselorId = -1;
		try {
			counselorId = Integer.valueOf(counselorIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		if (counselorId <= 0) {
			return mav;
		}
		request.setAttribute("counselorId", counselorId);

		int userId = -1;
		try {
			userId = Utils.getUserIdByHash(cookies);
		} catch (NullPointerException e) {
			return mav;

		}

		List<ProjectPermissions> projectPermissionsList = ProjectDetailsManager
				.getInstance().getProjectsPermissions(userId);
		mav.addObject("projectPermissionsList", projectPermissionsList);

		Counselor counselor = CounselorsManager.getInstance().getCounselorById(
				counselorId);
		mav.addObject("counselor", counselor);

		int projectId;
		try {
			projectId = Integer.valueOf(projectIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		mav.addObject("isInvited", false);
		if (projectId != 0) {

			boolean isInvited = CounselorsManager.getInstance().isInvited(
					projectId, counselorId);
			mav.addObject("isInvited", isInvited);
			if (isInvited) {
				return mav;
			}

			InvitedCounselors invitedCounselor = new InvitedCounselors();

			Project project = new Project();
			project.setId(projectId);
			invitedCounselor.setProject(project);

			invitedCounselor.setCounselor(counselor);
			invitedCounselor.setInvitationDate(new Date().toString());

			CounselorsManager.getInstance().invitCounselor(invitedCounselor);

			ProjectPermissions permission = new ProjectPermissions();

			permission.setProject(project);

			User user = new User();
			user.setId(counselor.getUser().getId());
			permission.setUser(user);

			permission.setRights(3);
			PermissionsManager.getInstance().addProjectsPermissions(permission);

			PrivateMessage privateMessage = new PrivateMessage();

			User sender = new User();
			sender.setId(userId);
			privateMessage.setSender(sender);

			User receiver = new User();
			receiver.setId(counselor.getUser().getId());
			privateMessage.setReceiver(receiver);

			privateMessage.setCounselor(counselor);

			privateMessage.setProject(project);

			privateMessage.setCreationDate((new Date()).toString());
			PersonalMessagesManager.getInstance().addNewMessage(privateMessage);

			String address = "project.action?projectId=" + projectIdText;
			response.sendRedirect(address);
		}
		return mav;
	}

	@RequestMapping(value = "/counselorDetails.action", method = RequestMethod.GET)
	public ModelAndView doGetDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return counselorDetails(request, response);
	}

	@RequestMapping(value = "/counselorDetails.action", method = RequestMethod.POST)
	public ModelAndView doPostDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return counselorDetails(request, response);
	}

	private ModelAndView counselorDetails(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("counselorDetails");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				request.setAttribute("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				request.setAttribute("homePageData", homePageData);
			}
		}
		request.setAttribute("isAuthenticated", isAuthenticated);
		String counselorIdText = request.getParameter("counselorId");
		if (counselorIdText == null) {
			return mav;
		}
		int counselorId = -1;
		try {
			counselorId = Integer.valueOf(counselorIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		if (counselorId <= 0) {
			return mav;
		}
		Counselor counselor = CounselorsManager.getInstance().getCounselorById(
				counselorId);
		mav.addObject("counselor", counselor);
		return mav;
	}

	@RequestMapping(value = "/newCounselor.action", method = RequestMethod.GET)
	public ModelAndView doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return createNewCounselor(request, response);
	}

	@RequestMapping(value = "/newCounselor.action", method = RequestMethod.POST)
	public ModelAndView doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return createNewCounselor(request, response);
	}

	private ModelAndView createNewCounselor(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("newCounselor");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		List<Country> countriesList = ProjectManager.getInstance()
				.getAllcountriesList();
		mav.addObject("countriesList", countriesList);

		String countryIdText = request.getParameter("countryId");
		int countryId = -1;
		try {
			countryId = Integer.valueOf(countryIdText);
		} catch (NumberFormatException e) {
		}
		if (countryId > -1) {

			mav.addObject("countryId", countryId);

		} else {
			mav.addObject("countryId", -1);
		}
		String titleText = request.getParameter("title");
		if (titleText != null) {
			if (titleText.length() > 0) {
				mav.addObject("isTitle", true);
				mav.addObject("title", titleText);
			}
		}

		String description = request.getParameter("description");
		if (description != null) {
			if (description.length() > 0) {

				int userId = Utils.getUserIdByHash(cookies);
				String title = request.getParameter("title");

				Counselor counselor = new Counselor();
				User user = new User();
				user.setId(userId);
				counselor.setUser(user);
				Country country = new Country();
				country.setId(countryId);
				counselor.setCountry(country);
				counselor.setTitle(title);
				counselor.setCompetence(description);
				CounselorsManager.getInstance().addNewCounselor(counselor);

				// Добавить запись в таблицу прав:
				// 1.Взять ID созданного проекта:
				int counselorId = CounselorsManager.getInstance().getMaxId();
				try {
					response.sendRedirect("counselorDetails.action?counselorId="
							+ counselorId);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return mav;
			}
		}
		return mav;
	}

}
