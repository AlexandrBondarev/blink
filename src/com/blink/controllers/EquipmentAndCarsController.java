package com.blink.controllers;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.City;
import com.blink.entity.Country;
import com.blink.entity.Equipment;
import com.blink.entity.Image;
import com.blink.entity.InvitedEquipment;
import com.blink.entity.PrivateMessage;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.Region;
import com.blink.entity.User;
import com.blink.managers.EquipmentAndCarsManager;
import com.blink.managers.ImageManager;
import com.blink.managers.PermissionsManager;
import com.blink.managers.PersonalMessagesManager;
import com.blink.managers.ProjectDetailsManager;
import com.blink.managers.ProjectManager;
import com.blink.managers.UserManager;
import com.blink.utils.HomePageData;
import com.blink.utils.Utils;

@Controller
public class EquipmentAndCarsController {

	@RequestMapping(value = "/cars.action", method = RequestMethod.GET)
	public ModelAndView showEquipmentAndCars(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "search", required = false) String searchText,
			@RequestParam(value = "countryId", required = false) String countryIdText,
			@RequestParam(value = "regionId", required = false) String regionIdText,
			@RequestParam(value = "cityId", required = false) String cityIdText)

	throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("cars");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		List<Country> countriesList = EquipmentAndCarsManager.getInstance()
				.getCountriesList();
		mav.addObject("countriesList", countriesList);

		if (searchText != null) {
			if (searchText.length() > 0) {
				Set<Equipment> equipmentAndCarsList = EquipmentAndCarsManager
						.getInstance().getFoundEquipmentsList(searchText);
				mav.addObject("equipmentAndCarsList", equipmentAndCarsList);
				return mav;
			}

		}

		int countryId = -1;
		try {
			countryId = Integer.valueOf(countryIdText);
		} catch (NumberFormatException e) {
		}
		if (countryId > -1) {
			mav.addObject("countryId", countryId);

			// Если выбрана страна то отображаем лист регионов этой страны и
			// проверяем передано ли ID региона

			List<Region> regionsList = EquipmentAndCarsManager.getInstance()
					.getRegionList(countryId);
			mav.addObject("regionsList", regionsList);
			int regionId = -1;
			try {
				regionId = Integer.valueOf(regionIdText);
			} catch (NumberFormatException e) {
			}
			if (regionId > -1) {
				mav.addObject("regionId", regionId);

				// Если выбрана страна и регион то отбражаем список городов
				// региона и проверяем передано ли ID города

				List<City> citiesList = EquipmentAndCarsManager.getInstance()
						.getCitiesList(regionId);
				mav.addObject("citiesList", citiesList);

				int cityId = -1;
				try {
					cityId = Integer.valueOf(cityIdText);
				} catch (NumberFormatException e) {
				}
				if (cityId > -1) {
					List<Equipment> sortedEquipmentAndCarsList = EquipmentAndCarsManager
							.getInstance().getSortedEquipmentAndCarsListByCity(
									cityId);
					mav.addObject("equipmentAndCarsList",
							sortedEquipmentAndCarsList);
					mav.addObject("cityId", cityId);
					// если не выбран город то сортирум проекты по региону
				} else {
					List<Equipment> sortedEquipmentAndCarsList = EquipmentAndCarsManager
							.getInstance()
							.getSortedEquipmentAndCarsListByRegion(regionId);
					mav.addObject("equipmentAndCarsList",
							sortedEquipmentAndCarsList);
				}
				// если не выбран регион то сортируем проекты по стране
			} else {
				List<Equipment> sortedEquipmentAndCars = EquipmentAndCarsManager
						.getInstance().getSortedEquipmentAndCarsList(countryId);
				mav.addObject("equipmentAndCarsList", sortedEquipmentAndCars);

			}
		} else {
			List<Equipment> equipmentAndCarsList = EquipmentAndCarsManager
					.getInstance().getEquipmentAndCarsList();
			mav.addObject("equipmentAndCarsList", equipmentAndCarsList);
			mav.addObject("countryId", -1);
		}

		return mav;
	}

	@RequestMapping(value = "/newEquipment.action", method = RequestMethod.GET)
	public ModelAndView doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return createNewEquipment(request, response);
	}

	@RequestMapping(value = "/newEquipment.action", method = RequestMethod.POST)
	public ModelAndView doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return createNewEquipment(request, response);
	}

	private ModelAndView createNewEquipment(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("newEquipment");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		List<Country> countriesList = ProjectManager.getInstance()
				.getAllcountriesList();
		mav.addObject("countriesList", countriesList);

		String countryIdText = request.getParameter("countryId");
		int countryId = -1;
		int regionId = -1;
		int cityId = -1;
		try {
			countryId = Integer.valueOf(countryIdText);
		} catch (NumberFormatException e) {
		}
		if (countryId > -1) {

			mav.addObject("countryId", countryId);

			List<Region> regionsList = ProjectManager.getInstance()
					.getAllRegionListByCountryId(countryId);
			mav.addObject("regionsList", regionsList);
			String regionIdText = request.getParameter("regionId");

			try {
				regionId = Integer.valueOf(regionIdText);
			} catch (NumberFormatException e) {
			}
			if (regionId > -1) {
				mav.addObject("regionId", regionId);

				List<City> citiyList = ProjectManager.getInstance()
						.getAllCitiesListByRegionId(regionId);
				mav.addObject("cityList", citiyList);
				String cityIdText = request.getParameter("cityId");

				try {
					cityId = Integer.valueOf(cityIdText);
				} catch (NumberFormatException e) {
				}
				if (cityId > -1) {
					mav.addObject("cityId", cityId);
				}
			}
		} else {
			mav.addObject("countryId", -1);
		}

		String titleText = request.getParameter("title");
		if (titleText != null) {
			if (titleText.length() > 0) {
				mav.addObject("isTitle", true);
				mav.addObject("title", titleText);
			}
		}

		String description = request.getParameter("description");
		if (description != null) {
			if (description.length() > 0) {

				int userId = Utils.getUserIdByHash(cookies);
				String title = request.getParameter("title");

				Equipment newEquipment = new Equipment();
				User user = new User();
				user.setId(userId);
				newEquipment.setUser(user);
				newEquipment.setTitle(title);
				newEquipment.setDescription(description);

				Country country = new Country();
				country.setId(countryId);
				newEquipment.setCountry(country);

				Region region = new Region();
				region.setId(regionId);
				newEquipment.setRegion(region);

				City city = new City();
				city.setId(cityId);
				newEquipment.setCity(city);
				newEquipment.setCreationDate(new Date().toString());

				EquipmentAndCarsManager.getInstance().addNewEquipment(
						newEquipment);

				try {
					response.sendRedirect("/cars.action");
				} catch (IOException e) {
					e.printStackTrace();
				}
				return mav;
			}
		}
		return mav;
	}

	@RequestMapping(value = "/equipmentUpdate.action", method = RequestMethod.GET)
	public ModelAndView doGetUpdate(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return equipmentRedact(request, response);
	}

	@RequestMapping(value = "/equipmentUpdate.action", method = RequestMethod.POST)
	public ModelAndView doPostUpdate(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return equipmentRedact(request, response);
	}

	private ModelAndView equipmentRedact(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("equipmentUpdate");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		String equipmentIdText = request.getParameter("equipmentId");
		int equipmentId = -1;
		try {
			equipmentId = Integer.valueOf(equipmentIdText);
			request.setAttribute("equipmentId", equipmentId);
		} catch (NumberFormatException e) {
		}
		if (equipmentId > -1) {

			Equipment equipment = EquipmentAndCarsManager.getInstance()
					.getEquipmentById(equipmentId);
			request.setAttribute("equipment", equipment);
		}

		String description = request.getParameter("description");
		if (description != null) {
			if (description.length() > 0) {

				String title = request.getParameter("title");

				Equipment equipment = new Equipment();
				equipment.setTitle(title);
				equipment.setDescription(description);
				equipment.setId(equipmentId);
				equipment.setLastUpdate(new Date().toString());

				EquipmentAndCarsManager.getInstance()
						.equipmentUpdate(equipment);

				try {
					response.sendRedirect("personalEquipments.action");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return mav;
	}

	@RequestMapping(value = "/equipmentImages.action", method = RequestMethod.GET)
	public ModelAndView doGetImages(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return equipmentImages(request, response);
	}

	@RequestMapping(value = "/equipmentImages.action", method = RequestMethod.POST)
	public ModelAndView doPostImages(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return equipmentImages(request, response);
	}

	private ModelAndView equipmentImages(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("equipmentImages");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				mav.addObject("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		String equipmentIdText = request.getParameter("equipmentId");
		int equipmentId = -1;
		try {
			equipmentId = Integer.valueOf(equipmentIdText);
			request.setAttribute("equipmentId", equipmentId);
		} catch (NumberFormatException e) {
		}
		if (equipmentId > -1) {
			List<Image> imagesList = ImageManager.getInstance()
					.getImagesByEquipmentId(equipmentId);
			request.setAttribute("imagesList", imagesList);
		}

		String description = request.getParameter("description");
		if (description != null) {
			if (description.length() > 0) {

				String title = request.getParameter("title");

				Equipment equipment = new Equipment();
				equipment.setTitle(title);
				equipment.setDescription(description);
				equipment.setId(equipmentId);
				equipment.setLastUpdate(new Date().toString());

				EquipmentAndCarsManager.getInstance()
						.equipmentUpdate(equipment);

				try {
					response.sendRedirect("personalEquipments.action");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return mav;
	}

	@RequestMapping(value = "/equipmentDetails.action", method = RequestMethod.GET)
	public ModelAndView doGetDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return equipmentDetails(request, response);
	}

	@RequestMapping(value = "/equipmentDetails.action", method = RequestMethod.POST)
	public ModelAndView doPostDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return equipmentDetails(request, response);
	}

	private ModelAndView equipmentDetails(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("equipmentDetails");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				request.setAttribute("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				request.setAttribute("homePageData", homePageData);
			}
		}
		request.setAttribute("isAuthenticated", isAuthenticated);

		String equipmentIdText = request.getParameter("equipmentId");
		int equipmentId = -1;
		try {
			equipmentId = Integer.valueOf(equipmentIdText);
			request.setAttribute("equipmentId", equipmentId);
		} catch (NumberFormatException e) {
		}
		if (equipmentId > -1) {
			String deleteIdText = request.getParameter("deleteId");
			int deleteId = -1;
			try {
				deleteId = Integer.valueOf(deleteIdText);
			} catch (NumberFormatException e) {
			}
			if (deleteId > -1) {

				String fileName = ImageManager.getInstance().getImageNameById(
						deleteId);
				ImageManager.getInstance().deleteImgById(deleteId);
				if (fileName != null) {
					//fileName = "C:\\Java\\workspace\\blink\\WebContent\\images\\equipments\\"
					fileName ="/var/lib/tomcat7/webapps/ROOT/images/equipments/"
							+ fileName;
					File file = new File(fileName);
					file.delete();
				}
			}

			Equipment equipment = EquipmentAndCarsManager.getInstance()
					.getEquipmentById(equipmentId);
			request.setAttribute("equipment", equipment);

			List<Image> imagesList = ImageManager.getInstance()
					.getImagesByEquipmentId(equipmentId);
			request.setAttribute("imagesList", imagesList);
		}
		return mav;
	}

	@RequestMapping(value = "/inviteEquipment.action", method = RequestMethod.GET)
	public ModelAndView inviteEquipment(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "equipmentId", required = false) String equipmentIdText,
			@RequestParam(value = "projectId", required = false) String projectIdText)
			throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("inviteEquipment");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				request.setAttribute("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);
			}
		}
		request.setAttribute("isAuthenticated", isAuthenticated);
		if (equipmentIdText == null) {
			return mav;
		}
		int equipmentId = -1;
		try {
			equipmentId = Integer.valueOf(equipmentIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		if (equipmentId <= 0) {
			return mav;
		}
		request.setAttribute("equipmentId", equipmentId);

		int userId = Utils.getUserIdByHash(cookies);

		List<ProjectPermissions> projectPermissionsList = ProjectDetailsManager
				.getInstance().getProjectsPermissions(userId);
		mav.addObject("projectPermissionsList", projectPermissionsList);

		Equipment equipment = EquipmentAndCarsManager.getInstance()
				.getEquipmentById(equipmentId);
		mav.addObject("equipment", equipment);

		int projectId;
		try {
			projectId = Integer.valueOf(projectIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		mav.addObject("isInvited", false);
		if (projectId != 0) {

			boolean isInvited = EquipmentAndCarsManager.getInstance()
					.isInvited(projectId, equipmentId);
			mav.addObject("isInvited", isInvited);
			if (isInvited) {
				return mav;
			}

			InvitedEquipment invitedEquipment = new InvitedEquipment();

			Project project = new Project();
			project.setId(projectId);
			invitedEquipment.setProject(project);

			invitedEquipment.setEquipment(equipment);
			invitedEquipment.setInvitationDate(Utils.getShortDate());

			EquipmentAndCarsManager.getInstance().invitEquipment(
					invitedEquipment);

			ProjectPermissions permission = new ProjectPermissions();

			permission.setProject(project);

			User user = new User();
			user.setId(equipment.getUser().getId());
			permission.setUser(user);

			permission.setRights(4);
			PermissionsManager.getInstance().addProjectsPermissions(permission);

			PrivateMessage privateMessage = new PrivateMessage();

			User sender = new User();
			sender.setId(userId);
			privateMessage.setSender(sender);

			User receiver = new User();
			receiver.setId(equipment.getUser().getId());
			privateMessage.setReceiver(receiver);

			privateMessage.setEquipment(equipment);
			privateMessage.setProject(project);

			privateMessage.setCreationDate((new Date()).toString());
			PersonalMessagesManager.getInstance().addNewMessage(privateMessage);

			String address = "project.action?projectId=" + projectIdText;
			response.sendRedirect(address);
		}
		return mav;
	}

}