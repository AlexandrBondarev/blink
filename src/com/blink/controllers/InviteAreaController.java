package com.blink.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.Area;
import com.blink.entity.PrivateMessage;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.User;
import com.blink.managers.AreaManager;
import com.blink.managers.PermissionsManager;
import com.blink.managers.PersonalMessagesManager;
import com.blink.managers.ProjectDetailsManager;
import com.blink.managers.ProjectManager;
import com.blink.managers.UserManager;
import com.blink.utils.HomePageData;
import com.blink.utils.Utils;

@Controller
public class InviteAreaController {

	@RequestMapping(value = "/inviteArea.action", method = RequestMethod.GET)
	public ModelAndView inviteArea(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "areaId", required = false) String areaIdText,
			@RequestParam(value = "projectId", required = false) String projectIdText)
			throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("inviteArea");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				request.setAttribute("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
			}
		} else {
			response.sendRedirect("signin.action");
		}
		request.setAttribute("isAuthenticated", isAuthenticated);

		if (areaIdText == null) {
			return mav;
		}
		int areaId = -1;
		try {
			areaId = Integer.valueOf(areaIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		if (areaId <= 0) {
			return mav;
		}
		request.setAttribute("areaId", areaId);

		int userId = -1;
		try {
			userId = Utils.getUserIdByHash(cookies);
		} catch (NullPointerException e) {
			return mav;

		}

		List<ProjectPermissions> projectPermissionsList = ProjectDetailsManager
				.getInstance().getProjectsPermissions(userId);
		mav.addObject("projectPermissionsList", projectPermissionsList);

		Area area = AreaManager.getInstance().getAreaById(areaId);
		mav.addObject("area", area);

		int projectId;
		try {
			projectId = Integer.valueOf(projectIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		mav.addObject("isInvited", false);
		if (projectId != 0) {

			boolean isInvited = AreaManager.getInstance().isInvited(projectId,
					areaId);
			mav.addObject("isInvited", isInvited);
			if (isInvited) {
				return mav;
			}

			AreaManager.getInstance().invitArea(projectId, areaId);
			ProjectPermissions permission = new ProjectPermissions();

			Project project = new Project();
			project.setId(projectId);
			permission.setProject(project);

			User user = new User();
			user.setId(area.getUser().getId());
			permission.setUser(user);

			permission.setRights(2);
			PermissionsManager.getInstance().addProjectsPermissions(permission);

			PrivateMessage privateMessage = new PrivateMessage();

			User sender = new User();
			sender.setId(userId);
			privateMessage.setSender(sender);

			privateMessage.setArea(area);

			User receiver = new User();
			receiver.setId(area.getUser().getId());
			privateMessage.setReceiver(receiver);

			privateMessage.setProject(project);

			privateMessage.setCreationDate(Utils.getShortDate());
			PersonalMessagesManager.getInstance().addNewMessage(privateMessage);

			String address = "project.action?projectId=" + projectIdText;
			response.sendRedirect(address);
		}
		return mav;
	}
}
