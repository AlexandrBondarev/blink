package com.blink.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.Area;
import com.blink.entity.Counselor;
import com.blink.entity.Equipment;
import com.blink.entity.InvitedArea;
import com.blink.entity.InvitedCounselors;
import com.blink.entity.InvitedEquipment;
import com.blink.entity.InvitedUsers;
import com.blink.entity.Messages;
import com.blink.entity.PrivateMessage;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.User;
import com.blink.managers.AreaManager;
import com.blink.managers.CounselorsManager;
import com.blink.managers.EquipmentAndCarsManager;
import com.blink.managers.PermissionsManager;
import com.blink.managers.PersonalMessagesManager;
import com.blink.managers.ProjectDetailsManager;
import com.blink.managers.ProjectManager;
import com.blink.managers.UserManager;
import com.blink.utils.HomePageData;
import com.blink.utils.Utils;

@Controller
public class ProjectDetailsController {

	@RequestMapping(value = "/projectDetails.action", method = RequestMethod.GET)
	public ModelAndView doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return showProject(request, response);
	}

	@RequestMapping(value = "/projectDetails.action", method = RequestMethod.POST)
	public ModelAndView doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return showProject(request, response);
	}

	private ModelAndView showProject(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		ModelAndView mav = new ModelAndView("projectDetails");

		String projectIdText = request.getParameter("projectId");
		int projectId;
		try {
			projectId = Integer.valueOf(projectIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		mav.addObject("projectId", projectId);
		int userId = -1;
		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);

			}
		}
		List<InvitedArea> invitedAreaList = AreaManager.getInstance()
				.getAreaByProject(projectId);
		mav.addObject("invitedAreaList", invitedAreaList);
		List<InvitedCounselors> invitedCounselorsList = CounselorsManager
				.getInstance().getInvitedCounselors(projectId);
		mav.addObject("invitedCounselorsList", invitedCounselorsList);
		List<InvitedEquipment> invitedEquipmentList = EquipmentAndCarsManager
				.getInstance().getInvitedEquipment(projectId);
		mav.addObject("invitedEquipmentList", invitedEquipmentList);
		List<InvitedUsers> invitedUsersList = UserManager.getInstance()
				.getInvitedUsers(projectId);
		mav.addObject("invitedUsersList", invitedUsersList);
		mav.addObject("isAuthenticated", isAuthenticated);

		// сдесь права юзера
		if (isAuthenticated) {
			int userPermission = UserManager.getInstance().getUserPermissions(
					userId, projectId);
			mav.addObject("userPermission", userPermission);
			if (userPermission == 0) {
				String suggestionKeyText = request
						.getParameter("suggestionKey");
				int suggestionKey = -1;
				try {
					suggestionKey = Integer.valueOf(suggestionKeyText);
				} catch (NumberFormatException e) {
				}
				String invitation = request.getParameter("privateMessage");
				if (suggestionKey > -1) {
					mav.addObject("suggestionKey", suggestionKey);

					String categoryText = request.getParameter("category");
					int category = -1;
					try {
						category = Integer.valueOf(categoryText);
					} catch (NumberFormatException e) {
					}
					if (category > -1) {
						mav.addObject("category", category);

						if (invitation != null) {
							PrivateMessage privateMessage = new PrivateMessage();
							privateMessage.setType(2);
							privateMessage.setMessage(invitation);

							int receiverUserId = UserManager.getInstance()
									.getUserIdByProjectId(projectId);
							User receiver = new User();
							receiver.setId(receiverUserId);
							privateMessage.setReceiver(receiver);

							int senderUserId = Utils.getUserIdByHash(cookies);
							User sender = new User();
							sender.setId(senderUserId);
							privateMessage.setSender(sender);

							privateMessage.setCreationDate(new Date()
									.toString());

							Project project = new Project();
							project.setId(projectId);
							privateMessage.setProject(project);

							switch (suggestionKey) {
							case 1:
								suggestionKey = 1;
								Area area = AreaManager.getInstance()
										.getAreaById(category);
								privateMessage.setArea(area);
								break;
							case 2:
								suggestionKey = 2;
								Counselor counselor = CounselorsManager
										.getInstance().getCounselorById(
												category);
								privateMessage.setCounselor(counselor);
								break;
							case 3:
								suggestionKey = 3;
								Equipment equipment = EquipmentAndCarsManager
										.getInstance().getEquipmentById(
												category);
								privateMessage.setEquipment(equipment);
								break;
							}

							PersonalMessagesManager.getInstance()
									.addProjectInvitation(privateMessage);
							response.sendRedirect("outbox.action");
						}
					}
				}
				if ((suggestionKey == 4) && (invitation != null)) {
					PrivateMessage privateMessage = new PrivateMessage();

					privateMessage.setMessage(invitation);

					int receiverUserId = UserManager.getInstance()
							.getUserIdByProjectId(projectId);
					User receiver = new User();
					receiver.setId(receiverUserId);
					privateMessage.setReceiver(receiver);
					privateMessage.setType(2);
					int senderUserId = Utils.getUserIdByHash(cookies);
					User sender = new User();
					sender.setId(senderUserId);
					privateMessage.setSender(sender);

					privateMessage.setCreationDate(new Date().toString());

					Project project = new Project();
					project.setId(projectId);
					privateMessage.setProject(project);
					PersonalMessagesManager.getInstance().addProjectInvitation(
							privateMessage);
					response.sendRedirect("outbox.action");
				}
			} else {
				mav.addObject("suggestionKey", -1);
			}
		}

		String newMessage = request.getParameter("newMessage");
		if (newMessage != null) {
			ProjectDetailsManager.getInstance().addNewMessage(
					Utils.getUserIdByHash(cookies), newMessage, projectId);
			response.sendRedirect("projectDetails.action?projectId="
					+ projectId);
		}

		Project projectInfo = ProjectManager.getInstance().getProjectInfo(
				projectId);
		mav.addObject("projectInfo", projectInfo);

		List<Messages> messagesList = ProjectDetailsManager.getInstance()
				.getMessagesList(projectId);
		mav.addObject("messagesList", messagesList);

		return mav;
	}

	@RequestMapping(value = "/invitedarea.action", method = RequestMethod.GET)
	public ModelAndView showInvitedAreas(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "projectId", required = false) String projectIdText)

	throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("showInvitedAreas");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				mav.addObject("userName", userName);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		int projectId;
		try {
			projectId = Integer.valueOf(projectIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		List<InvitedArea> invitedAreaList = AreaManager.getInstance()
				.getAreaByProject(projectId);
		mav.addObject("areasList", invitedAreaList);
		return mav;

	}

	@RequestMapping(value = "/invitedusers.action", method = RequestMethod.GET)
	public ModelAndView showInvitedUsers(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "projectId", required = false) String projectIdText)

	throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("showInvitedUsers");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				mav.addObject("userName", userName);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		int projectId;
		try {
			projectId = Integer.valueOf(projectIdText);
		} catch (NumberFormatException e) {
			return mav;
		}
		List<ProjectPermissions> invitedUsersList = PermissionsManager
				.getInstance().getUsersByProject(projectId);
		mav.addObject("usersList", invitedUsersList);
		return mav;

	}

	@RequestMapping(value = "/application.action", method = RequestMethod.POST)
	public void getRegions(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				String suggestion = "<option value=-1>Выберите желаемый статус в проекте</option>";

				List<Area> areasList = AreaManager.getInstance()
						.getAreasByUserId(userId);
				if (!areasList.isEmpty()) {
					suggestion = suggestion
							+ "<option value= 1>Владелец площади</option>";
				}
				List<Counselor> counselorsList = CounselorsManager
						.getInstance().getCounselorsByUserId(userId);
				if (!counselorsList.isEmpty()) {
					suggestion = suggestion
							+ "<option value= 2>Консалтер</option>";
				}
				List<Equipment> equipmentList = EquipmentAndCarsManager
						.getInstance().getEquipmentsByUserId(userId);
				if (!equipmentList.isEmpty()) {
					suggestion = suggestion
							+ "<option value= 3>Владелец оборудования и машин</option>";
				}
				suggestion = suggestion + "<option value= 4>Участник</option>";

				response.getWriter().println(suggestion);
			}
		}

	}

	@RequestMapping(value = "/showCategory.action", method = RequestMethod.POST)
	public void getRegions(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "key", required = false) String keyText)
			throws ServletException, IOException {

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				int suggestionKey = -1;
				try {
					suggestionKey = Integer.valueOf(keyText);
				} catch (NumberFormatException e) {
				}
				if (suggestionKey > -1) {

					String category = "<option value= -1>Выберите объявление</option>";
					switch (suggestionKey) {
					case 1:
						suggestionKey = 1;
						List<Area> areasList = AreaManager.getInstance()
								.getAreasByUserId(userId);
						for (Area each : areasList) {
							category = category + "<option value="
									+ each.getId() + ">" + each.getTitle()
									+ "</option>";
						}
						response.getWriter().println(category);
						break;
					case 2:
						suggestionKey = 2;
						List<Counselor> counselorsList = CounselorsManager
								.getInstance().getCounselorsByUserId(userId);
						for (Counselor each : counselorsList) {
							category = category + "<option value="
									+ each.getId() + ">" + each.getTitle()
									+ "</option>";
						}
						response.getWriter().println(category);
						break;
					case 3:
						suggestionKey = 3;
						List<Equipment> equipmentList = EquipmentAndCarsManager
								.getInstance().getEquipmentsByUserId(userId);
						for (Equipment each : equipmentList) {
							category = category + "<option value="
									+ each.getId() + ">" + each.getTitle()
									+ "</option>";
						}
						response.getWriter().println(category);
						break;

					}

				}
			}

		}
	}
}
