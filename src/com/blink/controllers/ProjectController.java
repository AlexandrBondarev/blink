package com.blink.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.City;
import com.blink.entity.Country;
import com.blink.entity.Project;
import com.blink.entity.Region;
import com.blink.managers.ProjectManager;
import com.blink.managers.UserManager;
import com.blink.utils.HomePageData;
import com.blink.utils.Utils;

@Controller
public class ProjectController {

	@RequestMapping(value = "/project.action", method = RequestMethod.GET)
	public ModelAndView showAllProjects(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "search", required = false) String searchText,
			@RequestParam(value = "countryId", required = false) String countryIdText,
			@RequestParam(value = "regionId", required = false) String regionIdText,
			@RequestParam(value = "cityId", required = false) String cityIdText)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("project");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				mav.addObject("userName", userName);
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				mav.addObject("homePageData", homePageData);

			}

		}
		mav.addObject("isAuthenticated", isAuthenticated);

		List<Country> countriesList = ProjectManager.getInstance()
				.getCountriesList();
		mav.addObject("countriesList", countriesList);
		if (searchText != null) {
			if (searchText.length() > 0) {
				Set<Project> projectsList = ProjectManager.getInstance()
						.getFoundProjectsList(searchText);
				mav.addObject("projectsBeansList", projectsList);
				return mav;
			}

		}

		int countryId = -1;
		try {
			countryId = Integer.valueOf(countryIdText);
		} catch (NumberFormatException e) {
		}
		if (countryId > -1) {
			mav.addObject("countryId", countryId);

			// Если выбрана страна то отображаем лист регионов этой страны и
			// проверяем передано ли ID региона

			List<Region> regionsList = ProjectManager.getInstance()
					.getRegionList(countryId);
			mav.addObject("regionsList", regionsList);
			int regionId = -1;
			try {
				regionId = Integer.valueOf(regionIdText);
			} catch (NumberFormatException e) {
			}
			if (regionId > -1) {
				mav.addObject("regionId", regionId);

				// Если выбрана страна и регион то отбражаем список городов
				// региона и проверяем передано ли ID города

				List<City> citiesList = ProjectManager.getInstance()
						.getCitiesList(regionId);
				mav.addObject("citiesList", citiesList);

				int cityId = -1;
				try {
					cityId = Integer.valueOf(cityIdText);
				} catch (NumberFormatException e) {
				}
				if (cityId > -1) {
					List<Project> projectsList = ProjectManager.getInstance()
							.getProjectsListByCityId(cityId);
					mav.addObject("projectsBeansList", projectsList);
					mav.addObject("cityId", cityId);
					// если не выбран город то сортирум проекты по региону
				} else {
					List<Project> projectsList = ProjectManager.getInstance()
							.getProjectsListByRegionId(regionId);
					mav.addObject("projectsBeansList", projectsList);
				}
				// если не выбран регион то сортируем проекты по стране
			} else {
				List<Project> projectsList = ProjectManager.getInstance()
						.getProjectsListByCountryId(countryId);
				mav.addObject("projectsBeansList", projectsList);
			}
		} else {
			List<Project> projectsList = ProjectManager.getInstance()
					.getProjectsList();
			mav.addObject("projectsBeansList", projectsList);
			mav.addObject("countryId", -1);

		}

		// JSP - request - передается в преобразователь JSP шаблона, а потом
		// передается в response:
		// request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request,
		// response);

		return mav;
	}

	@RequestMapping(value = "/project.action", method = RequestMethod.POST)
	public ModelAndView signUpVerification(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "password", required = true) String password)
			throws ServletException, IOException {

		boolean isUserRegistered = UserManager.getInstance().isUserRegistered(
				email, password);
		if (isUserRegistered) {
			String userHash = UserManager.getInstance().getUserHash(email);
			Cookie userHashCookie = new Cookie("userhash", userHash);
			userHashCookie.setMaxAge(60 * 60);
			response.addCookie(userHashCookie);
			request.setAttribute("isAuthenticated", true);
			response.sendRedirect("project.action");
		} else {
			return showAllProjects(request, response, null, null, null, null);

		}
		return showAllProjects(request, response, null, null, null, null);

	}

}
