package com.blink.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blink.managers.UserManager;
import com.blink.utils.Utils;

@Controller
public class UserSignOutController {

	@RequestMapping(value = "/signout.action", method = RequestMethod.GET)
	public ModelAndView userSignOut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("userSignOut");
		
		//request - запрос
		//response - ответ
		Cookie[] cookiesArray = request.getCookies();
		for (Cookie each : cookiesArray) {
			if (each.getName().equals("userhash")) {
				
				each.setMaxAge(0);
				//этим методом мы изменяем значение куки у клиента в браузере?
				response.addCookie(each);
				break;
			}
		}
		Integer userId = Utils.getUserIdByHash(cookiesArray);
		UserManager.getInstance().deleteHash(userId);
		return mav;
		
		
	}

}
