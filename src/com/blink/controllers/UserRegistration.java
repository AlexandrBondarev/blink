package com.blink.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.User;
import com.blink.managers.UserManager;

@Controller
public class UserRegistration {

	@RequestMapping(value = "/userRegistration.action", method = RequestMethod.GET)
	public ModelAndView doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("userRegistration");
		return mav;
	}

	@RequestMapping(value = "/userRegistration.action", method = RequestMethod.POST)
	public ModelAndView doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		ModelAndView mav = new ModelAndView("userRegistration");

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String lastName = request.getParameter("lastName");
		String firstName = request.getParameter("firstName");
		String middleName = request.getParameter("middleName");
		String dateOfBirth = request.getParameter("dateOfBirth");
		String organization = request.getParameter("organization");
		String position = request.getParameter("position");
		String phoneNumber = request.getParameter("phoneNumber");

		User newUser = new User();
		newUser.setUsername(username);
		newUser.setPassword(password);
		newUser.setEmail(email);
		newUser.setLastName(lastName);
		newUser.setFirstName(firstName);
		newUser.setMiddleName(middleName);
		newUser.setDateOfBirth(dateOfBirth);
		newUser.setOrganization(organization);
		newUser.setPosition(position);
		newUser.setPhoneNumber(phoneNumber);

		UserManager.getInstance().addUser(newUser);
		try {
			response.sendRedirect("project.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mav;

	}
}
