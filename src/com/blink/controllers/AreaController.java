package com.blink.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.Area;
import com.blink.entity.City;
import com.blink.entity.Country;
import com.blink.entity.Image;
import com.blink.entity.Region;
import com.blink.entity.User;
import com.blink.managers.AreaManager;
import com.blink.managers.ImageManager;
import com.blink.managers.ProjectManager;
import com.blink.managers.UserManager;
import com.blink.utils.HomePageData;
import com.blink.utils.Utils;

@Controller
public class AreaController {

	@RequestMapping(value = "/area.action", method = RequestMethod.GET)
	public ModelAndView showCommercialAreas(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "search", required = false) String searchText,
			@RequestParam(value = "countryId", required = false) String countryIdText,
			@RequestParam(value = "regionId", required = false) String regionIdText,
			@RequestParam(value = "cityId", required = false) String cityIdText)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("area");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				mav.addObject("userName", userName);
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
			}
			HomePageData homePageData = ProjectManager.getInstance()
					.getHomePageData(Utils.getUserIdByHash(cookies));
			mav.addObject("homePageData", homePageData);

		}
		mav.addObject("isAuthenticated", isAuthenticated);

		List<Country> countriesList = AreaManager.getInstance()
				.getCountriesList();
		mav.addObject("countriesList", countriesList);
		if (searchText != null) {
			if (searchText.length() > 0) {
				Set<Area> areasList = AreaManager.getInstance()
						.getFoundAreasList(searchText);
				mav.addObject("areaBeansList", areasList);
				return mav;
			}

		}

		int countryId = -1;
		try {
			countryId = Integer.valueOf(countryIdText);
		} catch (NumberFormatException e) {
		}
		if (countryId > -1) {
			mav.addObject("countryId", countryId);

			// Если выбрана страна то отображаем лист регионов этой страны и
			// проверяем передано ли ID региона

			List<Region> regionsList = AreaManager.getInstance().getRegionList(
					countryId);
			mav.addObject("regionsList", regionsList);
			int regionId = -1;
			try {
				regionId = Integer.valueOf(regionIdText);
			} catch (NumberFormatException e) {
			}
			if (regionId > -1) {
				mav.addObject("regionId", regionId);

				// Если выбрана страна и регион то отбражаем список городов
				// региона и проверяем передано ли ID города

				List<City> citiesList = AreaManager.getInstance()
						.getCitiesList(regionId);
				mav.addObject("citiesList", citiesList);

				int cityId = -1;
				try {
					cityId = Integer.valueOf(cityIdText);
				} catch (NumberFormatException e) {
				}
				if (cityId > -1) {
					List<Area> areasList = AreaManager.getInstance()
							.getAreasListByCityId(cityId);
					mav.addObject("areaBeansList", areasList);
					mav.addObject("cityId", cityId);
					// если не выбран город то сортирум проекты по региону
				} else {
					List<Area> areasList = AreaManager.getInstance()
							.getAreasListByRegionId(regionId);
					mav.addObject("areaBeansList", areasList);
				}
				// если не выбран регион то сортируем проекты по стране
			} else {
				List<Area> areasList = AreaManager.getInstance()
						.getAreasListByCountryId(countryId);
				mav.addObject("areaBeansList", areasList);
			}
		} else {
			List<Area> areasList = AreaManager.getInstance().getAreaBeansList();
			request.setAttribute("areaBeansList", areasList);
			mav.addObject("countryId", -1);

		}
		return mav;
	}

	@RequestMapping(value = "/areaDetails.action", method = RequestMethod.GET)
	public ModelAndView doGetDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return areaDetails(request, response);
	}

	@RequestMapping(value = "/areaDetails.action", method = RequestMethod.POST)
	public ModelAndView doPostDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return areaDetails(request, response);
	}

	private ModelAndView areaDetails(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("areaDetails");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				request.setAttribute("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(Utils.getUserIdByHash(cookies));
				request.setAttribute("homePageData", homePageData);
			}
		}
		request.setAttribute("isAuthenticated", isAuthenticated);

		String areaIdText = request.getParameter("areaId");
		int areaId = -1;
		try {
			areaId = Integer.valueOf(areaIdText);
			request.setAttribute("areaId", areaId);
		} catch (NumberFormatException e) {
		}
		if (areaId > -1) {

			Area area = AreaManager.getInstance().getAreaById(areaId);
			request.setAttribute("area", area);

			String deleteIdText = request.getParameter("deleteId");
			int deleteId = -1;
			try {
				deleteId = Integer.valueOf(deleteIdText);
			} catch (NumberFormatException e) {
			}
			if (deleteId > -1) {

				String fileName = ImageManager.getInstance().getImageNameById(
						deleteId);
				ImageManager.getInstance().deleteImgById(deleteId);

				if (fileName != null) {
					fileName = "/var/lib/tomcat7/webapps/ROOT/images/areas"
							+ fileName;
					File file = new File(fileName);
					file.delete();
				}
			}
			List<Image> imagesList = ImageManager.getInstance()
					.getImagesByAreaId(areaId);
			request.setAttribute("imagesList", imagesList);
		}
		return mav;
	}

	@RequestMapping(value = "/newArea.action", method = RequestMethod.GET)
	public ModelAndView doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return createNewArea(request, response);
	}

	@RequestMapping(value = "/newArea.action", method = RequestMethod.POST)
	public ModelAndView doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return createNewArea(request, response);
	}

	private ModelAndView createNewArea(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("newArea");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		List<Country> countriesList = ProjectManager.getInstance()
				.getAllcountriesList();
		mav.addObject("countriesList", countriesList);

		String countryIdText = request.getParameter("countryId");
		int countryId = -1;
		int regionId = -1;
		int cityId = -1;
		try {
			countryId = Integer.valueOf(countryIdText);
		} catch (NumberFormatException e) {
		}
		if (countryId > -1) {

			mav.addObject("countryId", countryId);

			List<Region> regionsList = ProjectManager.getInstance()
					.getAllRegionListByCountryId(countryId);
			mav.addObject("regionsList", regionsList);
			String regionIdText = request.getParameter("regionId");

			try {
				regionId = Integer.valueOf(regionIdText);
			} catch (NumberFormatException e) {
			}
			if (regionId > -1) {
				mav.addObject("regionId", regionId);

				List<City> citiyList = ProjectManager.getInstance()
						.getAllCitiesListByRegionId(regionId);
				mav.addObject("cityList", citiyList);
				String cityIdText = request.getParameter("cityId");

				try {
					cityId = Integer.valueOf(cityIdText);
				} catch (NumberFormatException e) {
				}
				if (cityId > -1) {
					mav.addObject("cityId", cityId);
				}
			}
		} else {
			mav.addObject("countryId", -1);
		}

		String titleText = request.getParameter("title");
		if (titleText != null) {
			if (titleText.length() > 0) {
				mav.addObject("isTitle", true);
				mav.addObject("title", titleText);
			}
		}

		String description = request.getParameter("description");
		if (description != null) {
			if (description.length() > 0) {

				int userId = Utils.getUserIdByHash(cookies);
				String title = request.getParameter("title");

				Area newArea = new Area();
				User user = new User();
				user.setId(userId);
				newArea.setUser(user);
				City city = new City();
				city.setId(cityId);
				newArea.setCity(city);
				Region region = new Region();
				region.setId(regionId);
				newArea.setRegion(region);
				Country country = new Country();
				country.setId(countryId);
				newArea.setCountry(country);
				newArea.setTitle(title);
				newArea.setDescription(description);
				AreaManager.getInstance().addNewArea(newArea);

				// Добавить запись в таблицу прав:
				// 1.Взять ID созданного проекта:
				int areaId = AreaManager.getInstance().getMaxId();
				try {
					response.sendRedirect("areaDetails.action?areaId=" + areaId);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return mav;
			}
		}
		return mav;
	}

}
