package com.blink.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blink.entity.City;
import com.blink.entity.Country;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.Region;
import com.blink.entity.User;
import com.blink.managers.PermissionsManager;
import com.blink.managers.ProjectDetailsManager;
import com.blink.managers.ProjectManager;
import com.blink.managers.UserManager;
import com.blink.utils.HomePageData;
import com.blink.utils.Utils;

@Controller
public class NewProjectController {

	@RequestMapping(value = "/newProject.action", method = RequestMethod.GET)
	public ModelAndView doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return createNewProject(request, response);
	}

	@RequestMapping(value = "/newProject.action", method = RequestMethod.POST)
	public ModelAndView doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return createNewProject(request, response);
	}

	private ModelAndView createNewProject(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("newProject");

		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				int userId = Utils.getUserIdByHash(cookies);
				mav.addObject("userId", userId);
				String avatar = UserManager.getInstance().getAvatarByUserId(
						userId);
				mav.addObject("avatar", avatar);
				mav.addObject("userName", userName);
				HomePageData homePageData = ProjectManager.getInstance()
						.getHomePageData(userId);
				mav.addObject("homePageData", homePageData);
			}
		}
		mav.addObject("isAuthenticated", isAuthenticated);

		List<Country> countriesList = ProjectManager.getInstance()
				.getAllcountriesList();
		mav.addObject("countriesList", countriesList);

		String countryIdText = request.getParameter("countryId");
		int countryId = -1;
		int regionId = -1;
		int cityId = -1;
		try {
			countryId = Integer.valueOf(countryIdText);
		} catch (NumberFormatException e) {
		}
		if (countryId > -1) {

			mav.addObject("countryId", countryId);

			List<Region> regionsList = ProjectManager.getInstance()
					.getAllRegionListByCountryId(countryId);
			mav.addObject("regionsList", regionsList);
			String regionIdText = request.getParameter("regionId");

			try {
				regionId = Integer.valueOf(regionIdText);
			} catch (NumberFormatException e) {
			}
			if (regionId > -1) {
				mav.addObject("regionId", regionId);

				List<City> citiyList = ProjectManager.getInstance()
						.getAllCitiesListByRegionId(regionId);
				mav.addObject("cityList", citiyList);
				String cityIdText = request.getParameter("cityId");

				try {
					cityId = Integer.valueOf(cityIdText);
				} catch (NumberFormatException e) {
				}
				if (cityId > -1) {
					mav.addObject("cityId", cityId);
				}
			}
		} else {
			mav.addObject("countryId", -1);
		}

		String titleText = request.getParameter("title");
		if (titleText != null) {
			if (titleText.length() > 0) {
				mav.addObject("isTitle", true);
				mav.addObject("title", titleText);
			}
		}

		String description = request.getParameter("description");
		if (description != null) {
			if (description.length() > 0) {

				int userId = Utils.getUserIdByHash(cookies);
				String title = request.getParameter("title");

				Project newProject = new Project();
				User user = new User();
				user.setId(userId);
				newProject.setUser(user);
				newProject.setTitle(title);
				newProject.setDescription(description);
				newProject.setMessagesCount(0);
				ProjectDetailsManager.getInstance().addNewProject(newProject,
						countryId, regionId, cityId);

				// Добавить запись в таблицу прав:
				// 1.Взять ID созданного проекта:

				int newProjectId = PermissionsManager.getInstance()
						.getLastProjectId();

				// 2.Добавить запись в projects_permissions:
				ProjectPermissions permission = new ProjectPermissions();

				Project project = new Project();
				project.setId(newProjectId);
				permission.setProject(project);

				permission.setUser(user);

				permission.setRights(1);
				PermissionsManager.getInstance().addProjectsPermissions(
						permission);

				try {
					response.sendRedirect("projectDetails.action?projectId="
							+ newProjectId);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return mav;
			}
		}
		return mav;
	}

	

	@RequestMapping(value = "/regionsList.action", method = RequestMethod.POST)
	public void getRegions(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "countryId", required = false) String countryIdText)
			throws ServletException, IOException {
		int countryId = -1;
		if (countryIdText != null) {
			try {
				countryId = Integer.valueOf(countryIdText);
			} catch (NumberFormatException e) {
			}
		}
		if (countryId > -1) {
			List<Region> regionsList = ProjectManager.getInstance()
					.getAllRegionListByCountryId(countryId);
			String regionsAndId = "<option value=-1>Выберите регион</option>";
			for (Region each : regionsList) {
				regionsAndId = regionsAndId + "<option value=" + each.getId()
						+ ">" + each.getRegionNameRu() + "</option>";
			}
			response.getWriter().println(regionsAndId);
		}
	}

	@RequestMapping(value = "/citiesList.action", method = RequestMethod.POST)
	public void getCities(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "regionId", required = false) String regionIdText)
			throws ServletException, IOException {
		int regionId = -1;
		if (regionIdText != null) {
			try {
				regionId = Integer.valueOf(regionIdText);
			} catch (NumberFormatException e) {
			}
		}
		if (regionId > -1) {
			List<City> citiesList = ProjectManager.getInstance()
					.getAllCitiesListByRegionId(regionId);
			String citiesAndId = "<option value=-1>Выберите Город</option>";
			for (City each : citiesList) {
				citiesAndId = citiesAndId + "<option value=" + each.getId()
						+ ">" + each.getCityNameRu() + "</option>";
			}
			response.getWriter().println(citiesAndId);
		}
	}

}