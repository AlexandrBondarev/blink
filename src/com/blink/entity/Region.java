package com.blink.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "region")
public class Region implements Serializable {

	@Id
	@Column(name = "id")
	private int id;

	@ManyToOne
	@JoinColumn(name = "id_country")
	private Country country;

	@Column(name = "oid")
	private int oid;

	@Column(name = "region_name_ru")
	private String regionNameRu;

	@Column(name = "region_name_en")
	private String regionNameEn;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "region")
	public Set<Project> projects = new HashSet<Project>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "region")
	public Set<Area> areas = new HashSet<Area>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "region")
	public Set<Equipment> equipments = new HashSet<Equipment>(0);

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getRegionNameRu() {
		return regionNameRu;
	}

	public void setRegionNameRu(String regionNameRu) {
		this.regionNameRu = regionNameRu;
	}

	public String getRegionNameEn() {
		return regionNameEn;
	}

	public void setRegionNameEn(String regionNameEn) {
		this.regionNameEn = regionNameEn;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public Set<Equipment> getEquipments() {
		return equipments;
	}

	public void setEquipments(Set<Equipment> equipments) {
		this.equipments = equipments;
	}

	public Set<Area> getAreas() {
		return areas;
	}

	public void setAreas(Set<Area> areas) {
		this.areas = areas;
	}

}