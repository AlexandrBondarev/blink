package com.blink.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "country")
public class Country implements Serializable {

	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "oid")
	private int oid;

	@Column(name = "country_name_ru")
	private String countryNameRu;

	@Column(name = "country_name_en")
	private String countryNameEn;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
	public Set<Project> projects = new HashSet<Project>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
	public Set<Counselor> counselors = new HashSet<Counselor>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
	public Set<Area> areas = new HashSet<Area>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
	public Set<Equipment> equipments = new HashSet<Equipment>(0);

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getCountryNameRu() {
		return countryNameRu;
	}

	public void setCountryNameRu(String countryNameRu) {
		this.countryNameRu = countryNameRu;
	}

	public String getCountryNameEn() {
		return countryNameEn;
	}

	public void setCountryNameEn(String countryNameEn) {
		this.countryNameEn = countryNameEn;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public Set<Equipment> getEquipments() {
		return equipments;
	}

	public void setEquipments(Set<Equipment> equipments) {
		this.equipments = equipments;
	}

	public Set<Area> getAreas() {
		return areas;
	}

	public void setAreas(Set<Area> areas) {
		this.areas = areas;
	}
}