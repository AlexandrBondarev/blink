package com.blink.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invited_counselors")
public class InvitedCounselors {


		@Id
		@Column(name = "id")
		private int id;

		@ManyToOne
		@JoinColumn(name = "project_id")
		private Project project;

		@ManyToOne
		@JoinColumn(name = "counselor_id")
		private Counselor counselor;
		
		@Column(name = "invitation_date")
		private String invitationDate;
		
		@Column(name = "agreement_date")
		private String agreementDate;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Project getProject() {
			return project;
		}

		public void setProject(Project project) {
			this.project = project;
		}

		public Counselor getCounselor() {
			return counselor;
		}

		public void setCounselor(Counselor counselor) {
			this.counselor = counselor;
		}

		public String getInvitationDate() {
			return invitationDate;
		}

		public void setInvitationDate(String invitationDate) {
			this.invitationDate = invitationDate;
		}

		public String getAgreementDate() {
			return agreementDate;
		}

		public void setAgreementDate(String agreementDate) {
			this.agreementDate = agreementDate;
		}


}
