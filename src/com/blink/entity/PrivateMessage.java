package com.blink.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "private_messages")
public class PrivateMessage implements Serializable {

	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "type")
	private int type;

	@Column(name = "message")
	private String message;

	@Column(name = "creation_date")
	private String creationDate;

	@Column(name = "trash_by_sender")
	private String trashBySender;
	
	@Column(name = "trash_by_receiver")
	private String trashByReceiver;
	
	@Column(name = "deleted_by_sender")
	private String deletedBySender;
	
	@Column(name = "deleted_by_receiver")
	private String deletedByReceiver;
	
	@ManyToOne
	@JoinColumn(name = "sender_user_id")
	private User sender;

	
	@ManyToOne
	@JoinColumn(name = "receiver_user_id")
	private User receiver;

	
	@ManyToOne
	@JoinColumn(name = "projects_id")
	private Project project;

	
	@ManyToOne
	@JoinColumn(name = "area_id")
	private Area area;
	
	@ManyToOne
	@JoinColumn(name = "equipment_id")
	private Equipment equipment;

	@ManyToOne
	@JoinColumn(name = "counselors_id")
	private Counselor counselor;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Counselor getCounselor() {
		return counselor;
	}

	public void setCounselor(Counselor counselor) {
		this.counselor = counselor;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTrashBySender() {
		return trashBySender;
	}

	public void setTrashBySender(String trashBySender) {
		this.trashBySender = trashBySender;
	}

	public String getTrashByReceiver() {
		return trashByReceiver;
	}

	public void setTrashByReceiver(String trashByReceiver) {
		this.trashByReceiver = trashByReceiver;
	}

	public String getDeletedBySender() {
		return deletedBySender;
	}

	public void setDeletedBySender(String deletedBySender) {
		this.deletedBySender = deletedBySender;
	}

	public String getDeletedByReceiver() {
		return deletedByReceiver;
	}

	public void setDeletedByReceiver(String deletedByReceiver) {
		this.deletedByReceiver = deletedByReceiver;
	}



}
