package com.blink.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "city")
public class City implements Serializable {

	@Id
	@Column(name = "id")
	private int id;

	@ManyToOne
	@JoinColumn(name = "id_region")
	private Region region;

	@ManyToOne
	@JoinColumn(name = "id_country")
	private Country country;

	@Column(name = "oid")
	private int oid;

	@Column(name = "city_name_ru")
	private String cityNameRu;

	@Column(name = "city_name_en")
	private String cityNameEn;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
	public Set<Project> projects = new HashSet<Project>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
	public Set<Area> areas = new HashSet<Area>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
	public Set<Equipment> equipments = new HashSet<Equipment>(0);

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getCityNameRu() {
		return cityNameRu;
	}

	public void setCityNameRu(String cityNameRu) {
		this.cityNameRu = cityNameRu;
	}

	public String getCityNameEn() {
		return cityNameEn;
	}

	public void setCityNameEn(String cityNameEn) {
		this.cityNameEn = cityNameEn;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public Set<Equipment> getEquipments() {
		return equipments;
	}

	public void setEquipments(Set<Equipment> equipments) {
		this.equipments = equipments;
	}

}
