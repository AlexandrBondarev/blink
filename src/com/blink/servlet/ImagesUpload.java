package com.blink.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.blink.entity.Area;
import com.blink.entity.Equipment;
import com.blink.entity.Image;
import com.blink.entity.User;
import com.blink.managers.ImageManager;
import com.blink.managers.UserManager;
import com.blink.utils.Utils;

@SuppressWarnings("serial")
// @WebServlet(value = "/imagesUpload")
public class ImagesUpload extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// String uploadDirectory =
		// "C:\\Java\\workspace\\blink\\WebContent\\images\\equipments\\";
		String uploadDirectory = "/var/lib/tomcat7/webapps/ROOT/images/equipments";
		int equipmentId = -1;
		int isUser = -1;
		int areaId = -1;

		int userId;
		Cookie[] cookies = request.getCookies();
		boolean isAuthenticated = Utils.isAuthentication(cookies);
		String userName = Utils.getUserNameByHash(cookies);
		if (isAuthenticated) {
			if (userName != null) {
				userId = Utils.getUserIdByHash(cookies);
				if (ServletFileUpload.isMultipartContent(request)) {
					try {
						@SuppressWarnings("unchecked")
						List<FileItem> multiparts = new ServletFileUpload(
								new DiskFileItemFactory())
								.parseRequest(request);
						String name = null;
						for (FileItem item : multiparts) {
							if (!item.isFormField()) {
								name = new File(item.getName()).getName();

								String equipmentIdText = request
										.getParameter("equipmentId");
								try {
									equipmentId = Integer
											.valueOf(equipmentIdText);
								} catch (NumberFormatException e) {
								}
								if (equipmentId > 0) {
									Image image = new Image();
									image.setFileName(name);

									User user = new User();
									user.setId(userId);
									image.setUser(user);

									Equipment equipment = new Equipment();
									equipment.setId(equipmentId);
									image.setEquipment(equipment);

									ImageManager.getInstance().savaImage(image);
								}

								String areaIdText = request
										.getParameter("areaId");
								try {
									areaId = Integer.valueOf(areaIdText);
								} catch (NumberFormatException e) {
								}
								if (areaId > 0) {

									Image image = new Image();
									image.setFileName(name);

									User user = new User();
									user.setId(userId);
									image.setUser(user);

									Area area = new Area();
									area.setId(areaId);
									image.setArea(area);

									ImageManager.getInstance().savaImage(image);
									// uploadDirectory =
									 //"C:\\Java\\workspace\\blink\\WebContent\\images\\areas\\";
									uploadDirectory = "/var/lib/tomcat7/webapps/ROOT/images/areas";
								}

								String isUserText = request
										.getParameter("isUser");
								try {
									isUser = Integer.valueOf(isUserText);
								} catch (NumberFormatException e) {
								}
								if (isUser > 0) {

									User user = new User();
									user.setId(userId);
									user.setAvatar(name);
									UserManager.getInstance().setAvatar(user);
									// uploadDirectory =
									// "C:\\Java\\workspace\\blink\\WebContent\\images\\users\\";
									uploadDirectory = "/var/lib/tomcat7/webapps/ROOT/images/users";
								}
								item.write(new File(uploadDirectory
										+ File.separator + name));

								if (equipmentId > 0) {
									response.sendRedirect("./equipmentDetails.action?equipmentId="
											+ equipmentId);
								}
								if (isUser > 0) {
									response.sendRedirect("./changeAvatar.action");
								}
								if (areaId > 0) {
									response.sendRedirect("./areaDetails.action?areaId="
											+ areaId);
								}

							}
						}

						// File uploaded successfully
						request.setAttribute("message",
								"Изображение успешно загруженно!");
					} catch (Exception ex) {
						request.setAttribute("message",
								"File Upload Failed due to " + ex);
					}

				} else {
					request.setAttribute("message",
							"Sorry this Servlet only handles file upload request");
				}
				try {
					request.getRequestDispatcher("WEB-INF/jsp/result.jsp")
							.forward(request, response);
				} catch (Exception e) {
				}

			}

		}
	}
}
