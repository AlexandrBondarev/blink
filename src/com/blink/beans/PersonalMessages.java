package com.blink.beans;

public class PersonalMessages {
	private String message;
	private String creationDate;
	private String readDate;
	private int senderUserId;
	private int receiverUserId;
	private int projectId;
	private int areaId;
	private int couselorsId;
	private String receiverUserName;
	private String projectTitle;
	private int countUnread;
	private int unreadProjectInvites;
	private int unreadAreaInvites;
	private int unreadCounselorsInvites;
	private int unreadLetters;
	private int messageId;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getReadDate() {
		return readDate;
	}
	public void setReadDate(String readDate) {
		this.readDate = readDate;
	}
	public int getSenderUserId() {
		return senderUserId;
	}
	public void setSenderUserId(int senderUserId) {
		this.senderUserId = senderUserId;
	}
	public int getReceiverUserId() {
		return receiverUserId;
	}
	public void setReceiverUserId(int receiverUserId) {
		this.receiverUserId = receiverUserId;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public int getCouselorsId() {
		return couselorsId;
	}
	public void setCouselorsId(int couselorsId) {
		this.couselorsId = couselorsId;
	}
	public String getReceiverUserName() {
		return receiverUserName;
	}
	public void setReceiverUserName(String receiverUserName) {
		this.receiverUserName = receiverUserName;
	}
	public String getProjectTitle() {
		return projectTitle;
	}
	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}
	public int getCountUnread() {
		return countUnread;
	}
	public void setCountUnread(int countUnread) {
		this.countUnread = countUnread;
	}
	public int getUnreadProjectInvites() {
		return unreadProjectInvites;
	}
	public void setUnreadProjectInvites(int unreadProjectInvites) {
		this.unreadProjectInvites = unreadProjectInvites;
	}
	public int getUnreadAreaInvites() {
		return unreadAreaInvites;
	}
	public void setUnreadAreaInvites(int unreadAreaInvites) {
		this.unreadAreaInvites = unreadAreaInvites;
	}
	public int getUnreadCounselorsInvites() {
		return unreadCounselorsInvites;
	}
	public void setUnreadCounselorsInvites(int unreadCounselorsInvites) {
		this.unreadCounselorsInvites = unreadCounselorsInvites;
	}

	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public int getUnreadLetters() {
		return unreadLetters;
	}
	public void setUnreadLetters(int unreadLetters) {
		this.unreadLetters = unreadLetters;
	}
	
	

	
}
