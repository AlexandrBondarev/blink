package com.blink.beans;


public class UserBean {
	
	private int id;
	private String username;
	private String password;
	private String email;
	private String lastName;
	private String firstName;
	private String middleName;
	private String dateOfBirth;
	private String organization;
	private String position;
	private String phoneNumber;
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}

	public String getUsername(){
		return username;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	
	public String getMiddleName(){
		return middleName;
	}
	
	public void setMiddleName(String middleName){
		this.middleName = middleName;
	}
	
	public String getDateOfBirth(){
		return dateOfBirth;
	}
	
	public void setDateOfBirth(String dateOfBirth){
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getOrganization(){
		return organization;
	}
	
	public void setOrganization(String organization){
		this.organization = organization;
	}
	
	public String getPosition(){
		return position;
	}
	
	public void setPosition(String pozition){
		this.position = pozition;
	}
	
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}
}
