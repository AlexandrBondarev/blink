package com.blink.beans;

public class ProjectBean {

	private String title;
	private String description;
	private String username;
	private String message;
	private String creationDate;
	private int projectId;
	private String countryNameRu;
	private String regionNameRu;
	private String cityNameRu;
	private String dateOfLastMessage;
	private int countMessages;
	
	public String getDateOfLastMessage() {
		return dateOfLastMessage;
	}

	public void setDateOfLastMessage(String dateOfLastMessage) {
		this.dateOfLastMessage = dateOfLastMessage;
	}

	public int getCountMessages() {
		return countMessages;
	}

	public void setCountMessages(int count) {
		this.countMessages = count;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId=projectId;
	}

	public String getCountryNameRu() {
		return countryNameRu;
	}

	public void setCountryNameRu(String countryNameRu) {
		this.countryNameRu = countryNameRu;
	}

	public String getRegionNameRu() {
		return regionNameRu;
	}

	public void setRegionNameRu(String regionNameRu) {
		this.regionNameRu = regionNameRu;
	}

	public String getCityNameRu() {
		return cityNameRu;
	}

	public void setCityNameRu(String cityNameRu) {
		this.cityNameRu = cityNameRu;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	private int userId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
