package com.blink.beans;
//rights
//1-директор
//2-владелец территории
//3-консалтер
//4-владелец машины
//5- участник




public class PermissionsBean {
	private int projectId;
	private int userId;
	private int rights;
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRights() {
		return rights;
	}

	public void setRights(int rights) {
		this.rights = rights;
	}

	

}
