package com.blink.utils;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;

import org.hibernate.Criteria;
import org.hibernate.Query;

import com.blink.managers.ProjectDetailsManager;
import com.blink.managers.UserManager;

public class Utils {

	public static boolean isAuthentication(Cookie[] cookiesArray) {

		if (cookiesArray != null) {
			for (Cookie each : cookiesArray) {
				if (each.getName().equals("userhash")) {
					String userHash = each.getValue();
					boolean isAuthenticated = UserManager.getInstance()
							.isAuthenticated(userHash);
					return isAuthenticated;
				}
			}
		}

		return false;
	}

	public static String getUserNameByHash(Cookie[] cookiesArray) {

		if (cookiesArray != null) {
			for (Cookie each : cookiesArray) {
				if (each.getName().equals("userhash")) {
					String userHash = each.getValue();
					String userName = UserManager.getInstance()
							.getUserNameByHash(userHash);
					return userName;
				}
			}
		}

		return null;
	}

	public static Integer getUserIdByHash(Cookie[] cookiesArray) {

		if (cookiesArray != null) {
			for (Cookie each : cookiesArray) {
				if (each.getName().equals("userhash")) {
					String userHash = each.getValue();
					int userId = ProjectDetailsManager.getInstance().getUserId(
							userHash);
					return userId;
				}
			}
		}

		return null;
	}

	public static <T> List<T> listAndCast(Criteria cr) {
		@SuppressWarnings("unchecked")
		List<T> list = cr.list();
		return list;
	}

	public static <T> List<T> listAndCast(Query query) {
		@SuppressWarnings("unchecked")
		List<T> list = query.list();
		return list;
	}

	public static String getFullDate() {
		Date currentDate = new Date();
		SimpleDateFormat dateFormat = null;
		DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols();

		String[] months = { "января", "февраля", "марта", "апреля", "мая",
				"июня", "июля", "августа", "сентября", "октября", "ноября",
				"декабря" };
		myDateFormatSymbols.setMonths(months);

		dateFormat = new SimpleDateFormat("dd MMMM yyyy'г.' HH:mm", myDateFormatSymbols);
		System.out.println("Constructor 3: " + dateFormat.format(currentDate));
		
		return (dateFormat.format(currentDate).toString());
	}
	
	public static String getShortDate() {
		Date currentDate = new Date();
		SimpleDateFormat dateFormat = null;
		DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols();

		String[] months = { "янв.", "фев.", "мар.", "апр.", "мая",
				"июня", "июля", "авг.", "сент.", "окт.", "нояб.",
				"дек." };
		myDateFormatSymbols.setMonths(months);

		dateFormat = new SimpleDateFormat("dd MMMM yyyy'г.'", myDateFormatSymbols);
		System.out.println("Constructor 3: " + dateFormat.format(currentDate));
		
		return (dateFormat.format(currentDate).toString());
	}


}
