package com.blink.utils;

public class HomePageData {
	private int projectsCount;
	private int equipmentCount;
	private int counselorsCount;
	private int areasCount;
	private int messagesUnread;
	
	public int getProjectsCount() {
		return projectsCount;
	}
	public void setProjectsCount(int projectsCount) {
		this.projectsCount = projectsCount;
	}
	public int getEquipmentCount() {
		return equipmentCount;
	}
	public void setEquipmentCount(int equipmentCount) {
		this.equipmentCount = equipmentCount;
	}
	public int getCounselorsCount() {
		return counselorsCount;
	}
	public void setCounselorsCount(int counselorsCount) {
		this.counselorsCount = counselorsCount;
	}
	public int getAreasCount() {
		return areasCount;
	}
	public void setAreasCount(int areasCount) {
		this.areasCount = areasCount;
	}
	public int getMessagesUnread() {
		return messagesUnread;
	}
	public void setMessagesUnread(int messagesUnread) {
		this.messagesUnread = messagesUnread;
	}

}
