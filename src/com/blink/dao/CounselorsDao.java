package com.blink.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.Area;
import com.blink.entity.Counselor;
import com.blink.entity.Country;
import com.blink.entity.InvitedArea;
import com.blink.entity.InvitedCounselors;

@Transactional
public class CounselorsDao {
	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Counselor> getCouselorsList() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Counselor");

		@SuppressWarnings("unchecked")
		List<Counselor> counselorsList = query.list();
		return counselorsList;
	}

	public List<Country> getCountriesList() {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Country.class)
				.createAlias("counselors", "counselor")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<Country> countriesList = cr.list();
		return countriesList;
	}

	public List<Counselor> getCountriesListByCountry(int id) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Counselor.class)
				.createAlias("country", "country")
				.add(Restrictions.eq("country.id", id));
		@SuppressWarnings("unchecked")
		List<Counselor> counselorsList = cr.list();
		return counselorsList;
	}

	@SuppressWarnings("unchecked")
	public Set<Counselor> getFoundCounselorsList(String searchText) {

		String query = "%" + searchText + "%";
		Set<Counselor> counselorsSet = new HashSet<>();

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria countryOrCompetence = currentSession.createCriteria(Counselor.class)
				.createAlias("country", "country");
		countryOrCompetence.add(Restrictions.disjunction()
				.add(Restrictions.ilike("competence", query))
				.add(Restrictions.ilike("country.countryNameRu", query)));
		
		
		List<Counselor> resultsList = countryOrCompetence.list();
		counselorsSet.addAll(resultsList);
		
		Criteria userOrTitle = currentSession.createCriteria(Counselor.class)
				.createAlias("user", "user");
		userOrTitle.add(Restrictions.disjunction()
				.add(Restrictions.ilike("title", query))
				.add(Restrictions.ilike("user.username", query)));

		resultsList =  userOrTitle.list();
		counselorsSet.addAll(resultsList);

		return counselorsSet;

	}

	public List<Counselor> getCounselorsByUserId(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Counselor.class)
				.createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId));
		@SuppressWarnings("unchecked")
		List<Counselor> counselorsList = cr.list();
		return counselorsList;
	}

	public Counselor getCounselorById(int category) {
		Session currentSession = sessionFactory.getCurrentSession();
		Counselor counselor = (Counselor) currentSession
				.createCriteria(Counselor.class)
				.add(Restrictions.eq("id", category)).uniqueResult();
		return counselor;
	}

	public boolean isInvited(int projectId, int counselorId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(InvitedCounselors.class)
				.createAlias("counselor", "counselor")
				.add(Restrictions.eq("counselor.id", counselorId))
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));
		@SuppressWarnings("unchecked")
		List<InvitedCounselors> counselorsList = cr.list();
		if (counselorsList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public void invitCounselor(InvitedCounselors invitedCounselor) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(invitedCounselor);

	}

	public void setAgreenemtDate(int projectId, int counselorId) {
		Session currentSession = sessionFactory.getCurrentSession();
		InvitedCounselors invitedCounselors = (InvitedCounselors) currentSession
				.createCriteria(InvitedCounselors.class)
				.createAlias("counselor", "counselor")
				.add(Restrictions.eq("counselor.id", counselorId))
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId)).uniqueResult();
		invitedCounselors.setAgreementDate((new Date()).toString());
		currentSession.update(invitedCounselors);
	}

	public List<InvitedCounselors> getInvitedCounselors(int projectId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(InvitedCounselors.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));
		@SuppressWarnings("unchecked")
		List<InvitedCounselors> invitedCounselorsList = cr.list();
		return invitedCounselorsList;
	}

	public void setAreaAgreenemtDate(int projectId, int areaId) {
		Session currentSession = sessionFactory.getCurrentSession();
		InvitedArea invitedArea = (InvitedArea) currentSession
				.createCriteria(InvitedArea.class).createAlias("area", "area")
				.add(Restrictions.eq("area.id", areaId))
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId)).uniqueResult();
		invitedArea.setAgreementDate((new Date()).toString());
		currentSession.update(invitedArea);
	}

	public void addNewCounselor(Counselor counselor) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(counselor);
	}

	public int getMaxId() {
		Session currentSession = sessionFactory.getCurrentSession();
		Counselor counselor = (Counselor) currentSession
				.createCriteria(Counselor.class).addOrder(Order.desc("id"))
				.setMaxResults(1).uniqueResult();

		return counselor.getId();
	}

}
