package com.blink.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;

@Transactional
public class PermissionsDao {

	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public int getLastProjectId() {
		Session currentSession = sessionFactory.getCurrentSession();
		Project project = (Project) currentSession
				.createCriteria(Project.class).addOrder(Order.desc("id"))
				.setMaxResults(1).uniqueResult();

		return project.getId();

	}

	public void addProjectsPermissions(ProjectPermissions projectPermissions) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(projectPermissions);
	}

	public List<ProjectPermissions> getUsersByProjectId(int projectId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(ProjectPermissions.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));
		@SuppressWarnings("unchecked")
		List<ProjectPermissions> projectPermissionsList = cr.list();
		return projectPermissionsList;
	}

	public List<ProjectPermissions> getPermissionsByProjectId(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(ProjectPermissions.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", id))
				.createAlias("user", "user").addOrder(Order.desc("user.id"));
		@SuppressWarnings("unchecked")
		List<ProjectPermissions> permissionsList = cr.list();
		return permissionsList;
	}

	public boolean isUserInvited(int projectId, int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(ProjectPermissions.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId))
				.createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId));
		@SuppressWarnings("unchecked")
		List<ProjectPermissions> permissionsList = cr.list();

		if (permissionsList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
}
