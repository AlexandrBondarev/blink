package com.blink.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.InvitedUsers;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.User;

@Transactional
public class UserDao {
	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void addUser(User user) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(user);
	}

	public boolean isUserRegistered(String email, String password) {

		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.createCriteria(User.class)
				.add(Restrictions.eq("email", email))
				.add(Restrictions.eq("password", password)).uniqueResult();
		return (user != null);
	}

	public boolean isAuthenticated(String userHash) {
		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.createCriteria(User.class)
				.add(Restrictions.eq("userHash", userHash)).uniqueResult();

		boolean isAuthenticated = false;

		if (user != null) {
			isAuthenticated = true;
		}

		return isAuthenticated;
	}

	public void setUserHash(String email, String userHash) {
		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.createCriteria(User.class)
				.add(Restrictions.eq("email", email)).uniqueResult();
		user.setUserHash(userHash);

	}

	public String getUserNameByHash(String userHash) {

		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.createCriteria(User.class)
				.add(Restrictions.eq("userHash", userHash)).uniqueResult();
		if (user == null) {
			return null;
		}

		return user.getUsername();
	}

	public int getUserPermissions(int userId, int projectId) {

		Project project = new Project();
		project.setId(projectId);
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria c = currentSession.createCriteria(ProjectPermissions.class);
		c.createAlias("project", "project").add(
				Restrictions.eq("project.id", projectId));
		c.createAlias("user", "user").add(Restrictions.eq("user.id", userId));
		c.addOrder(Order.desc("rights"));
		c.setProjection(Projections.min("rights"));

		Integer userPermissions = ((Integer) c.uniqueResult());
		if (userPermissions == null) {
			return 0;
		} else {
			return userPermissions;
		}

	}

	public List<User> getUsersList() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from User");
		@SuppressWarnings("unchecked")
		List<User> usersList = query.list();
		return usersList;
	}

	public int getUserId(String userHash) {

		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.createCriteria(User.class)
				.add(Restrictions.eq("userHash", userHash)).uniqueResult();
		return user.getId();
	}

	public int getUserIdByProjectId(int projectId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Project project = (Project) currentSession
				.createCriteria(Project.class)
				.add(Restrictions.eq("id", projectId)).uniqueResult();
		return project.getUser().getId();
	}

	public User getAvatar(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.createCriteria(User.class)
				.add(Restrictions.eq("id", userId)).uniqueResult();
		return user;
	}

	public User getUserById(Integer userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.createCriteria(User.class)
				.add(Restrictions.eq("id", userId)).uniqueResult();
		return user;
	}

	public void userUpdate(User userRedact) {
		Session currentSession = sessionFactory.getCurrentSession();
		User oldUser = (User) currentSession
				.get(User.class, userRedact.getId());

		oldUser.setUsername(userRedact.getUsername());
		oldUser.setEmail(userRedact.getEmail());
		oldUser.setLastName(userRedact.getLastName());
		oldUser.setFirstName(userRedact.getFirstName());
		oldUser.setMiddleName(userRedact.getMiddleName());
		oldUser.setDateOfBirth(userRedact.getDateOfBirth());
		oldUser.setOrganization(userRedact.getOrganization());
		oldUser.setPosition(userRedact.getPosition());
		oldUser.setPhoneNumber(userRedact.getPhoneNumber());

		currentSession.update(oldUser);
	}

	public void inviteUser(InvitedUsers invitedUser) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(invitedUser);

	}

	public List<InvitedUsers> getInvitedUsers(int projectId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(InvitedUsers.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));
		@SuppressWarnings("unchecked")
		List<InvitedUsers> invitedUsersList = cr.list();
		return invitedUsersList;
	}

	public void changePassword(User user) {
		Session currentSession = sessionFactory.getCurrentSession();
		User oldUser = (User) currentSession.get(User.class, user.getId());
		oldUser.setPassword(user.getPassword());
		currentSession.update(oldUser);
	}

	public void setAvatar(User user) {
		Session currentSession = sessionFactory.getCurrentSession();
		User oldUser = (User) currentSession.get(User.class, user.getId());
		oldUser.setAvatar(user.getAvatar());
		currentSession.update(oldUser);

	}

	public void deleteHash(Integer userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, userId);
		user.setUserHash(null);
		currentSession.update(user);

	}

	public String getAvatarByUserId(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, userId);
		return user.getAvatar();
	}

}