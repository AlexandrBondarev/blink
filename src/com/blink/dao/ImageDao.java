package com.blink.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.Image;

@Transactional
public class ImageDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveImage(Image image) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(image);

	}

	public List<Image> getImagesByEquipmentId(int equipmentId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Image.class)
				.createAlias("equipment", "equipment")
				.add(Restrictions.eq("equipment.id", equipmentId));
		@SuppressWarnings("unchecked")
		List<Image> imagesList = cr.list();
		return imagesList;
	}

	public List<Image> getImagesByAreaId(int areaId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Image.class)
				.createAlias("area", "area")
				.add(Restrictions.eq("area.id", areaId));
		@SuppressWarnings("unchecked")
		List<Image> imagesList = cr.list();
		return imagesList;
	}

	public void deleteImgById(int deleteId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Image image = (Image) currentSession.load(Image.class, deleteId);
		currentSession.delete(image);

	}


	public String getImageNameById(int deleteId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Image image = (Image) currentSession.load(Image.class, deleteId);
		return image.getFileName();
	}

}
