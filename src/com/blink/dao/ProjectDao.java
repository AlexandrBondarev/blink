package com.blink.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.Area;
import com.blink.entity.City;
import com.blink.entity.Counselor;
import com.blink.entity.Country;
import com.blink.entity.Equipment;
import com.blink.entity.PrivateMessage;
import com.blink.entity.Project;
import com.blink.entity.Region;
import com.blink.utils.HomePageData;

// классы *Dao служат для получения готовых Джава-сущностей из базы данных
@Transactional
public class ProjectDao {
	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Project> getProjectsList() {
		// список проектов:
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Project");
		@SuppressWarnings("unchecked")
		List<Project> projectsList = query.list();
		return projectsList;
	}

	public Project getProjectInfo(int id) {

		Session currentSession = sessionFactory.getCurrentSession();
		Project projectInfo = (Project) currentSession
				.createCriteria(Project.class).add(Restrictions.eq("id", id))
				.uniqueResult();
		return projectInfo;
	}

	public List<Country> getCountriesList() {
		Session currentSession = sessionFactory.getCurrentSession();

		Criteria cr = currentSession.createCriteria(Country.class)
				.createAlias("projects", "project")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<Country> countriesList = cr.list();
		return countriesList;

	}

	public List<Project> getSortedProjectsByCountryList(int id) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Project.class)
				.createAlias("country", "country")
				.add(Restrictions.eq("country.id", id));
		@SuppressWarnings("unchecked")
		List<Project> projectsList = cr.list();
		return projectsList;

	}

	public List<Project> getSortedProjectsByRegionList(int id) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Project.class)
				.createAlias("region", "region")
				.add(Restrictions.eq("region.id", id));
		@SuppressWarnings("unchecked")
		List<Project> projectsList = cr.list();
		return projectsList;
	}

	public List<Region> getRegionsList(int countryId) {

		Session currentSession = sessionFactory.getCurrentSession();

		Criteria cr = currentSession.createCriteria(Region.class)
				.createAlias("country", "country")
				.add(Restrictions.eq("country.id", countryId))
				.createAlias("projects", "project")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<Region> regionList = cr.list();

		return regionList;

	}

	public List<Project> getSortedProjectsByCityList(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Project.class)
				.createAlias("city", "city")
				.add(Restrictions.eq("city.id", id));
		@SuppressWarnings("unchecked")
		List<Project> projectsList = cr.list();
		return projectsList;
	}

	public List<City> getCitiesFromRegionList(int regionId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(City.class)
				.createAlias("region", "region")
				.add(Restrictions.eq("region.id", regionId))
				.createAlias("projects", "project")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<City> citiesList = cr.list();
		return citiesList;

	}

	public Set<Project> getFoundProjects(String searchText) {
		String query = "%" + searchText + "%";
		Set<Project> projects = new HashSet<>();

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria searchByProject = currentSession.createCriteria(Project.class);
		searchByProject.add(Restrictions.disjunction()
				.add(Restrictions.ilike("title", query))
				.add(Restrictions.ilike("description", query)));

		@SuppressWarnings("unchecked")
		List<Project> searchByProjectList = searchByProject.list();
		projects.addAll(searchByProjectList);

		Criteria searchByCountryCity = currentSession
				.createCriteria(Project.class).createAlias("city", "city")
				.createAlias("country", "country");
		Criterion city = Restrictions.ilike("city.cityNameRu", query);
		Criterion country = Restrictions.ilike("country.countryNameRu", query);
		LogicalExpression orExp = Restrictions.or(city, country);
		searchByCountryCity.add(orExp);

		@SuppressWarnings("unchecked")
		List<Project> searchByCountryCityList = searchByCountryCity.list();
		projects.addAll(searchByCountryCityList);

		Criteria searchByRegion = currentSession.createCriteria(Project.class)
				.createAlias("region", "region")
				.add(Restrictions.ilike("region.regionNameRu", query));
		@SuppressWarnings("unchecked")
		List<Project> searchByRegionList = searchByRegion.list();
		projects.addAll(searchByRegionList);

		return projects;
	}

	public List<Country> getAllcountriesList() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Country");
		@SuppressWarnings("unchecked")
		List<Country> countriesList = query.list();
		return countriesList;

	}

	public List<Region> getAllRegionListByCountryId(int countryId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Region.class)
				.createAlias("country", "country")
				.add(Restrictions.eq("country.id", countryId));
		@SuppressWarnings("unchecked")
		List<Region> regionList = cr.list();
		return regionList;

	}

	public List<City> getAllCitiesListByRegionId(int regionId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(City.class)
				.createAlias("region", "region")
				.add(Restrictions.eq("region.id", regionId));
		@SuppressWarnings("unchecked")
		List<City> citiesList = cr.list();
		return citiesList;

	}

	public HomePageData getHomePageData(int userId) {

		HomePageData homePageData = new HomePageData();
		Session currentSession = sessionFactory.getCurrentSession();
		Long projectsCount = (Long) currentSession
				.createCriteria(Project.class).createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId))
				.setProjection(Projections.rowCount()).uniqueResult();
		if (projectsCount == null) {
			homePageData.setProjectsCount(0);
		} else {
			homePageData.setProjectsCount(projectsCount.intValue());
		}

		Long equipmentCount = (Long) currentSession
				.createCriteria(Equipment.class).createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId))
				.setProjection(Projections.rowCount()).uniqueResult();
		if (equipmentCount == null) {
			homePageData.setEquipmentCount(0);
		} else {
			homePageData.setEquipmentCount(equipmentCount.intValue());
		}

		Long counselorsCount = (Long) currentSession
				.createCriteria(Counselor.class).createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId))
				.setProjection(Projections.rowCount()).uniqueResult();
		if (counselorsCount == null) {
			homePageData.setCounselorsCount(0);
		} else {
			homePageData.setCounselorsCount(counselorsCount.intValue());
		}

		Long areasCount = (Long) currentSession.createCriteria(Area.class)
				.createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId))
				.setProjection(Projections.rowCount()).uniqueResult();
		if (areasCount == null) {
			homePageData.setAreasCount(0);
		} else {
			homePageData.setAreasCount(areasCount.intValue());
		}

		Long messagesUnread = (Long) currentSession
				.createCriteria(PrivateMessage.class)
				.add(Restrictions.isNull("trashByReceiver"))	.add(Restrictions.isNull("deletedByReceiver"))
				.createAlias("receiver", "receiver")
				.add(Restrictions.eq("receiver.id", userId))
				.setProjection(Projections.rowCount()).uniqueResult();
		if (messagesUnread == null) {
			homePageData.setMessagesUnread(0);
		} else {
			homePageData.setMessagesUnread(messagesUnread.intValue());
		}
		return homePageData;

	}

}