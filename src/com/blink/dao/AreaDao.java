package com.blink.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.Area;
import com.blink.entity.City;
import com.blink.entity.Counselor;
import com.blink.entity.Country;
import com.blink.entity.InvitedArea;
import com.blink.entity.Region;

@Transactional
public class AreaDao {
	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Area> getAreaBeansList() {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Area.class);
		@SuppressWarnings("unchecked")
		List<Area> areasList = cr.list();
		return areasList;
	}

	public void inviteArea(InvitedArea invitedArea) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(invitedArea);
	}

	public List<InvitedArea> getAreaByProject(int projectId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(InvitedArea.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));
		@SuppressWarnings("unchecked")
		List<InvitedArea> invitedAreaList = cr.list();
		return invitedAreaList;

	}

	public List<Country> getCountriesList() {
		Session currentSession = sessionFactory.getCurrentSession();

		Criteria cr = currentSession.createCriteria(Country.class)
				.createAlias("areas", "areas")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<Country> countriesList = cr.list();
		return countriesList;

	}

	public List<Region> getRegionList(int countryId) {
		Session currentSession = sessionFactory.getCurrentSession();

		Criteria cr = currentSession.createCriteria(Region.class)
				.createAlias("country", "country")
				.add(Restrictions.eq("country.id", countryId))
				.createAlias("areas", "areas")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<Region> regionList = cr.list();
		return regionList;

	}

	public List<City> getCitiesList(int regionId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(City.class)
				.createAlias("region", "region")
				.add(Restrictions.eq("region.id", regionId))
				.createAlias("areas", "areas")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<City> citiesList = cr.list();
		return citiesList;
	}

	public List<Area> getAreasListByCountryId(int countryId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Area.class)
				.createAlias("country", "country")
				.add(Restrictions.eq("country.id", countryId));
		@SuppressWarnings("unchecked")
		List<Area> areasList = cr.list();
		return areasList;
	}

	public List<Area> getAreasListByRegionId(int regionId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Area.class)
				.createAlias("region", "region")
				.add(Restrictions.eq("region.id", regionId));
		@SuppressWarnings("unchecked")
		List<Area> areasList = cr.list();
		return areasList;
	}

	public List<Area> getAreasListByCityId(int cityId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Area.class)
				.createAlias("city", "city")
				.add(Restrictions.eq("city.id", cityId));
		@SuppressWarnings("unchecked")
		List<Area> areasList = cr.list();
		return areasList;
	}

	@SuppressWarnings("unchecked")
	public Set<Area> getFoundAreasList(String searchText) {
		String query = "%" + searchText + "%";
		Set<Area> areasSet = new HashSet<>();

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria searchByAreas = currentSession.createCriteria(Area.class)
				.createAlias("region", "region");
		searchByAreas.add(Restrictions.disjunction()
				.add(Restrictions.ilike("region.regionNameRu", query))
				.add(Restrictions.ilike("description", query)));

		List<Area> searchByAreasList = searchByAreas.list();
		areasSet.addAll(searchByAreasList);

		Criteria searchByCountryCity = currentSession
				.createCriteria(Area.class).createAlias("city", "city")
				.createAlias("country", "country");
		Criterion city = Restrictions.ilike("city.cityNameRu", query);
		Criterion country = Restrictions.ilike("country.countryNameRu", query);
		LogicalExpression orExp = Restrictions.or(city, country);
		searchByCountryCity.add(orExp);

		List<Area> searchByCountryCityList = searchByCountryCity.list();
		areasSet.addAll(searchByCountryCityList);

		int areaId = -1;
		try {
			areaId = Integer.valueOf(searchText);
		} catch (NumberFormatException e) {
		}
		Area area = (Area) currentSession.createCriteria(Area.class)
				.add(Restrictions.eq("id", areaId)).uniqueResult();

		areasSet.add(area);

		Criteria searchIdOrTitle = currentSession.createCriteria(Area.class)
				.createAlias("user", "user");
		Criterion id = Restrictions.ilike("user.username", query);
		Criterion title = Restrictions.ilike("title", query);
		LogicalExpression idOrTitle = Restrictions.or(id, title);
		searchIdOrTitle.add(idOrTitle);

		List<Area> result = searchIdOrTitle.list();
		areasSet.addAll(result);

		return areasSet;

	}

	public Area getAreaById(int areaId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Area area = (Area) currentSession.createCriteria(Area.class)
				.add(Restrictions.eq("id", areaId)).uniqueResult();
		return area;
	}

	public boolean isInvited(int projectId, int areaId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(InvitedArea.class)
				.createAlias("area", "area")
				.add(Restrictions.eq("area.id", areaId))
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));

		@SuppressWarnings("unchecked")
		List<InvitedArea> areasList = cr.list();
		if (areasList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Area> getAreasByUserId(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Area.class)
				.createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId));
		List<Area> areasList = cr.list();
		return areasList;
	}

	public List<InvitedArea> getInvitedArea(int projectId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(InvitedArea.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));
		@SuppressWarnings("unchecked")
		List<InvitedArea> invitedAreaList = cr.list();
		return invitedAreaList;
	}

	public void addNewArea(Area newArea) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(newArea);
	}

	public int getMaxId() {
		Session currentSession = sessionFactory.getCurrentSession();
		Area area = (Area) currentSession
				.createCriteria(Area.class).addOrder(Order.desc("id"))
				.setMaxResults(1).uniqueResult();

		return area.getId();
	}
}