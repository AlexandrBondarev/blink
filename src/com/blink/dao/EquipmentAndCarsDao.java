package com.blink.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.City;
import com.blink.entity.Country;
import com.blink.entity.Equipment;
import com.blink.entity.InvitedEquipment;
import com.blink.entity.Region;

@Transactional
public class EquipmentAndCarsDao {
	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Equipment> getEquipmentAndCarsList() {

		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Equipment");
		@SuppressWarnings("unchecked")
		List<Equipment> equipmentList = query.list();
		return equipmentList;
	}

	public List<Equipment> getSortedEquipmentAndCarsList(int id) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Equipment.class)
				.createAlias("country", "country")
				.add(Restrictions.eq("country.id", id));
		@SuppressWarnings("unchecked")
		List<Equipment> equipmentList = cr.list();
		return equipmentList;
	}

	public List<Country> getCountrysList() {
		Session currentSession = sessionFactory.getCurrentSession();

		Criteria cr = currentSession.createCriteria(Country.class)
				.createAlias("equipments", "equipment")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<Country> countriesList = cr.list();
		return countriesList;

	}

	public List<Region> getRegionList(int countryId) {

		Session currentSession = sessionFactory.getCurrentSession();

		Criteria cr = currentSession.createCriteria(Region.class)
				.createAlias("country", "country")
				.add(Restrictions.eq("country.id", countryId))
				.createAlias("equipments", "equioment")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<Region> regionList = cr.list();

		return regionList;
	}

	public List<City> getCitiesList(int regionId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(City.class)
				.createAlias("region", "region")
				.add(Restrictions.eq("region.id", regionId))
				.createAlias("equipments", "equipment")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<City> citiesList = cr.list();
		return citiesList;

	}

	public List<Equipment> getSortedEquipmentAndCarsListByRegion(int regionId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Equipment.class)
				.createAlias("region", "region")
				.add(Restrictions.eq("region.id", regionId));
		@SuppressWarnings("unchecked")
		List<Equipment> equipmentList = cr.list();
		return equipmentList;
	}

	public List<Equipment> getSortedEquipmentAndCarsListByCity(int cityId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Equipment.class)
				.createAlias("city", "city")
				.add(Restrictions.eq("city.id", cityId));
		@SuppressWarnings("unchecked")
		List<Equipment> equipmentList = cr.list();
		return equipmentList;
	}

	public List<Equipment> getEquipmentsByUserId(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Equipment.class)
				.createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId));
		@SuppressWarnings("unchecked")
		List<Equipment> equipmentList = cr.list();
		return equipmentList;
	}

	public Equipment getEquipmentById(int equipmentId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Equipment equipment = (Equipment) currentSession
				.createCriteria(Equipment.class)
				.add(Restrictions.eq("id", equipmentId)).uniqueResult();
		return equipment;
	}

	public void addNewEquipment(Equipment newEquipment) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(newEquipment);

	}

	public void equipmentUpdate(Equipment equipment) {
		Session currentSession = sessionFactory.getCurrentSession();
		Equipment oldEquipment = (Equipment) currentSession.get(
				Equipment.class, equipment.getId());
		oldEquipment.setTitle(equipment.getTitle());
		oldEquipment.setDescription(equipment.getDescription());
		oldEquipment.setLastUpdate(equipment.getLastUpdate());
		currentSession.update(oldEquipment);

	}

	public boolean isInvited(int projectId, int equipmentId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(InvitedEquipment.class)
				.createAlias("equipment", "equipment")
				.add(Restrictions.eq("equipment.id", equipmentId))
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));
		@SuppressWarnings("unchecked")
		List<InvitedEquipment> equipmentsList = cr.list();
		if (equipmentsList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public void invitEquipment(InvitedEquipment invitedEquipment) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(invitedEquipment);

	}

	public void setAgreementDate(int projectId, int equipmentId) {
		Session currentSession = sessionFactory.getCurrentSession();
		InvitedEquipment invitedEquipment = (InvitedEquipment) currentSession
				.createCriteria(InvitedEquipment.class)
				.createAlias("equipment", "equipment")
				.add(Restrictions.eq("equipment.id", equipmentId))
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId)).uniqueResult();
		if (invitedEquipment.getAgreementDate() == null) {
			invitedEquipment.setAgreementDate((new Date()).toString());
		}
		currentSession.update(invitedEquipment);

	}

	public List<InvitedEquipment> getInvitedEquipment(int projectId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(InvitedEquipment.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", projectId));
		@SuppressWarnings("unchecked")
		List<InvitedEquipment> invitedEquipmentList = cr.list();
		return invitedEquipmentList;

	}

	public Set<Equipment> getFoundEquipmentsList(String searchText) {
		String query = "%" + searchText + "%";
		Set<Equipment> equipmentsSet = new HashSet<>();

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria searchByEquipments = currentSession.createCriteria(
				Equipment.class).createAlias("region", "region");
		searchByEquipments.add(Restrictions.disjunction()
				.add(Restrictions.ilike("region.regionNameRu", query))
				.add(Restrictions.ilike("description", query)));

		@SuppressWarnings("unchecked")
		List<Equipment> searchByAreasList = searchByEquipments.list();
		equipmentsSet.addAll(searchByAreasList);

		Criteria searchByCountryCity = currentSession
				.createCriteria(Equipment.class).createAlias("city", "city")
				.createAlias("country", "country");
		Criterion city = Restrictions.ilike("city.cityNameRu", query);
		Criterion country = Restrictions.ilike("country.countryNameRu", query);
		LogicalExpression orExp = Restrictions.or(city, country);
		searchByCountryCity.add(orExp);

		@SuppressWarnings("unchecked")
		List<Equipment> searchByCountryCityList = searchByCountryCity.list();
		equipmentsSet.addAll(searchByCountryCityList);

		int equipmentId = -1;
		try {
			equipmentId = Integer.valueOf(searchText);
		} catch (NumberFormatException e) {
		}
		Equipment equipment = (Equipment) currentSession
				.createCriteria(Equipment.class)
				.add(Restrictions.eq("id", equipmentId)).uniqueResult();

		equipmentsSet.add(equipment);

		Criteria searchIdOrTitle = currentSession.createCriteria(
				Equipment.class).createAlias("user", "user");
		Criterion id = Restrictions.ilike("user.username", query);
		Criterion title = Restrictions.ilike("title", query);
		LogicalExpression idOrTitle = Restrictions.or(id, title);
		searchIdOrTitle.add(idOrTitle);

		@SuppressWarnings("unchecked")
		List<Equipment> result = searchIdOrTitle.list();
		equipmentsSet.addAll(result);

		return equipmentsSet;

	}
}
