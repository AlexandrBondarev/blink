package com.blink.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.City;
import com.blink.entity.Country;
import com.blink.entity.Messages;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.Region;
import com.blink.utils.Utils;

@Transactional
public class ProjectDetailsDao {

	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Messages> getMessagesList(int id) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Messages.class)
				.createAlias("project", "project")
				.add(Restrictions.eq("project.id", id));
		@SuppressWarnings("unchecked")
		List<Messages> messagesList = cr.list();
		return messagesList;

	}

	public void addNewMessage(Messages message) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(message);
		int projectId = message.getProject().getId();
		Project project = (Project) currentSession
				.get(Project.class, projectId);
		// если добавили сообщенние, увеличиваем счетчик сообщений и обновляем
		// дату
		Integer messagesCount = project.getMessagesCount();
		if (messagesCount == null) {
			messagesCount = 1;
		} else {
			messagesCount = messagesCount + 1;
		}
		project.setMessagesCount(messagesCount);
		project.setLastUpdate(message.getCreationDate());

	}

	public void addNewProject(Project newProject, int countryId, int regionId,
			int cityId) {
		Session currentSession = sessionFactory.getCurrentSession();
		newProject.setCreationDate(Utils.getShortDate());

		Country country = new Country();
		country.setId(countryId);

		Region region = new Region();
		region.setId(regionId);

		City city = new City();
		city.setId(cityId);

		newProject.setCountry(country);
		newProject.setRegion(region);
		newProject.setCity(city);

		currentSession.save(newProject);
	}

	public List<Project> getProjectsByUserId(int userId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(Project.class)
				.createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId));
		@SuppressWarnings("unchecked")
		List<Project> projectsList = cr.list();
		return projectsList;
	}

	public List<ProjectPermissions> getProjectsPermissions(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(ProjectPermissions.class)
				.createAlias("user", "user")
				.add(Restrictions.eq("user.id", userId))
				.createAlias("project", "project")
				.addOrder(Order.asc("project.id"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<ProjectPermissions> projectsPermissionsList = cr.list();
		return projectsPermissionsList;
	}

}
