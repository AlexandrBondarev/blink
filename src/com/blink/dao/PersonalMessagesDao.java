package com.blink.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.blink.entity.PrivateMessage;
import com.blink.utils.Utils;

@Transactional
public class PersonalMessagesDao {
	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private SessionFactory sessionFactory;

	public enum notRead {
		projectApplication, areaApplication, counselorsApplication, total;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<PrivateMessage> getPrivateMessagesList(int userId) {

		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(PrivateMessage.class)
				.add(Restrictions.isNull("trashByReceiver"))
				.add(Restrictions.isNull("deletedByReceiver"))
				.addOrder(Order.desc("id")).createAlias("receiver", "receiver")
				.add(Restrictions.eq("receiver.id", userId));
		@SuppressWarnings("unchecked")
		List<PrivateMessage> privateMessagesList = cr.list();
		return privateMessagesList;
	}

	public PrivateMessage getConcreteProjectInvite(int messageId) {

		Session currentSession = sessionFactory.getCurrentSession();
		PrivateMessage privateMessage = (PrivateMessage) currentSession
				.createCriteria(PrivateMessage.class)
				.add(Restrictions.eq("id", messageId)).uniqueResult();

		return privateMessage;

	}

	public void addNewMessage(PrivateMessage privateMessage) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(privateMessage);
	}

	public void addProjectInvitation(PrivateMessage privateMessage) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(privateMessage);
	}

	public PrivateMessage getMessageById(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		PrivateMessage privateMessage = (PrivateMessage) currentSession
				.createCriteria(PrivateMessage.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		return privateMessage;
	}

	public void deleteMessage(PrivateMessage privateMessage) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.delete(privateMessage);

	}

	public void deleteMessageById(int deleteId) {
		Session currentSession = sessionFactory.getCurrentSession();
		PrivateMessage privateMessage = (PrivateMessage) currentSession.load(
				PrivateMessage.class, deleteId);
		sessionFactory.getCurrentSession().delete(privateMessage);

	}

	public List<PrivateMessage> getOutBox(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Criteria cr = currentSession.createCriteria(PrivateMessage.class)
				.add(Restrictions.isNull("trashBySender"))
				.addOrder(Order.desc("id")).createAlias("sender", "sender")
				.add(Restrictions.eq("sender.id", userId))
				.add(Restrictions.eq("type", 2));
		@SuppressWarnings("unchecked")
		List<PrivateMessage> privateMessagesList = cr.list();
		return privateMessagesList;
	}

	public void trashByReceiver(int privateDeleteId) {
		Session currentSession = sessionFactory.getCurrentSession();
		PrivateMessage privateMessage = (PrivateMessage) currentSession.get(
				PrivateMessage.class, privateDeleteId);
		privateMessage.setTrashByReceiver(Utils.getShortDate());
		currentSession.update(privateMessage);
	}

	public List<PrivateMessage> getDeletedBox(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();

		Criteria criteria = currentSession.createCriteria(PrivateMessage.class)
				.addOrder(Order.desc("id")).createAlias("receiver", "receiver")
				.add(Restrictions.eq("receiver.id", userId));

		Criterion rest1 = Restrictions.and(
				Restrictions.isNotNull("trashByReceiver"),
				Restrictions.isNull("deletedByReceiver"));

		criteria.add(rest1);

		@SuppressWarnings("unchecked")
		List<PrivateMessage> privateMessagesList = criteria.list();

		criteria = currentSession.createCriteria(PrivateMessage.class)
				.addOrder(Order.desc("id")).createAlias("sender", "sender")
				.add(Restrictions.eq("sender.id", userId));

		rest1 = Restrictions.and(Restrictions.isNotNull("trashBySender"),
				Restrictions.isNull("deletedBySender"));

		criteria.add(rest1);

		@SuppressWarnings("unchecked")
		List<PrivateMessage> privateMessagesList2 = criteria.list();
		privateMessagesList.addAll(privateMessagesList2);

		return privateMessagesList;

	}

	public void deletedBySender(int deleteId) {
		Session currentSession = sessionFactory.getCurrentSession();
		PrivateMessage privateMessage = (PrivateMessage) currentSession.get(
				PrivateMessage.class, deleteId);

		if (privateMessage.getDeletedByReceiver() == null) {

			privateMessage.setDeletedBySender(Utils.getShortDate());
			currentSession.update(privateMessage);
		} else {
			deleteMessageById(deleteId);

		}

	}

	public void deletedByReceiver(int deletedByReceiver) {
		Session currentSession = sessionFactory.getCurrentSession();
		PrivateMessage privateMessage = (PrivateMessage) currentSession.get(
				PrivateMessage.class, deletedByReceiver);

		if (privateMessage.getDeletedBySender() == null) {

			privateMessage.setDeletedByReceiver(Utils.getShortDate());
			privateMessage.setTrashByReceiver(Utils.getShortDate());
			currentSession.update(privateMessage);
		} else {
			deleteMessageById(deletedByReceiver);
		}

	}

	public void trashBySender(int trashId) {
		Session currentSession = sessionFactory.getCurrentSession();
		PrivateMessage privateMessage = (PrivateMessage) currentSession.get(
				PrivateMessage.class, trashId);
		privateMessage.setTrashBySender(Utils.getShortDate());
		currentSession.update(privateMessage);

	}

}
