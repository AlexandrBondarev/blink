package com.blink.managers;

import java.util.List;

import com.blink.dao.PermissionsDao;
import com.blink.entity.ProjectPermissions;

public class PermissionsManager {

	private PermissionsDao permissionsDao;
	
	private static PermissionsManager instance;

	public static PermissionsManager getInstance() {
		return instance;
	}

	public PermissionsManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setPermissionsDao(PermissionsDao permissionsDao) {
		this.permissionsDao = permissionsDao;
	}

	public void addProjectsPermissions(ProjectPermissions projectPermissions) {
		permissionsDao.addProjectsPermissions(projectPermissions);

	}

	public int getLastProjectId() {
		return permissionsDao.getLastProjectId();
	}

	public List<ProjectPermissions> getUsersByProject(int projectId) {
		return permissionsDao.getUsersByProjectId(projectId);
	}

}
