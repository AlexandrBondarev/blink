package com.blink.managers;

import java.util.List;
import java.util.Set;

import com.blink.dao.ProjectDao;
import com.blink.dao.ProjectDetailsDao;
import com.blink.entity.City;
//классы *Manager  являют собой ядра сайта (API) в которых заключенны частные действия
//получить список пользователей, авторизировать пользователя,, получит список заказов.
//изменить, удалить, показать что-то.
import com.blink.entity.Country;
import com.blink.entity.Project;
import com.blink.entity.Region;
import com.blink.utils.HomePageData;

public class ProjectManager {
	private ProjectDao projectDao;
	@SuppressWarnings("unused")
	private ProjectDetailsDao projectDetailsDao;
	private static ProjectManager instance;

	public static ProjectManager getInstance() {
		return instance;
	}

	public ProjectManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setProjectDetailsDao(ProjectDetailsDao projectDetailsDao) {
		this.projectDetailsDao = projectDetailsDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public List<Project> getProjectsList() {
		return projectDao.getProjectsList();
	}

	public Project getProjectInfo(int id) {
		return projectDao.getProjectInfo(id);
	}

	public List<Country> getCountriesList() {
		return projectDao.getCountriesList();

	}

	public List<Project> getProjectsListByCountryId(int id) {
		return projectDao.getSortedProjectsByCountryList(id);
	}

	public List<Project> getProjectsListByRegionId(int regionId) {
		return projectDao.getSortedProjectsByRegionList(regionId);
	}

	public List<Region> getRegionList(int countryId) {
		return projectDao.getRegionsList(countryId);
	}

	public List<Project> getProjectsListByCityId(int cityId) {
		return projectDao.getSortedProjectsByCityList(cityId);
	}

	public List<City> getCitiesList(int regionId) {
		return projectDao.getCitiesFromRegionList(regionId);
	}

	public Set<Project> getFoundProjectsList(String lookingFor) {
		return projectDao.getFoundProjects(lookingFor);
	}

	public List<Country> getAllcountriesList() {
		return projectDao.getAllcountriesList();

	}

	public List<Region> getAllRegionListByCountryId(int countryId) {
		return projectDao.getAllRegionListByCountryId(countryId);
	}

	public List<City> getAllCitiesListByRegionId(int regionId) {
		return projectDao.getAllCitiesListByRegionId(regionId);
	}

	public HomePageData getHomePageData(int userId) {
		return projectDao.getHomePageData(userId);
	}

}
