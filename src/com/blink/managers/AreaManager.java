package com.blink.managers;

import java.util.List;
import java.util.Set;

import com.blink.dao.AreaDao;
import com.blink.entity.Area;
import com.blink.entity.City;
import com.blink.entity.Country;
import com.blink.entity.InvitedArea;
import com.blink.entity.Project;
import com.blink.entity.Region;

public class AreaManager {

	private AreaDao areaDao;
	private static AreaManager instance;

	public static AreaManager getInstance() {
		return instance;
	}

	public AreaManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}

	public List<Area> getAreaBeansList() {
		return areaDao.getAreaBeansList();
	}

	public void invitArea(int projectId, int areaId) {
		Area area = new Area();
		area.setId(areaId);
		InvitedArea invitedArea = new InvitedArea();
		invitedArea.setArea(area);
		Project project = new Project();
		project.setId(projectId);
		invitedArea.setProject(project);

		areaDao.inviteArea(invitedArea);
	}

	public List<InvitedArea> getAreaByProject(int projectId) {
		return areaDao.getAreaByProject(projectId);
	}

	public List<Country> getCountriesList() {
		return areaDao.getCountriesList();
	}

	public List<Region> getRegionList(int countryId) {
		return areaDao.getRegionList(countryId);
	}

	public List<City> getCitiesList(int regionId) {
		return areaDao.getCitiesList(regionId);
	}

	public List<Area> getAreasListByCountryId(int countryId) {
		return areaDao.getAreasListByCountryId(countryId);
	}

	public List<Area> getAreasListByRegionId(int regionId) {
		return areaDao.getAreasListByRegionId(regionId);
	}

	public List<Area> getAreasListByCityId(int cityId) {
		return areaDao.getAreasListByCityId(cityId);
	}

	public Set<Area> getFoundAreasList(String searchText) {
		return areaDao.getFoundAreasList(searchText);
	}

	public Area getAreaById(int areaId) {
		return areaDao.getAreaById(areaId);
	}

	public boolean isInvited(int projectId, int areaId) {
		
		return areaDao.isInvited(projectId,  areaId);
	}

	public List<Area> getAreasByUserId(int userId) {
		return areaDao.getAreasByUserId(userId);
	}

	public List<InvitedArea> getInvitedArea(int projectId) {
		return areaDao.getInvitedArea(projectId);
	}

	public void addNewArea(Area newArea) {
		 areaDao.addNewArea(newArea);
		
	}

	public int getMaxId() {
		return areaDao.getMaxId();
	}
}
