package com.blink.managers;

import java.util.Date;
import java.util.List;

import com.blink.dao.AreaDao;
import com.blink.dao.CounselorsDao;
import com.blink.dao.EquipmentAndCarsDao;
import com.blink.dao.PermissionsDao;
import com.blink.dao.PersonalMessagesDao;
import com.blink.dao.UserDao;
import com.blink.entity.InvitedArea;
import com.blink.entity.InvitedCounselors;
import com.blink.entity.InvitedEquipment;
import com.blink.entity.InvitedUsers;
import com.blink.entity.PrivateMessage;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.User;
import com.blink.utils.Utils;

public class PersonalMessagesManager {
	private UserDao userDao;
	private PermissionsDao permissionsDao;
	private CounselorsDao counselorsDao;
	private EquipmentAndCarsDao equipmentAndCarsDao;
	private PersonalMessagesDao personalMessagesDao;
	private AreaDao areaDao;
	private static PersonalMessagesManager instance;

	public static PersonalMessagesManager getInstance() {
		return instance;
	}

	public PersonalMessagesManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}

	public void setEquipmentAndCarsDao(EquipmentAndCarsDao equipmentAndCarsDao) {
		this.equipmentAndCarsDao = equipmentAndCarsDao;
	}

	public void setCounselorsDao(CounselorsDao counselorsDao) {
		this.counselorsDao = counselorsDao;
	}

	public void setPermissionsDao(PermissionsDao permissionsDao) {
		this.permissionsDao = permissionsDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setPersonalMessagesDao(PersonalMessagesDao personalMessagesDao) {
		this.personalMessagesDao = personalMessagesDao;
	}

	public List<PrivateMessage> getPrivateMessagesList(int userId) {
		return personalMessagesDao.getPrivateMessagesList(userId);
	}

	public PrivateMessage getConcreteProjectInvite(int messageId) {
		return personalMessagesDao.getConcreteProjectInvite(messageId);
	}

	public void addNewMessage(PrivateMessage privateMessage) {
		personalMessagesDao.addNewMessage(privateMessage);

	}

	public void addProjectInvitation(PrivateMessage privateMessage) {
		personalMessagesDao.addProjectInvitation(privateMessage);

	}

	public void counselorAgree(int counselorAgree) {

		PrivateMessage privateMessage = personalMessagesDao
				.getMessageById(counselorAgree);

		int projectId = privateMessage.getProject().getId();
		int counselorId = privateMessage.getCounselor().getId();

		List<ProjectPermissions> permissionsList = permissionsDao
				.getPermissionsByProjectId(projectId);
		counselorsDao.setAgreenemtDate(projectId, counselorId);

		for (ProjectPermissions each : permissionsList) {
			PrivateMessage newMessage = new PrivateMessage();
			newMessage.setProject(privateMessage.getProject());
			newMessage.setCounselor(privateMessage.getCounselor());
			// newMessage.setReceiver(privateMessage.getSender());
			newMessage.setSender(privateMessage.getReceiver());
			newMessage.setCreationDate((new Date()).toString());
			newMessage.setType(1);
			newMessage.setReceiver(each.getUser());
			personalMessagesDao.addNewMessage(newMessage);
		}
		personalMessagesDao.deleteMessage(privateMessage);
	}

	public void areaAgree(int areaAgree) {
		PrivateMessage privateMessage = personalMessagesDao
				.getMessageById(areaAgree);

		int projectId = privateMessage.getProject().getId();
		int areatId = privateMessage.getArea().getId();

		List<ProjectPermissions> permissionsList = permissionsDao
				.getPermissionsByProjectId(projectId);
		counselorsDao.setAreaAgreenemtDate(projectId, areatId);

		for (ProjectPermissions each : permissionsList) {
			PrivateMessage newMessage = new PrivateMessage();
			newMessage.setProject(privateMessage.getProject());
			newMessage.setArea(privateMessage.getArea());
			// newMessage.setReceiver(privateMessage.getSender());
			newMessage.setSender(privateMessage.getSender());
			newMessage.setCreationDate((new Date()).toString());
			newMessage.setType(1);
			newMessage.setReceiver(each.getUser());
			personalMessagesDao.addNewMessage(newMessage);
		}
		personalMessagesDao.deleteMessage(privateMessage);

	}

	public void equipmentAgree(int equipmentAgree) {
		PrivateMessage privateMessage = personalMessagesDao
				.getMessageById(equipmentAgree);

		if (privateMessage == null) {
			return;
		}
		int projectId = privateMessage.getProject().getId();
		int equipmentId = privateMessage.getEquipment().getId();

		List<ProjectPermissions> permissionsList = permissionsDao
				.getPermissionsByProjectId(projectId);

		equipmentAndCarsDao.setAgreementDate(projectId, equipmentId);

		for (ProjectPermissions each : permissionsList) {
			PrivateMessage newMessage = new PrivateMessage();
			newMessage.setProject(privateMessage.getProject());
			newMessage.setEquipment(privateMessage.getEquipment());
			// newMessage.setReceiver(privateMessage.getSender());
			newMessage.setSender(privateMessage.getSender());
			newMessage.setCreationDate((new Date()).toString());
			newMessage.setType(1);
			newMessage.setReceiver(each.getUser());
			personalMessagesDao.addNewMessage(newMessage);
		}
		personalMessagesDao.deleteMessage(privateMessage);

	}

	public void deleteMessage(int deleteId) {
		personalMessagesDao.trashByReceiver(deleteId);

	}

	public void counselorInvite(int counselorInvite) {
		PrivateMessage privateMessage = personalMessagesDao
				.getMessageById(counselorInvite);

		int projectId = privateMessage.getProject().getId();
		int userId = privateMessage.getSender().getId();
		boolean isInvited = permissionsDao.isUserInvited(projectId, userId);
		if (!isInvited) {

			ProjectPermissions permission = new ProjectPermissions();
			Project project = new Project();
			project.setId(projectId);
			permission.setProject(project);

			User user = new User();
			user.setId(privateMessage.getSender().getId());
			permission.setUser(user);

			permission.setRights(3);
			PermissionsManager.getInstance().addProjectsPermissions(permission);

			List<ProjectPermissions> permissionsList = permissionsDao
					.getPermissionsByProjectId(projectId);

			InvitedCounselors invitedCounselor = new InvitedCounselors();
			invitedCounselor.setProject(privateMessage.getProject());
			invitedCounselor.setCounselor(privateMessage.getCounselor());
			invitedCounselor.setAgreementDate(Utils.getShortDate());
			invitedCounselor
					.setInvitationDate(privateMessage.getCreationDate());
			counselorsDao.invitCounselor(invitedCounselor);

			for (ProjectPermissions each : permissionsList) {
				PrivateMessage newMessage = new PrivateMessage();
				newMessage.setProject(privateMessage.getProject());
				newMessage.setCounselor(privateMessage.getCounselor());
				newMessage.setSender(privateMessage.getReceiver());
				newMessage.setCreationDate((new Date()).toString());
				newMessage.setType(3);
				newMessage.setReceiver(each.getUser());
				personalMessagesDao.addNewMessage(newMessage);
			}
		}
		personalMessagesDao.deletedByReceiver(privateMessage.getId());

	}

	public void areaInvite(int areaInvite) {

		PrivateMessage privateMessage = personalMessagesDao
				.getMessageById(areaInvite);

		int projectId = privateMessage.getProject().getId();
		int userId = privateMessage.getSender().getId();
		boolean isInvited = permissionsDao.isUserInvited(projectId, userId);
		if (!isInvited) {

			ProjectPermissions permission = new ProjectPermissions();

			Project project = new Project();
			project.setId(projectId);
			permission.setProject(project);

			User user = new User();
			user.setId(privateMessage.getSender().getId());
			permission.setUser(user);

			permission.setRights(2);
			PermissionsManager.getInstance().addProjectsPermissions(permission);

			List<ProjectPermissions> permissionsList = permissionsDao
					.getPermissionsByProjectId(projectId);

			InvitedArea invitedArea = new InvitedArea();
			invitedArea.setProject(privateMessage.getProject());
			invitedArea.setArea(privateMessage.getArea());
			invitedArea.setAgreementDate(Utils.getShortDate());
			areaDao.inviteArea(invitedArea);

			for (ProjectPermissions each : permissionsList) {
				PrivateMessage newMessage = new PrivateMessage();
				newMessage.setProject(privateMessage.getProject());
				newMessage.setArea(privateMessage.getArea());
				newMessage.setSender(privateMessage.getSender());
				newMessage.setCreationDate((new Date()).toString());
				newMessage.setType(3);
				newMessage.setReceiver(each.getUser());
				personalMessagesDao.addNewMessage(newMessage);
			}
		}
		personalMessagesDao.deletedByReceiver(privateMessage.getId());

	}

	public void equipmentInvite(int equipmentInvite) {
		PrivateMessage privateMessage = personalMessagesDao
				.getMessageById(equipmentInvite);
		int projectId = privateMessage.getProject().getId();
		int userId = privateMessage.getSender().getId();
		boolean isInvited = permissionsDao.isUserInvited(projectId, userId);
		if (!isInvited) {

			ProjectPermissions permission = new ProjectPermissions();

			Project project = new Project();
			project.setId(projectId);
			permission.setProject(project);

			User user = new User();
			user.setId(privateMessage.getSender().getId());
			permission.setUser(user);

			permission.setRights(4);
			PermissionsManager.getInstance().addProjectsPermissions(permission);

			List<ProjectPermissions> permissionsList = permissionsDao
					.getPermissionsByProjectId(projectId);

			InvitedEquipment invitedEquipment = new InvitedEquipment();
			invitedEquipment.setProject(privateMessage.getProject());
			invitedEquipment.setEquipment(privateMessage.getEquipment());
			invitedEquipment.setAgreementDate(Utils.getShortDate());
			equipmentAndCarsDao.invitEquipment(invitedEquipment);

			for (ProjectPermissions each : permissionsList) {
				PrivateMessage newMessage = new PrivateMessage();
				newMessage.setProject(privateMessage.getProject());
				newMessage.setEquipment(privateMessage.getEquipment());
				newMessage.setReceiver(privateMessage.getReceiver());
				newMessage.setSender(privateMessage.getSender());
				newMessage.setCreationDate((new Date()).toString());
				newMessage.setType(3);
				newMessage.setReceiver(each.getUser());
				personalMessagesDao.addNewMessage(newMessage);
			}
		}
		personalMessagesDao.deletedByReceiver(privateMessage.getId());

	}

	public void userInvite(int userInvite) {
		PrivateMessage privateMessage = personalMessagesDao
				.getMessageById(userInvite);
		int projectId = privateMessage.getProject().getId();
		int userId = privateMessage.getSender().getId();

		boolean isInvited = permissionsDao.isUserInvited(projectId, userId);
		if (!isInvited) {

			ProjectPermissions permission = new ProjectPermissions();

			Project project = new Project();
			project.setId(projectId);
			permission.setProject(project);

			User user = new User();
			user.setId(userId);
			permission.setUser(user);

			permission.setRights(4);
			PermissionsManager.getInstance().addProjectsPermissions(permission);

			List<ProjectPermissions> permissionsList = permissionsDao
					.getPermissionsByProjectId(projectId);

			InvitedUsers invitedUser = new InvitedUsers();
			invitedUser.setProject(privateMessage.getProject());
			invitedUser.setUser(privateMessage.getSender());
			invitedUser.setAgreementDate(Utils.getShortDate());
			userDao.inviteUser(invitedUser);

			for (ProjectPermissions each : permissionsList) {
				PrivateMessage newMessage = new PrivateMessage();
				newMessage.setProject(privateMessage.getProject());
				newMessage.setEquipment(privateMessage.getEquipment());
				newMessage.setReceiver(privateMessage.getReceiver());
				newMessage.setSender(privateMessage.getSender());
				newMessage.setCreationDate((new Date()).toString());
				newMessage.setType(3);
				newMessage.setReceiver(each.getUser());
				personalMessagesDao.addNewMessage(newMessage);
			}
		}
		personalMessagesDao.trashByReceiver(privateMessage.getId());

	}

	public List<PrivateMessage> getOutBox(int userId) {
		return personalMessagesDao.getOutBox(userId);
	}

	public void trashByReceiver(int privateDeleteId) {
		personalMessagesDao.trashByReceiver(privateDeleteId);

	}

	public List<PrivateMessage> getDeletedBox(int userId) {
		return personalMessagesDao.getDeletedBox(userId);
	}

	public void trashBySender(int deleteId) {
		personalMessagesDao.trashBySender(deleteId);

	}

	public void deletedBySender(int deleteId) {
		personalMessagesDao.deletedBySender(deleteId);

	}

	public void deletedByReceiver(int deletedByReceiver) {
		personalMessagesDao.deletedByReceiver(deletedByReceiver);

	}

}
