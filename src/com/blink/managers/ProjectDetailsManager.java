package com.blink.managers;

import java.util.Date;
import java.util.List;

import com.blink.dao.ProjectDetailsDao;
import com.blink.dao.UserDao;
import com.blink.entity.Messages;
import com.blink.entity.Project;
import com.blink.entity.ProjectPermissions;
import com.blink.entity.User;

public class ProjectDetailsManager {
	private ProjectDetailsDao projectDetailsDao;
	private UserDao userDao;
	private static ProjectDetailsManager instance;

	public static ProjectDetailsManager getInstance() {
		return instance;
	}

	public ProjectDetailsManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setProjectDetailsDao(ProjectDetailsDao projectDetailsDao) {
		this.projectDetailsDao = projectDetailsDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public List<Messages> getMessagesList(int id) {
		return projectDetailsDao.getMessagesList(id);
	}

	public void addNewMessage(int userId, String newMessage, int id) {

		User sender = new User();
		sender.setId(userId);
		Messages message = new Messages();
		message.setSender(sender);

		Project project = new Project();
		project.setId(id);
		message.setProject(project);

		message.setMessage(newMessage);
		message.setCreationDate(new Date().toString());
		projectDetailsDao.addNewMessage(message);
	}

	public List<Project> getProjectsByUserId(int userId) {
		return projectDetailsDao.getProjectsByUserId(userId);
	}

	public int getUserId(String userHash) {
		return userDao.getUserId(userHash);
	}

	public void addNewProject(Project newProject, int countryId, int regionId,
			int cityId) {
		projectDetailsDao
				.addNewProject(newProject, countryId, regionId, cityId);
	}

	public List<ProjectPermissions> getProjectsPermissions(int userId) {

		return projectDetailsDao.getProjectsPermissions(userId);
	}

}
