package com.blink.managers;

import java.util.List;
import java.util.UUID;

import com.blink.dao.UserDao;
import com.blink.entity.InvitedUsers;
import com.blink.entity.User;

public class UserManager {

	private UserDao userDao;
	private static UserManager instance;

	public static UserManager getInstance() {
		return instance;
	}

	public UserManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void addUser(User user) {
		userDao.addUser(user);
	}

	public boolean isUserRegistered(String email, String password) {
		return userDao.isUserRegistered(email, password);
	}

	public boolean isAuthenticated(String userHash) {
		return userDao.isAuthenticated(userHash);
	}

	public String getUserHash(String email) {
		String userhash = UUID.randomUUID().toString();
		userDao.setUserHash(email, userhash);
		return userhash;
	}

	public String getUserNameByHash(String userHash) {
		return userDao.getUserNameByHash(userHash);

	}

	public int getUserPermissions(int userId, int projectId) {
		return userDao.getUserPermissions(userId, projectId);
	}

	public List<User> getUsersList() {
		return userDao.getUsersList();
	}

	public int getUserIdByHash(String userHash) {
		return userDao.getUserId(userHash);
	}

	public int getUserIdByProjectId(int projectId) {
		return userDao.getUserIdByProjectId(projectId);
	}

	public User getAvatar(int userId) {
		return userDao.getAvatar(userId);
	}

	public User getUserById(Integer userIdByHash) {

		return userDao.getUserById(userIdByHash);
	}

	public void userUpdate(User userRedact) {
		userDao.userUpdate(userRedact);

	}

	public List<InvitedUsers> getInvitedUsers(int projectId) {
		return userDao.getInvitedUsers(projectId);
	}

	public void changePassword(User user) {
		userDao.changePassword(user);

	}

	public void setAvatar(User user) {
		userDao.setAvatar(user);

	}

	public void deleteHash(Integer userId) {
		userDao.deleteHash(userId);

	}

	public String getAvatarByUserId(int userId) {
		return userDao.getAvatarByUserId(userId);
	}

}
