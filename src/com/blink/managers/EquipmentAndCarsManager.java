package com.blink.managers;

import java.util.List;
import java.util.Set;

import com.blink.dao.EquipmentAndCarsDao;
import com.blink.entity.City;
import com.blink.entity.Country;
import com.blink.entity.Equipment;
import com.blink.entity.InvitedEquipment;
import com.blink.entity.Region;

public class EquipmentAndCarsManager {

	private EquipmentAndCarsDao equipmentAndCarsDao;
	private static EquipmentAndCarsManager instance;

	public static EquipmentAndCarsManager getInstance() {
		return instance;
	}

	public EquipmentAndCarsManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setEquipmentAndCarsDao(EquipmentAndCarsDao equipmentAndCarsDao) {
		this.equipmentAndCarsDao = equipmentAndCarsDao;
	}

	public List<Equipment> getEquipmentAndCarsList() {
		return equipmentAndCarsDao.getEquipmentAndCarsList();
	}

	public List<Equipment> getSortedEquipmentAndCarsList(int countryId) {
		return equipmentAndCarsDao.getSortedEquipmentAndCarsList(countryId);
	}

	public List<Country> getCountriesList() {
		return equipmentAndCarsDao.getCountrysList();
	}

	public List<Region> getRegionList(int countryId) {
		return equipmentAndCarsDao.getRegionList(countryId);
	}

	public List<City> getCitiesList(int regionId) {
		return equipmentAndCarsDao.getCitiesList(regionId);
	}

	public List<Equipment> getSortedEquipmentAndCarsListByRegion(int regionId) {
		return equipmentAndCarsDao
				.getSortedEquipmentAndCarsListByRegion(regionId);
	}

	public List<Equipment> getSortedEquipmentAndCarsListByCity(int cityId) {
		return equipmentAndCarsDao.getSortedEquipmentAndCarsListByCity(cityId);
	}

	public List<Equipment> getEquipmentsByUserId(int userId) {
		return equipmentAndCarsDao.getEquipmentsByUserId(userId);
	}

	public Equipment getEquipmentById(int equipmentId) {
		return equipmentAndCarsDao.getEquipmentById(equipmentId);
	}

	public void addNewEquipment(Equipment newEquipment) {
		equipmentAndCarsDao.addNewEquipment(newEquipment);
		
	}

	public void equipmentUpdate(Equipment equipment) {
		equipmentAndCarsDao.equipmentUpdate(equipment);
		
	}

	public boolean isInvited(int projectId, int equipmentId) {
		return equipmentAndCarsDao.isInvited(projectId, equipmentId);
	}

	public void invitEquipment(InvitedEquipment invitedEquipment) {
		equipmentAndCarsDao.invitEquipment(invitedEquipment);
		
	}

	public List<InvitedEquipment> getInvitedEquipment(int projectId) {
		return equipmentAndCarsDao.getInvitedEquipment(projectId);
	}

	public Set<Equipment> getFoundEquipmentsList(String searchText) {
		return equipmentAndCarsDao.getFoundEquipmentsList(searchText);
	}



}
