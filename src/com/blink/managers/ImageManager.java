package com.blink.managers;

import java.util.List;

import com.blink.dao.ImageDao;
import com.blink.entity.Image;

public class ImageManager {

	private ImageDao imageDao;
	private static ImageManager instance;

	public static ImageManager getInstance() {
		return instance;
	}

	public ImageManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setImageDao(ImageDao imageDao) {
		this.imageDao = imageDao;
	}

	public void savaImage(Image image) {
		imageDao.saveImage(image);
		
	}

	public List<Image> getImagesByEquipmentId(int equipmentId) {
		return imageDao.getImagesByEquipmentId( equipmentId);
	}

	public List<Image> getImagesByAreaId(int areatId) {
		return imageDao.getImagesByAreaId(areatId);
	}

	public void deleteImgById(int deleteId) {
		imageDao.deleteImgById(deleteId);
		
	}


	public String getImageNameById(int deleteId) {
		return imageDao.getImageNameById(deleteId);
	}

}
