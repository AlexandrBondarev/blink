package com.blink.managers;

import java.util.List;
import java.util.Set;

import com.blink.dao.CounselorsDao;
import com.blink.entity.Counselor;
import com.blink.entity.Country;
import com.blink.entity.InvitedCounselors;

public class CounselorsManager {

	private CounselorsDao counselorsDao;
	private static CounselorsManager instance;

	public static CounselorsManager getInstance() {
		return instance;
	}

	public CounselorsManager() {
		if (instance == null) {
			instance = this;
		}
	}

	public void setCounselorsDao(CounselorsDao counselorsDao) {
		this.counselorsDao = counselorsDao;
	}

	public List<Counselor> getCounselorsList() {
		return counselorsDao.getCouselorsList();
	}

	public List<Country> getCountriesList() {
		return counselorsDao.getCountriesList();
	}

	public List<Counselor> getCounselorsListByCountry(int countryId) {
		return counselorsDao.getCountriesListByCountry(countryId);
	}

	public Set<Counselor> getFoundCounselorsList(String searchText) {
		return counselorsDao.getFoundCounselorsList(searchText);
	}

	public List<Counselor> getCounselorsByUserId(int userId) {
		return counselorsDao.getCounselorsByUserId(userId);
	}

	public Counselor getCounselorById(int category) {
		return counselorsDao.getCounselorById(category);
	}

	public boolean isInvited(int projectId, int counselorId) {
		return counselorsDao.isInvited(projectId, counselorId);
	}

	public void invitCounselor(InvitedCounselors invitedCounselor) {
		counselorsDao.invitCounselor(invitedCounselor);

	}

	public List<InvitedCounselors> getInvitedCounselors(int projectId) {
		return counselorsDao.getInvitedCounselors(projectId);
	}

	public void addNewCounselor(Counselor counselor) {
		counselorsDao.addNewCounselor(counselor);

	}
	public int getMaxId() {
		return counselorsDao.getMaxId();
	}

}
