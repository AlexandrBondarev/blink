<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Страница ненайдена</title>
		<style>
			.center
 			{text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
		</style>
		<link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
			<div class="container">
			  <div class="row">
			    <div class="span12">
			      <div class="hero-unit center">
			          <h1>Page Not Found <small><font face="Tahoma" color="red">Error 404</font></small></h1>
			          <br />
			          <p>The page you requested could not be found, either contact your webmaster or try again. Use your browsers <b>Back</b> button to navigate to the page you have prevously come from</p>
			          <p><b>Or you could just press this neat little button:</b></p>
			          <a href="./index.action" class="btn btn-large btn-info"><i class="icon-home icon-white"></i> Take Me Home</a>
			        </div>
			    </div>
			  </div>
			</div>
	</body>
</html>