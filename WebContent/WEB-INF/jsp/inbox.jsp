<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Входящие сообщения</title>
	<link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
	 <link href="./css/sticky-footer.css" rel="stylesheet">
	 <link href="./css/my.css" rel="stylesheet">
</head>
	<body>

			<div id="wrap">
					<header>
							<nav class="navbar navbar-inverse navbar-static-top">
								<div class="navbar-header">
								    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
								      <span class="sr-only">Toggle navigation</span>
								      <span class="icon-bar"></span>
								      <span class="icon-bar"></span>
								      <span class="icon-bar"></span>
								    </button>
								    <a class="navbar-brand" href="./index.action">Blink</a>
							  </div>
							  <div class="navbar-collapse collapse">
								    <ul class="nav navbar-nav">
									      <li ><a href="./project.action">Проекты</a></li>
									      <li><a href="./cars.action">Оборудование и машины</a></li>
									      <li><a href="./counselors.action">Консультанты</a></li>
									      <li><a href="./area.action">Помещения</a></li>
								      </ul>
								      <c:if test="${isAuthenticated}">
									     <div class="nav navbar-right" >
										      <ul class="nav navbar-nav">
												  <li>	      
													<div class="media">
														  <a class="pull-left" href="#">
													  	  <c:choose>
							  							 	  <c:when test="${avatar != null}">
							  							 	  <img class="media-object" src="./images/users/${avatar}" onerror="this.src ='./images/userDefault.jpg"  width="50" height="50"/>	
							  							 	  </c:when>
							  							 	  <c:otherwise>
							  							 	  <img class="media-object" src="./images/userDefault.jpg"  width="50" height="50"/>
							  							 	  </c:otherwise>
		  							 	 	   	          </c:choose>
														  </a>
													</div>
											      </li>
											      <li class="dropdown">
										              <a href="index.action" class="dropdown-toggle" data-toggle="dropdown">${userName}<b class="caret"></b></a>
										              <ul class="dropdown-menu">
										                <li><a href="./userRedact.action">Изменить данные</a></li>
										                <li><a href="./changePassword.action">Изменить пароль</a></li>
										                <li><a href="./changeAvatar.action">Изменить аватар</a></li>
										              </ul>
									                </li>
											      <li><a href="./signout.action" >Выход <span class="glyphicon glyphicon-log-out"></span></a></li>
										    </ul> 
										</div>
									</c:if>
								</div>
						   	</nav>
						</header>
					
					
						<div class="container">
							 <div class="row">
							    <div class="col-md-2">
							     <c:choose>
								   <c:when test="${isAuthenticated}">
										<div class="panel panel-default">
											 <div class="panel-heading"  align="center">Мои объявления</div>
										   		<table class="table table-hover table-condensed">
														 <tr>
															  <td><a href="./personalProjects.action" >
										    					 <span class="badge pull-right">
										    					 ${homePageData.getProjectsCount()}
										    					 </span><span class="glyphicon glyphicon-briefcase"></span>&#160;
															 	 Проекты
										    					</a></td>
														 </tr>
														 <tr>
															  <td><a href="./personalEquipments.action">
										    					 <span class="badge pull-right">
										    					 ${homePageData.getEquipmentCount()}</span>
															 	Оборудование и машины
										    					</a></td>
														</tr>
														 <tr>
															  <td><a href="./personalAreas.action">
										    					 <span class="badge pull-right">
										    					 ${homePageData.getAreasCount()}</span>
															 	Площади
										    					</a></td>
														 </tr>
														 <tr>
															  <td><a href="./personalCounselors.action">
										    					 <span class="badge pull-right">
										    					 ${homePageData.getCounselorsCount()}</span>
															 	Консультации
										    					</a></td>
														</tr>
										   		</table>
									   		</div>
							   		
									   		<div class="panel panel-default">
												  <div class="panel-heading"  align="center">Мои сообщения</div>
									   		  			<table class="table table-hover table-condensed">
															<tr>
															     <td>
															   		<a href="./newPersonalMessage.action"><span class="glyphicon glyphicon-folder"></span>&#160;Новое сообщение</a>
															 	 </td>
															 </tr>
															 <tr>
															  	 <td class="danger">
															  	  	<a href="./inbox.action"><span class="badge pull-right">${homePageData.getMessagesUnread()}</span><span class="glyphicon glyphicon-folder-close"></span>&#160;Входящие</a>
															     </td>
															</tr>
															<tr>
															  	 <td>
															  	   <a href="./outbox.action"><span class="glyphicon glyphicon-send"></span>&#160;Отправленные</a>
															  	 </td>
															</tr>
															<tr>
															  <td>
															  	  <a href="./deletedBox.action"><span class="glyphicon glyphicon-trash"></span>&#160;Удаленные</a>
															  </td>
															</tr>
									   				</table>
								   			</div>
					   		
							   		</c:when>
				  				  <c:otherwise>
									  	<script type="text/javascript">
							        	 location="./notFoundPage.action";
									   </script>
				  				</c:otherwise>
				  			 </c:choose>
						  </div>
				
				         <div class="col-md-9">
				         	<c:choose>
							   <c:when test="${privateMessagesList.isEmpty()}">
							   <div class="alert alert-info"><span class="glyphicon glyphicon-exclamation-sign"></span>&#160; Почтовый ящик пуст.</div>
							   </c:when>
			 				 <c:otherwise>
									<form method="get">
										<table class="table table-hover table-condensed">
								
											<tr>
											    <td  class="forTd" align="center">ID</td>
												<td  class="forTd" align="center">ДАТА ОТПРАВЛЕНИЯ</td>
												<td  class="forTd" align="center"> ОТ</td>
												<td  class="forTd" align="center">ТЕМА</td>
												<td  class="forTd" align="center">ТЕКСТ СООБЩЕНИЯ</td>
												<td colspan="2">ДЕЙСТВИЕ</td>
											</tr>
								
											<c:forEach var="each" items="${privateMessagesList}">
											<tr>
													<td>${each.getId()}</td>
													<td>${each.getCreationDate()}</td>	
													<c:if test ="${each.getType() == 0}">
													 <td>${each.getSender().getUsername()}</td>
														<c:choose>
															 <c:when test="${each.getArea() == null && each.getCounselor() != null && each.getProject() != nul && each.getCounselor().getUser().getId() == userId  }">
															 <td colspan="2">
															 Ваc преглашают как консалтера ${each.getCounselor().getTitle()} в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>
															</td>
															<td><button type="submit"  name ="counselorAgree" value ="${each.getId()}"class="btn btn-warning">Вступить</button></td>
															</c:when>
															
															 <c:when test="${each.getArea() != null && each.getCounselor() == null && each.getProject() != null && each.getArea().getUser().getId() == userId }">
															 <td colspan="2">
															 Вашу площадь <a href="./areaDetails.action?areaId=${each.getArea().getId()}">${each.getArea().getTitle()}</a> приглашают в проект <a href="project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a></td>
															 <td><button type="submit"  name ="areaAgree" value ="${each.getId()}"class="btn btn-warning">Вступить</button></td>
															 </c:when>
															 
															  <c:when test="${each.getEquipment() != null && each.getCounselor() == null && each.getProject() != null && each.getEquipment().getUser().getId() == userId }">
															 <td colspan="2">
															 Ваше оборудование <a href="./equipmentDetails.action?equipmentId=${each.getEquipment().getId()}">${each.getEquipment().getTitle()}</a>  приглашают в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a></td>
															  <td><button type="submit"  name ="equipmentAgree" value ="${each.getId()}"class="btn btn-warning">Вступить</button></td>
															</c:when>
															 
													
									  			         </c:choose>
													</c:if>
													<c:if test ="${each.getType() ==1}">
													 <td>Сформированно автоматически</td>
														<c:choose>
															 <c:when test="${each.getArea() == null && each.getCounselor() != null && each.getProject() != nul && each.getCounselor().getUser().getId() != userId && each.getProject().getUser().getId() == userId }">
																 <td>Вступление консультанта</td>
																 <td>Консалтер&#160; ${each.getCounselor().getCompetence()}&#160;вступил в Ваш проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>.Пригласил&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															 <c:when test="${each.getArea() == null && each.getCounselor() != null && each.getProject() != nul && each.getCounselor().getUser().getId() != userId}">
																 <td>Вступление консультанта</td>
																 <td>Консалтер&#160; ${each.getCounselor().getCompetence()}&#160;вступил в проект <a href="project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>.Пригласил&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															<c:when test="${each.getArea() == null && each.getCounselor() != null && each.getProject() != nul && each.getCounselor().getUser().getId() == userId}">
																 <td>Вступление консультанта</td>
																 <td>Вы как консалтер&#160; ${each.getCounselor().getCompetence()}&#160;вступил в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>.Пригласил&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															<c:when test="${each.getArea() != null && each.getCounselor() == null && each.getProject() != nul}">
																 <td>Вступление владельца площади</td>
																 <td>Владелец площади ${each.getArea().getDescription()} вступил в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>
																 Пригласил&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															<c:when test="${each.getEquipment() != null && each.getCounselor() == null && each.getProject() != nul}">
																 <td>Вступление владельца оборудования</td>
																 <td>Владелец оборудования <a href="./equipmentDetails.action?equipmentId=${each.getEquipment().getId()}"> ${each.getEquipment().getTitle()}</a> вступил в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>
																 		 Пригласил&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															 
									  			         </c:choose>
													</c:if>
													<c:if test ="${each.getType() ==2}">
													 <td>${each.getSender().getUsername()}</td>
														<c:choose>																																																																								
															<c:when test="${each.getArea() == null && each.getCounselor() != null && each.getProject() != nul && each.getCounselor().getUser().getId() != userId && each.getProject().getUser().getId() == userId}">
															 <td>Заявка</td>
															 <td>Консалтер  ${each.getCounselor().getTitle()} предлагает свои услуги для проекта <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a></td>
															  <td><button type="submit"  name ="counselorInvite" value ="${each.getId()}"class="btn btn-warning">Принять</button></td>
															 </c:when>
															 
															  <c:when test="${each.getArea() != null && each.getCounselor() == null && each.getProject() != null && each.getArea().getUser().getId() != userId && each.getProject().getUser().getId() == userId}">
															 <td>Заявка</td>
														     <td>Площадь <a href="./areaDetails.action?areaId=${each.getArea().getId()}">${each.getArea().getTitle()}</a> может быть полезна вашему проекту <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a></td>
														     <td><button type="submit"  name ="areaInvite" value ="${each.getId()}"class="btn btn-warning">Принять</button></td>
															 </c:when>
															 
															   <c:when test="${each.getEquipment() != null && each.getCounselor() == null && each.getProject() != null && each.getProject().getUser().getId() == userId }">
															 <td>Заявка</td>
															 <td>Оборудование <a href="./equipmentDetails.action?equipmentId=${each.getEquipment().getId()}">${each.getEquipment().getTitle()}</a> может быть полезно Вашему проекту <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a></td>
															 <td><button type="submit"  name ="equipmentInvite" value ="${each.getId()}"class="btn btn-warning">Принять</button></td>
															 </c:when>
															 
															 <c:when test="${each.getEquipment() == null && each.getCounselor() == null && each.getProject() != null && each.getArea() == null}">
															 <td>Заявка в качестве участника</td>
															<td>  ${each.getMessage()}</td>
															 <td><button type="submit"  name ="userInvite" value ="${each.getId()}"class="btn btn-warning">Принять</button></td>
															</c:when>
															 <c:when test="${each.getEquipment() == null && each.getCounselor() == null && each.getProject() == null && each.getArea() == null}">
																 <td>Личное сообщение</td>
																<td>  ${each.getMessage()}</td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
									  			         </c:choose>
													</c:if>
													<c:if test ="${each.getType() ==3}">
															<c:choose>
															 <c:when test="${each.getArea() == null && each.getCounselor() != null && each.getProject() != nul && each.getCounselor().getUser().getId() != userId && each.getProject().getUser().getId() == userId }">
																 <td>Вступление участника</td>
																 <td>Консалтер&#160; ${each.getCounselor().getCompetence()}&#160;вступил в Ваш проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>.Принял&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															 <c:when test="${each.getArea() == null && each.getCounselor() != null && each.getProject() != nul && each.getCounselor().getUser().getId() != userId}">
																 <td>Вступление участника</td>
																 <td>Консалтер&#160; ${each.getCounselor().getCompetence()}&#160;вступил в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>.Принял&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															<c:when test="${each.getArea() == null && each.getCounselor() != null && each.getProject() != nul && each.getCounselor().getUser().getId() == userId}">
																 <td>Вступление участника</td>
																 <td>Вы как консалтер&#160; ${each.getCounselor().getCompetence()}&#160;вступил в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>.Принял&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															<c:when test="${each.getArea() != null && each.getCounselor() == null && each.getProject() != nul}">
																 <td>Вступление участника</td>
																 <td>Владелец площади ${each.getArea().getDescription()} вступил в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>
																 Принял&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															<c:when test="${each.getEquipment() != null && each.getCounselor() == null && each.getProject() != nul}">
																 <td>Вступление участника</td>
																 <td>Владелец оборудования <a href="./equipmentDetails.action?equipmentId=${each.getEquipment().getId()}"> ${each.getEquipment().getTitle()}</a> вступил в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>
																 		 Принял&#160;<a> ${each.getSender().getUsername()}</a></td>
																 <td></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															<c:when test="${each.getEquipment() == null && each.getCounselor() == null && each.getProject() != nul}">
																 <td>Вступление участника</td>
																 <td>Участник ${each.getSender().getUsername()}вступил в проект <a href="./project.action?projectId=${each.getProject().getId()}"> ${each.getProject().getTitle()}</a>
																 		 Принял&#160;<a> ${each.getProject().getUser().getUsername()}</a></td>
																 <td></td>
																 <td><button type="submit"  name ="deleteId" value ="${each.getId()}"class="btn btn-warning">Удалить</button></td>
															</c:when>
															 
									  			         </c:choose>
			        										 <td>${each.getMessage()}</td>
									  			         
													</c:if>
												</tr>
											</c:forEach>
									</table>
								</form>
						   </c:otherwise>
					  </c:choose>
		   		</div>
		   </div>
		</div>
	</div>

		<div id="footer" >
		     <div class="container">
		       	<p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span> Александр Бондарев, 2014 г. </p>
		     </div>
	   </div>


	   <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
       <script src="./js/bootstrap.min.js"></script>


	</body>
</html>