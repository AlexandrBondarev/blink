<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>userRedact</title>
        <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
       	 <link href="./css/sticky-footer.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <header>
			<nav class="navbar navbar-inverse">
				<div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				      <span class="sr-only">Toggle navigation</span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				    </button>
				    <a class="navbar-brand" href="./index.action">Blink</a>
			  </div>
			  <div class="navbar-collapse collapse">
				    <ul class="nav navbar-nav">
					      <li ><a href="./project.action">Проекты</a></li>
					      <li class="active"><a href="./cars.action">Оборудование и машины</a></li>
					      <li><a href="./counselors.action">Консультанты</a></li>
					      <li><a href="./area.action">Помещения</a></li>
				      </ul>
				      <c:if test="${isAuthenticated}">
					     <div class="nav navbar-right" >
						      <ul class="nav navbar-nav">
								  <li>	      
									<div class="media">
										  <a class="pull-left" href="#">
								  		    <c:choose>
			  							 	<c:when test="${avatar != null}">
			  							 	<img class="media-object" src="./images/users/${avatar}" onerror="this.src ='./images/userDefault.jpg'"  width="50" height="50"/>	
			  							 	</c:when>
			  							 	<c:otherwise>
			  							 	<img class="media-object" src="./images/userDefault.jpg"  width="50" height="50"/>
			  							 	</c:otherwise>
		  							 	 	</c:choose>
										  </a>
									</div>
							      </li>
							       <li class="dropdown">
						              <a href="./userRedact.action" class="dropdown-toggle" data-toggle="dropdown">${user.getUsername()}<b class="caret"></b></a>
						              <ul class="dropdown-menu">
						                <li><a href="./userRedact.action">Изменить данные</a></li>
						                <li><a href="./changePassword.action">Изменить пароль</a></li>
						                <li><a href="./changeAvatar.action">Изменить аватар</a></li>
						              </ul>
					              </li>
							      <li><a href="./signout.action" >Выход <span class="glyphicon glyphicon-log-out"></span></a></li>
						    </ul> 
						</div>
					</c:if>
				</div>
		   </nav>
		</header>
		
	
				
						
			<div class="container">
					 <div class="row">
					    <div class="col-md-2">
					     <c:choose>
						   <c:when test="${isAuthenticated}">
								<div class="panel panel-default">
									  	<div class="panel-heading" align="center">Мои объявления</div>
										   		<table class="table table-hover table-condensed">
														 <tr>
															  <td><a href="./personalProjects.action" >
										    					 <span class="badge pull-right">
										    					 ${homePageData.getProjectsCount()}
										    					 </span><span class="   glyphicon glyphicon-briefcase"></span>&#160;
															 	 Проекты
										    					</a></td>
														 </tr>
														 <tr>
															  <td><a href="./personalEquipments.action">
										    					 <span class="badge pull-right">
										    					 ${homePageData.getEquipmentCount()}</span>
															 	Оборудование и машины
										    					</a></td>
														 </tr>
													 	  <tr>
															  <td><a href="./personalAreas.action">
										    					 <span class="badge pull-right">
										    					 ${homePageData.getAreasCount()}</span>
															 	Площади
										    					</a></td>
														 </tr>
														  <tr>
															  <td><a href="./personalCounselors.action">
										    					 <span class="badge pull-right">
										    					 ${homePageData.getCounselorsCount()}</span>
															 	Консультации
										    					</a></td>
														  </tr>
										   		</table>
									   		</div>
									   		
									   		<div class="panel panel-default">
												  	<div class="panel-heading" align="center">Мои сообщения</div>
											   		  		<table class="table table-hover table-condensed">
																	<tr>
																	     <td>
																	   		<a href="./newPersonalMessage.action"><span class="glyphicon glyphicon-folder"></span>&#160;Новое сообщение</a>
																	 	 </td>
																	 </tr>
																	 <tr>
																	  	 <td>
																	  	  	<a href="./inbox.action"><span class="badge pull-right">${homePageData.getMessagesUnread()}</span><span class="glyphicon glyphicon-folder-close"></span>&#160;Входящие</a>
																	     </td>
																	</tr>
																	<tr>
																	  	 <td>
																	  	   <a href="./outbox.action"><span class="glyphicon glyphicon-send"></span>&#160;Отправленные</a>
																	  	 </td>
																	</tr>
																	<tr>
																		  <td >
																		  	  <a href="./deletedBox.action"><span class="glyphicon glyphicon-trash"></span>&#160;Удаленные</a>
																		  </td>
																	</tr>
												   		 </table>
										   		</div>
							   		
					   		</c:when>
		  				  <c:otherwise>
					  		<table class="table table-hover table-condensed">
									    <tr>
										   <td class="active"><h4>Выполните вход</h4></td>
										</tr>
										<tr>
									       <td class="warning">
								        	<form method="post" class="form-signin">
											        <input type="text" name="email" class="form-control" placeholder="Email address" required autofocus>
											        <input type="password" name="password" class="form-control" placeholder="Password" required>
											        <button class="btn btn-xs btn-primary btn-block" type="submit">Sign in</button>
									      </form>
										</tr>
										 <tr>
											 <td class="warning">
										      <a href="./userRegistration.action" class="list-group-item">Регистрация</a>
										      </td>
							     	    </tr>
										<tr>
									   </tr>
								</table>
		  				</c:otherwise>
		  			 </c:choose>
				   </div>
		
					 <div class ="col-md-5 "> 
					
						<c:choose>
							<c:when test="${!isAuthenticated}">
										Вы не можете создать проект т.к. неавторизованы
							</c:when>
						<c:otherwise>
								<div style ="font-weight:800; border: #FFFFFF" align="center">
										<h4>ИЗМЕНЕНИЕ ДАНЫХ ПОЛЬЗОВАТЕЛЯ</h4>
								</div>
								<form method = "POST">
				  					 <table class="table table-hover table-condensed">
										 <tr >
											<td style="border:#FFFFFF">	
												username
											</td>
										    <td style="border:#FFFFFF">	
												<input class="form-control input-sm"  type="text" name="username" class="form-control" id="inputName1" value="${user.getUsername()}">
											</td>
										</tr>
										<tr >
											<td style="border:#FFFFFF">	
												  email
										   </td>
										    <td style="border:#FFFFFF">		
										     	  <input class="form-control input-sm" type="email" name="email" class="form-control" id="inputEmail1" value="${user.getEmail()}">
											</td>
										<tr>
												<td style="border:#FFFFFF">		
												  last name
										   </td>
										  	<td style="border:#FFFFFF">		
										     	   <input class="form-control" type="text"  name="lastName" class="form-control" id="inputLastName" value="${user.getLastName()}">
											</td>
										</tr>	  
										<tr >
											<td style="border:#FFFFFF">	
												first name
										   </td>
										    <td style="border:#FFFFFF">	
										     	  <input class="form-control" type="text" name="firstName" class="form-control" id="inputFirstName" value="${user.getFirstName()}">
											</td>
										</tr> 
										<tr >
											<td style="border:#FFFFFF">		
												middle name
										   </td>
										    <td style="border:#FFFFFF">	
											  <input class="form-control" type="text" name="middleName" class="form-control" id="inputMiddleName" value="${user.getMiddleName()}">
											</td>
										</tr> 
											<tr>
											<td style ="border:#FFFFFF">	
												date of birth
										   </td>
										    	<td style="border:#FFFFFF">		
											  <input class="form-control" type="date" name="dateOfBirth" class="form-control" id="inputDateOfBirt" value="${user.getDateOfBirth()}">
											</td>
										</tr> 
										<tr>
											<td style="border:#FFFFFF">			
												organization
										   </td>
										    	<td style="border:#FFFFFF">		
											  <input class="form-control" type="text" name="organization" class="form-control" id="inputOrganization" value="${user.getOrganization()}">
											</td>
										</tr> 
										<tr>
											<td style="border:#FFFFFF">	
												position
										   </td>
										 	<td style="border:#FFFFFF">		
											 	<input class="form-control" type="text" name="position" class="form-control" id="inputPosition" value="${user.getPosition()}">
											</td>
										</tr>
										<tr >
											<td style="border:#FFFFFF">				
												phone number
										   </td>
										   	<td style="border:#FFFFFF">			
											  <input class="form-control" type="text" name="phoneNumber" class="form-control" id="inputPhone" pattern="[0-9]+" maxlength="50" value="${user.getPhoneNumber()}">
											</td>
										</tr>
						 		</table>
						 		
					 			<div align="right" style="margin-bottom: 1em;">
			      					<button type="submit"  class="btn btn-primary btn-xs" > Сохранить</button>
						  		</div>
						  		
							</form>
						</c:otherwise></c:choose>
					</div>
				</div>
			</div>
		

		 	<div id="footer" >
			     <div class="container">
		       		<p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span> Александр Бондарев, 2014 г. </p>
			     </div>
		   </div>
    
      <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
      <script src="./js/bootstrap.min.js"></script>	
	</body>
</html>