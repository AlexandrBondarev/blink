<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Проект детально</title>
 <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
 <link href="./css/sticky-footer.css" rel="stylesheet">
 <link href="./css/my.css" rel="stylesheet">
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
 <script src="./js/projectDetails.js"></script>
</head>

<body>
	
	<div id="wrap">
	  	<header>
				<nav class="navbar navbar-inverse navbar-static-top">
					<div class="navbar-header">
					    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					      <span class="sr-only">Toggle navigation</span>
					      <span class="icon-bar"></span>
					      <span class="icon-bar"></span>
					      <span class="icon-bar"></span>
					    </button>
					    <a class="navbar-brand" href="./index.action">Blink</a>
				  </div>
				  <div class="navbar-collapse collapse">
					    <ul class="nav navbar-nav">
						      <li class="active"><a href="./project.action">Проекты</a></li>
						      <li><a href="./cars.action">Оборудование и машины</a></li>
						      <li><a href="./counselors.action">Консультанты</a></li>
						      <li><a href="./area.action">Помещения</a></li>
					      </ul>
					      <c:if test="${isAuthenticated}">
						     <div class="nav navbar-right" >
							      <ul class="nav navbar-nav">
									  <li>	      
										<div class="media">
											  <a class="pull-left" href="#">
												   <c:choose>
					  							 	<c:when test="${avatar != null}">
					  							 	<img class="media-object" src="./images/users/${avatar}" onerror="this.src ='./images/userDefault.jpg'"  width="50" height="50"/>	
					  							 	</c:when>
					  							 	<c:otherwise>
					  							 	<img class="media-object" src="./images/userDefault.jpg"  width="50" height="50"/>
					  							 	</c:otherwise>
				  							 	 	</c:choose>
											  </a>
										</div>
								      </li>
								      <li class="dropdown">
								              <a href="./index.action" class="dropdown-toggle" data-toggle="dropdown">${userName}<b class="caret"></b></a>
								              <ul class="dropdown-menu">
								                <li><a href="./userRedact.action">Изменить данные</a></li>
								                <li><a href="./changePassword.action">Изменить пароль</a></li>
								                <li><a href="./changeAvatar.action">Изменить аватар</a></li>
								              </ul>
							                </li>
								      <li><a href="./signout.action">Выход <span class="glyphicon glyphicon-log-out"></span></a></li>
							    </ul> 
							</div>
						</c:if>
					</div>
			   </nav>
		</header>
	
		<div class="container">
			 <div class="row">
			    <div class="col-md-2">
				     <c:choose>
						   <c:when test="${isAuthenticated}">
						 			<div class="panel panel-default">
		 								<div class="panel-heading">Мои объявления</div>
						   					<table class="table table-hover table-condensed">
													 <tr>
														  <td><a href="./personalProjects.action"  >
									    					 <span class="badge pull-right">
									    					 ${homePageData.getProjectsCount()}
									    					 </span><span class="glyphicon glyphicon-briefcase"></span>&#160;
														 	 Проекты
									    					</a></td>
													 </tr>
													 <tr>
														  <td><a href="./personalEquipments.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getEquipmentCount()}</span>
														 	Оборудование и машины
									    					</a></td>
													</tr>
													 <tr>
														  <td><a href="./personalAreas.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getAreasCount()}</span>
														 	Площади
									    					</a></td>
													 </tr>
													 <tr>
														  <td><a href="./personalCounselors.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getCounselorsCount()}</span>
														 	Консультации
									    					</a></td>
													</tr>
									   		</table>
									   	</div>
									   		
									   <div class="panel panel-default">
										  <div class="panel-heading"  align="center">Мои сообщения</div>
									   		  <table class="table table-hover table-condensed">
													<tr>
													     <td>
													   		<a href="./newPersonalMessage.action"><span class="glyphicon glyphicon-folder"></span>&#160;Новое сообщение</a>
													 	 </td>
													 </tr>
													 <tr>
													  	 <td>
													  	  	<a href="./inbox.action"><span class="badge pull-right">${homePageData.getMessagesUnread()}</span><span class="glyphicon glyphicon-folder-close"></span>&#160;Входящие</a>
													     </td>
													</tr>
													<tr>
													  	 <td>
													  	   <a href="./outbox.action"><span class="glyphicon glyphicon-send"></span>&#160;Отправленные</a>
													  	 </td>
													</tr>
													<tr>
													  <td>
													  	  <a href="./deleted.box.action"><span class="glyphicon glyphicon-trash"></span>&#160;Удаленные</a>
													  </td>
													</tr>
									   		</table>
									   	</div>
							   		
								   	</c:when>
					  				  <c:otherwise>
					  				  	<div class="panel panel-default">
									  		<div class="panel-heading">Выполните вход</div>
										  		<table class="table table-hover table-condensed">
														<tr>
														     <td>
													         <form method="post" action ="./signin.action" class="form-signin">
													        	        <input TYPE="HIDDEN" name ="page" value ="project.action?projectId=${projectInfo.getId()}">
																        <input type="text" name="email" class="form-control" placeholder="Email address" required autofocus>
																        <input type="password" name="password" class="form-control" placeholder="Password" required>
																        <button class="btn btn-xs btn-primary btn-block" type="submit">Sign in</button>
														      </form>
														      </td>
														</tr>
														<tr>
															 <td>
														    	 <a href="./userRegistration.action" class="list-group-item">Регистрация</a>
														     </td>
											     	     </tr>
														 <tr>
														</tr>
												</table>
										</div>
				  				</c:otherwise>
			  				</c:choose>
				 	 </div>
					  
			   <c:if test ="${projectInfo == null}">
			  	<script type="text/javascript">
		        	 location="./notFoundPage.action"; 
				</script>
			  </c:if>
	
	
		<div class="col-md-7">
					 <table class="table table-bordered table-striped">
					
						<tr>
							<td class="forTd" align="center">ID</td>
							<td class="forTd" align="center">СТРАНА</td>
							<td class="forTd" align="center">РЕГИОН</td>
							<td class="forTd" align="center">ГОРОД</td>
							<td class="forTd" align="center">НАЗВАНИЕ ПРОЕКТА</td>
							<td class="forTd" align="center">ПРОЕКТ СОЗДАН </td>
							<td class="forTd" align="center">КОЛИЧЕСТВО СООБЩЕНИЙ</td>
							<td class="forTd" align="center">ПОСЛЕДНИЕ СООБЩЕНИК</td>
						</tr>
						<tr>
							<td>${projectInfo.getId()}</td>
							<td>${projectInfo.getCountry().getCountryNameRu()}</td>
							<td>${projectInfo.getRegion().getRegionNameRu()}</td>
							<td>${projectInfo.getCity().getCityNameRu()}</td>
							<td>${projectInfo.getTitle()}</td>
							<td>${projectInfo.getCreationDate()}</td>
							<td>${projectInfo.getMessagesCount()}</td>
							<td>${projectInfo.getLastUpdate()}</td>
						</tr>
					</table>
					
					
					<table class="table table-hover table-condensed">
						<tr>
						  	<td>
						   	  <div class="media">
								  <a class="pull-left" href="#">
								    <img class="media-object" src="./images/users/${projectInfo.getUser().getAvatar()}" onerror="this.src ='./images/userDefault.jpg'"  width="64" height="64">
								  </a>
								  <div class="media-body">
								    <h5 class="media-heading"><dfn>Проект создан:</dfn> &#160;${projectInfo.getUser().getUsername()}<dfn>&#160; Дата:</dfn>&#160; ${projectInfo.getCreationDate()}</h5>
								  	 ${projectInfo.getDescription()}
								  </div>
							 </div>
				  		    </td>
					   </tr>
				</table>
			    <c:choose>
				<c:when test="${!isAuthenticated}">
						<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span>&#160;Вы не можете просматривать cообщения,  пожалуйста зарегистрируйтесь или воидите в систему</div>
				</c:when>
				<c:otherwise>
				<c:choose>
							
					<c:when test="${userPermission ==0 && !suggestion.isEmpty()}">
						
								<div class="alert alert-danger"><p id = "dander"><span class="glyphicon glyphicon-warning-sign"></span>&#160;Вы не можете просматривать cообщения,  вы не преглашенны в проект. Напишите сообщение руководителю проекта.
										<button type="submit" id="ready" onclick="ready();" class="btn btn-danger btn-xs" > Написать сообщение</button></p>
											 <form method="POST">
								 <table>
								 	<tr>
								    	<td>
							 				<input TYPE="HIDDEN" name ="projectId" value ="${projectId}">
											 			
											<select class="form-control input-sm" style ="display: none" id="key" onchange="keySelected();"name="suggestionKey">
													<option value="-1" <c:if test="${suggestionKey== -1}">selected="selected"</c:if>>Выберете желаемый статус участия в проекте</option>
													<c:forEach var="each" items="${suggestion.entrySet()}">
														<option <c:if test="${each.getKey() == suggestionKey}">selected="selected" </c:if>value="${each.getKey()}" >${each.getValue()}</option>
													</c:forEach>
											</select>
									    </td> 
									</tr>
									 <tr>
										<td>
											<select class="form-control input-sm" style ="display: none" id="category" onchange="categorySelected();"name="category">
												<option value="-1" <c:if test="${category == -1}">selected="selected"</c:if>>Выберите Ваше объявление</option>
												<c:forEach var="each" items="${categoryList}">
													<option <c:if test="${each.getId() == category}">selected="selected" </c:if>value="${each.getId()}" ><c:if test="${suggestionKey ==1}">${each.getDescription()}</c:if><c:if test="${suggestionKey !=1}">${each.getTitle()}</c:if></option>
												</c:forEach>
										  </select>
									  </td>
									</tr>
									<tr>
										<td colspan="2" id="key4" style ="display: none">
													<b >Введите вашe cooбщение:</b>
												<div><textarea class="form-control" cols="75" rows="4" name="privateMessage"></textarea></div>	
										</td>
									</tr>
									<tr>
										<td id="submit" style ="display: none">
											<b><button type="submit" class="btn btn-danger btn-xs" > Выбрать</button></b>
									   </td>
									</tr>
							</table>
								    </form>
						  </div>
				
							</c:when>
								
							
							<c:otherwise>
								<table class="table table-hover table-condensed">
									<c:forEach var="each" items="${messagesList}">
									<tr>
									  	<td>
									   	  <div class="media">
											  <a class="pull-left" href="#">
											    <img class="media-object" src="./images/users/${each.getSender().getAvatar()}" onerror="this.src ='images/userDefault.jpg'"  width="64" height="64">
											  </a>
											  <div class="media-body"> 
											    <h5 class="media-heading"><dfn><span class="glyphicon glyphicon-user"></span></dfn> &#160;${each.getSender().getUsername()}<dfn>&#160; <span class="glyphicon glyphicon-calendar"></span></dfn>&#160; ${each.getCreationDate()}</h5>
											  	 ${each.getMessage()}
											  </div>
										 </div>
							  		    </td>
								   </tr>
								   </c:forEach>
							</table>
							<form method="post">
								<b>Введите вашe cooбщение:</b><br>
								<textarea class="form-control" rows="3" name="newMessage" required="required" cols="100"
								rows="10"></textarea>
								<br>
								<div align="right" style="margin-bottom: 1em;">
			      					<button type="submit"  class="btn btn-primary btn-xs" > Отправить</button>
						  	  </div>
							</form>
						   </c:otherwise>	
					  	</c:choose>
					</c:otherwise>
			</c:choose>
		</div>
					
		
		<div class="col-md-3">
			<div class="panel panel-default">
 				<div class="panel-heading">Участники проекта</div>
							<table class="table table-bordered table-condensed">
							
									<tr>
											<td colspan="3"class="warning"><B>Владельцы площадей</B></td>
									  </tr>
										  	<c:choose>
								   			<c:when test="${!invitedAreaList.isEmpty()}">
											<c:forEach var="each" items="${invitedAreaList}">
									  <tr>
											<td><a href="./areaDetails.action?areaId=${each.getArea().getId()}">${each.getArea().getTitle()}</a>
											</td>
											<td>${each.getArea().getUser().getUsername()}</td>
												<c:choose>
								   				<c:when test="${each.getAgreementDate() == null}">
											<td><span class="label label-default">Гость</span></td>
												</c:when>
												<c:otherwise>
											<td><span class="label label-success">Участник</span></td>
												</c:otherwise>
												</c:choose>
									  </tr>
											</c:forEach>
											</c:when>
										    <c:otherwise>
									  <tr>
											<td colspan="3">
											<span class="glyphicon glyphicon-question-sign"></span>
											&#160;В проект не приглашены помещения. 
										   </td>
									  </tr>
											</c:otherwise>
											</c:choose>
									   <tr>
											<td colspan="3"class="warning"><B>Владельцы оборудования</B></td>
									  </tr>
									 		<c:choose>
							   			    <c:when test="${!invitedEquipmentList.isEmpty()}">
											<c:forEach var="each" items="${invitedEquipmentList}">
									  <tr>
											<td><a href="./equipmentDetails.action?equipmentId=${each.getEquipment().getId()}">${each.getEquipment().getTitle()}</a>
											</td>
											<td> ${each.getEquipment().getUser().getUsername()}</td>
											 
											<c:choose>
							   				<c:when test="${each.getAgreementDate() == null}">
											<td><span class="label label-default">Гость</span></td>
											</c:when>
											<c:otherwise>
											<td><span class="label label-success">Участник</span></td>
											</c:otherwise>
											</c:choose>
									  </tr>
											</c:forEach>
											 </c:when>
											<c:otherwise>
									  <tr>
											<td colspan="3">
												<span class="glyphicon glyphicon-question-sign"></span>
												&#160;В проект не приглашены владельцы оборудования. 
											</td>
									 </tr>
											</c:otherwise>
											</c:choose>
									 <tr>
											<td colspan="3"class="warning"><B>Консалтеры</B></td>
									  </tr>
										 	<c:choose>
								   			<c:when test="${!invitedCounselorsList.isEmpty()}">
											<c:forEach var="each" items="${invitedCounselorsList}">
									 <tr>
											<td>${each.getCounselor().getCompetence()}
											</td>
											<td> ${each.getCounselor().getUser().getUsername()}</td>
											<c:choose>
							   				<c:when test="${each.getAgreementDate() == null}">
											<td><span class="label label-default">Гость</span></td>
											</c:when>
											<c:otherwise>
											<td><span class="label label-success">Участник</span></td>
											</c:otherwise>
											</c:choose>
									</tr>
											</c:forEach>
											</c:when>
											<c:otherwise>
									<tr>
											<td colspan="3">
												<span class="glyphicon glyphicon-question-sign"></span>
												&#160;В проект не приглашены консультанты. 
											</td>
									</tr>
										</c:otherwise>
										</c:choose>
										<c:choose>
										<c:when test="${!invitedUsersList.isEmpty()}">
										<tr>
											<td colspan="3"class="warning"><B>Другие пользователи</B></td>
									  </tr>
										  	
								   			
											<c:forEach var="each" items="${invitedUsersList}">
									  <tr>
											<td colspan="2">${each.getUser().getUsername()}
											</td>
											<c:choose>
							   				<c:when test="${each.getAgreementDate() == null}">
											<td><span class="label label-default">Гость</span></td>
											</c:when>
											<c:otherwise>
											<td><span class="label label-success">Участник</span></td>
											</c:otherwise>
											</c:choose>
									  </tr>
											</c:forEach>
											</c:when>
											</c:choose>
									   <tr>
							</table>
						</div>		
					</div>
				</div>		
			</div>
		</div>
		
		 <div id="footer" >
		     <div class="container">
		       <p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span> Александр Бондарев, 2014 г.</p>
		     </div>
	    </div>
	    
	   <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
       <script src="./js/bootstrap.min.js"></script>	
	   
	</body>
</html>