<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Мои помещения</title>
		<link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="./css/sticky-footer.css" rel="stylesheet">
		<link href="./css/my.css" rel="stylesheet">
	</head>
	<body>
	
		<div id="wrap">
			<header>
				<nav class="navbar navbar-inverse navbar-static-top">
					<div class="navbar-header">
					    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					      <span class="sr-only">Toggle navigation</span>
					      <span class="icon-bar"></span>
					      <span class="icon-bar"></span>
					      <span class="icon-bar"></span>
					    </button>
					    <a class="navbar-brand" href="index.action">Blink</a>
				  </div>
				  <div class="navbar-collapse collapse">
					    <ul class="nav navbar-nav">
						      <li><a href="./project.action">Проекты</a></li>
						      <li><a href="./cars.action">Оборудование и машины</a></li>
						      <li><a href="./counselors.action">Консультанты</a></li>
						      <li><a href="./area.action">Помещения</a></li>
					      </ul>
					      <c:choose>
						   <c:when test="${isAuthenticated}">
						     <div class="nav navbar-right" >
							      <ul class="nav navbar-nav">
									  <li>	      
										<div class="media">
										  <a class="pull-left" href="#">
									  		<c:choose>
			  							 	<c:when test="${avatar != null}">
			  							 	<img class="media-object" src="./images/users/${avatar}" onerror="this.src ='./images/userDefault.jpg'"  width="50" height="50"/>	
			  							 	</c:when>
			  							 	<c:otherwise>
			  							 	<img class="media-object" src="./images/userDefault.jpg"  width="50" height="50"/>
			  							 	</c:otherwise>
			  							 	</c:choose>
										  </a>
										</div>
								      </li>
								      <li class="dropdown">
							              <a href="./index.action" class="dropdown-toggle" data-toggle="dropdown">${userName}<b class="caret"></b></a>
							              <ul class="dropdown-menu">
							                <li><a href="./userRedact.action">Изменить данные</a></li>
							                <li><a href="./changePassword.action">Изменить пароль</a></li>
							                <li><a href="./changeAvatar.action">Изменить аватар</a></li>
							              </ul>
						                </li>
								      <li><a href="./signout.action" >Выход <span class="glyphicon glyphicon-log-out"></span></a></li>
							    </ul> 
							</div>
							 </c:when>
						   	<c:otherwise>
					  			<script type="text/javascript">
								        	 location="./signin.action";
								</script>
		  				  </c:otherwise>
		  			    </c:choose>
					   </div>
			  		</nav>
			</header>
			
			<div class="container">
					 <div class="row">
						    <div class="col-md-2">
						    	<div class="panel panel-default">
	  						  	<div class="panel-heading"  align="center">Мои объявления</div>
							   		<table class="table table-hover table-condensed">
											 <tr>
											  <td><a href="./personalProjects.action" >
						    					 <span class="badge pull-right">
						    					 ${homePageData.getProjectsCount()}
						    					 </span><span class="glyphicon glyphicon-briefcase"></span>&#160;
											 	 Проекты
						    					</a></td>
											 </tr>
											 <tr>
											  <td><a href="./personalEquipments.action">
						    					 <span class="badge pull-right">
						    					 ${homePageData.getEquipmentCount()}</span>
											 	Оборудование и машины
						    					</a></td>
											</tr>
											 <tr>
											  <td class="danger"><a href="#">
						    					 <span class="badge pull-right">
						    					 ${homePageData.getAreasCount()}</span>
											 	Площади
						    					</a></td>
											 </tr>
											 <tr>
											  <td><a href="./personalCounselors.action">
						    					 <span class="badge pull-right">
						    					 ${homePageData.getCounselorsCount()}</span>
											 	Консультации
						    					</a></td>
											</tr>
							   		</table>
						   	</div>
						   		
						   		
						     <div class="panel panel-default">
	  						  	<div class="panel-heading"  align="center">Мои сообщения</div>
						   			<table class="table table-hover table-condensed">
											<tr>
											     <td>
											   		<a href="./newPersonalMessage.action"><span class="glyphicon glyphicon-folder"></span>&#160;Новое сообщение</a>
											 	 </td>
											 </tr>
											 <tr>
											  	 <td>
											  	  	<a href="./inbox.action"><span class="badge pull-right">${homePageData.getMessagesUnread()}</span><span class="glyphicon glyphicon-folder-close"></span>&#160;Входящие</a>
											     </td>
											</tr>
											<tr>
											  	 <td>
											  	   <a href="./outbox.action"><span class="glyphicon glyphicon-send"></span>&#160;Отправленные</a>
											  	 </td>
											</tr>
											<tr>
												  <td>
												  	  <a href="./deletedBox.action"><span class="glyphicon glyphicon-trash"></span>&#160;Удаленные</a>
												  </td>
											</tr>
						   			</table>
						   	  	</div>
						   	</div>
					  
			   		
		
							<div class="col-md-7">
								<c:choose>
								   <c:when test="${homePageData.getAreasCount() == 0}">
								   <div class="alert alert-info"><span class="glyphicon glyphicon-exclamation-sign"></span>&#160; У Вас нет зарегистрированных площадей!</div>
								   </c:when>
				  				 <c:otherwise>
								<table class="table table-bordered table-striped">
										<tr>
											<td  class="forTd" align="center">ID</td>
											<td  class="forTd" align="center">СТРАНА</td>
											<td  class="forTd" align="center">РЕГИОН</td>
											<td  class="forTd" align="center">ГОРОД</td>
											<td  class="forTd" align="center">НАЗВАНИЕ ПОМЕЩЕНИЯ</td>
										</tr>
										 <c:forEach var="each" items="${areasList}">
										 <tr>
											<td>${each.getId()}</td>
											<td>${each.getCountry().getCountryNameRu()}</td>
											<td>${each.getRegion().getRegionNameRu()}</td>
											<td>${each.getCity().getCityNameRu()}</td>
											<td><a href="./areaDetails.action?areaId=${each.getId()}">${each.getTitle()}</a></td>
										</tr>
										 </c:forEach>
								</table>
								</c:otherwise>
							</c:choose>
						</div>
						
						<div class="col-lg-3">
							<p>
								<a href="./newArea.action" class="design">Опубликовать помещение</a>
							</p>
					  </div>
				   </div>
				</div>
	     	</div>
	     	
	    	 <div id="footer">
			     <div class="container">
			       <p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span> Александр Бондарев, 2014 г. </p>
			     </div>
		   </div>
		   
	  	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
   		<script src="./js/bootstrap.min.js"></script>
 	</body>
</html>