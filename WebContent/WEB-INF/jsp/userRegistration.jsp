<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Регистрация пользователя</title>
		<link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
		 <link href="./css/sticky-footer.css" rel="stylesheet">
	</head>
	<body>
	
		<div id="wrap">				
			<div class="container">
				<div class="row">
			  		<div class ="col-md-4 col-md-offset-3">
			 			<div style ="font-weight:800; border: #FFFFFF" align="center" >
							<h4>РЕГИСТРАЦИЯ НОВОГО ПОЛЬЗОВАТЕЛЯ</h4>
						</div>
		  				<form method="post">
					  	 <table class="table table-hover table-condensed">
								    <tr class="danger">
									   <td  colspan="2"><h4> <span class="glyphicon glyphicon-warning-sign"></span>&#160;Поля обязательные для заполнения:</h4></td>
									</tr>
									 <tr >
										<td style ="border: #FFFFFF">	
											username
										</td>
									    <td style ="border: #FFFFFF">	
											<input class="form-control input-sm"  type="text" name="username" placeholder="username" required>
										</td>
									</tr>
									<tr>
										<td style ="border: #FFFFFF">			
											  password
									   </td>
									    <td style ="border: #FFFFFF">	
									     	 <input class="form-control input-sm"  type="password" name="password" placeholder="password" required>
										</td>
									</tr>	    
									<tr >
										<td style ="border: #FFFFFF">	
											  email
									   </td>
									    <td style ="border: #FFFFFF">		
									     	  <input class="form-control input-sm" type="email" name="email" placeholder="email" required>
										</td>
									</tr>	  
									  <tr  class="warning">
									   <td  colspan="2"> <h4>Поля необязательные для заполнения:</h4></td>
									</tr>
									<tr>
											<td style ="border: #FFFFFF">		
											  last name
									   </td>
									  	<td style ="border: #FFFFFF">		
									     	   <input type="text"  name="lastName" class="form-control" placeholder="last name">
										</td>
									</tr>	  
									<tr >
										<td style ="border: #FFFFFF">	
											first name
									   </td>
									    <td style ="border: #FFFFFF">	
									     	  <input type="text" name="firstName" class="form-control"  placeholder="first name">
										</td>
									</tr> 
									<tr >
										<td style ="border: #FFFFFF">		
											middle name
									   </td>
									    <td style ="border: #FFFFFF">	
										  <input type="text" name="middleName" class="form-control"  placeholder="middle name">
										</td>
									</tr> 
										<tr>
										<td style ="border: #FFFFFF">	
											date of birth
									   </td>
									    	<td style ="border: #FFFFFF">		
										  <input type="date" name="dateOfBirth" class="form-control">
										</td>
									</tr> 
									<tr>
										<td style ="border: #FFFFFF">			
											organization
									   </td>
									    	<td style ="border: #FFFFFF">		
										  <input type="text" name="organization" class="form-control" placeholder="organization">
										</td>
									</tr> 
									<tr>
										<td style ="border: #FFFFFF">	
											position
									   </td>
									 	<td style ="border: #FFFFFF">		
										 <input type="text" name="position" class="form-control" placeholder="position">
										</td>
									</tr>
									<tr >
										<td style ="border: #FFFFFF">				
											phone number
									   </td>
									   	<td style ="border: #FFFFFF">			
										  <input type="text" name="phoneNumber" class="form-control" pattern="[0-9]+" maxlength="50" placeholder="phone number">
										</td>
									</tr>
								
							 </table>
							 
							 	<div align="right" style="margin-bottom: 1em;">
				      					<button class="btn btn-primary btn-block" type="submit"> Зарегистрироваться</button>
						  		</div>
						  
						</form>
					</div>
				</div>
			</div>
		</div>
		
		 <div id="footer" >
		     <div class="container">
		       	<p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span> Александр Бондарев, 2014 г.</p>
		     </div>
	   </div>		
	</body>
</html>