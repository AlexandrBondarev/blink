<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Главная</title>
	 <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
	   <link href="./css/sticky-footer.css" rel="stylesheet">
</head>
	<body>
		
	  <div id="wrap">
		<header>
			<nav class="navbar navbar-inverse navbar-static-top">
				<div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				      <span class="sr-only">Toggle navigation</span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				    </button>
				    <a class="navbar-brand" href="./index.action">Blink</a>
			  </div>
			  <div class="navbar-collapse collapse">
				    <ul class="nav navbar-nav">
					      <li ><a href="./project.action">Проекты</a></li>
					      <li><a href="./cars.action">Оборудование и машины</a></li>
					      <li><a href="./counselors.action">Консультанты</a></li>
					      <li><a href="./area.action">Помещения</a></li>
				      </ul>
		
				</div>
		   </nav>
		</header>
	
		<div class="container">
				<p style =" font: bold 110% serif;"> <span style =" color: red; font: bold italic 110% serif;">BLINK : </span>ОБСУЖДЕНИЕ ПРОЕКТОВ,  ВОПЛОЩЕНИЕ БИЗНЕС ИДЕЙ И ИСПОЛЬЗОВАНИЯ ЛИЧНЫХ РЕСУРСОВ ДЛЯ БИЗНЕСА</p>
				<div class="row">
					  <div class="col-sm-6 col-md-3">
						    <div class="thumbnail">
						      <img src="./images/home/idea.jpg" alt="...">
							      <div class="caption">
								        <h3> Обсуждай и воплощай идеи </h3>
								        <p>Объединяйте людей и ресурсы</p>
								        <p>для реализации Ваших проектов</p>
								        <p><a href="./project.action" class="btn btn-xs btn-primary btn-block">Перейти к проектам</a></p>
							      </div>
						    </div>
					  </div>
					  
					    <div class="col-sm-6 col-md-3">
						    <div class="thumbnail">
						      <img src = "images/home/area.jpg">
							      <div class="caption">
								        <h3>Реализуй бизнес на личных площадях</h3>
								        <p>Используйте Вашу недвижимость</p>
								        <p>для создания бизнеса</p>
								        <p><a href="./area.action" class="btn btn-xs btn-primary btn-block ">Перейти к помещениям</a></p>
							      </div>
						    </div>
					  </div>
					  
					    <div class="col-sm-6 col-md-3">
						    <div class="thumbnail">
						      <img src = "./images/home/cars.jpg">
							      <div class="caption">
								        <h3>Оборудование для достижения цели</h3>
								        <p>Публикуйте Ваше оборудование</p>
								        <p>и вступайте в проекты</p>
								        <p><a href="./cars.action" class="btn btn-xs btn-primary btn-block">Перейти к оборудованию и машинам</a></p>
							      </div>
						    </div>
					  </div>
					  
					  
					    <div class="col-sm-6 col-md-3">
						    <div class="thumbnail">
							      <img src = "./images/home/consalter.jpg">
							      <div class="caption">
								        <h3>Используй знания для построения проектов</h3>
								        <p>Учавстауйте в развитии проектов</p>
								        <p>пременяя свои опыт и знания</p>
								      <p><a href="./counselors.action" class="btn btn-xs btn-primary btn-block" >Перейти к консультантам</a></p>
							      </div>
						    </div>
					  </div>
				</div>
			</div>
		</div>
			
		
			<div id="footer" >
			      <div class="container">
			        	<p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span>Александр Бондарев, 2014 г. </p>
			      </div>
		    </div>
		    
   	   <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
       <script src="./js/bootstrap.min.js"></script>
	   <script src="./js/holder.js"></script>
	</body>
</html>