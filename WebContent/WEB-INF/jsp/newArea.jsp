<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Новый проект</title>
	<link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
		 <link href="./css/sticky-footer.css" rel="stylesheet">
</head>
	<body>
		<div id="wrap">
				<header>
						<nav class="navbar navbar-inverse navbar-static-top">
							<div class="navbar-header">
							    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							      <span class="sr-only">Toggle navigation</span>
							      <span class="icon-bar"></span>
							      <span class="icon-bar"></span>
							      <span class="icon-bar"></span>
							    </button>
							    <a class="navbar-brand" href="./index.action">Blink</a>
						  </div>
						  <div class="navbar-collapse collapse">
							    <ul class="nav navbar-nav">
								      <li ><a href="./project.action">Проекты</a></li>
								      <li><a href="./cars.action">Оборудование и машины</a></li>
								      <li><a href="./counselors.action">Консультанты</a></li>
								      <li><a href="./area.action">Помещения</a></li>
							      </ul>
							      <c:if test="${isAuthenticated}">
								     <div class="nav navbar-right" >
									      <ul class="nav navbar-nav">
											  <li >	      
												<div class="media">
													  <a class="pull-left" href="#">
													  	  	<c:choose>
							  							 	<c:when test="${avatar != null}">
							  							 	<img class="media-object" src="./images/users/${avatar}" onerror="this.src ='./images/userDefault.jpg'"  width="50" height="50"/>	
							  							 	</c:when>
							  							 	<c:otherwise>
							  							 	<img class="media-object" src="./images/userDefault.jpg"  width="50" height="50"/>
							  							 	</c:otherwise>
					  							 	 	   </c:choose>
													  </a>
												</div>
										      </li>
										      <li class="dropdown">
									              <a href="./newArea.action" class="dropdown-toggle" data-toggle="dropdown">${userName}<b class="caret"></b></a>
									              <ul class="dropdown-menu">
									                <li><a href="./userRedact.action">Изменить данные</a></li>
									                <li><a href="./changePassword.action">Изменить пароль</a></li>
									                <li><a href="./changeAvatar.action">Изменить аватар</a></li>
									              </ul>
								                </li>
										      <li><a href="./signout.action" >Выход <span class="glyphicon glyphicon-log-out"></span></a></li>
									    </ul> 
									</div>
								</c:if>
							</div>
					   </nav>
				</header>
				
			
						
								
				<div class="container">
					 <div class="row">
						    <div class="col-md-2">
							   	  <c:choose>
								  	 	<c:when test="${isAuthenticated}">
							
											<div class="panel panel-default">
										  			 <div class="panel-heading" align="center">Мои объявления</div>
												   			<table class="table table-hover table-condensed">
																	 <tr>
																		  <td><a href="./personalProjects.action" >
													    					 <span class="badge pull-right">
													    					 ${homePageData.getProjectsCount()}
													    					 </span><span class="glyphicon glyphicon-briefcase"></span>&#160;
																		 	 Проекты
													    					</a></td>
																	 </tr>
																	 <tr>
																		  <td><a href="./personalEquipments.action">
													    					 <span class="badge pull-right">
													    					 ${homePageData.getEquipmentCount()}</span>
																		 	Оборудование и машины
													    					</a></td>
																	</tr>
																	 <tr>
																		  <td><a href="./personalAreas.action">
													    					 <span class="badge pull-right">
													    					 ${homePageData.getAreasCount()}</span>
																		 	Площади
													    					</a></td>
																	 </tr>
																	 <tr>
																		  <td><a href="./personalCounselors.action">
													    					 <span class="badge pull-right">
													    					 ${homePageData.getCounselorsCount()}</span>
																		 	Консультации
													    					</a></td>
																	</tr>
												   			</table>
										   			</div>
															  
													  <div class="panel panel-default">
												  			 <div class="panel-heading" align="center">Мои сообщения</div> 		
												   		  			<table class="table table-hover table-condensed">
																			<tr>
																			     <td>
																			   		<a href="./newPersonalMessage.action"><span class="glyphicon glyphicon-folder"></span>&#160;Новое сообщение</a>
																			 	 </td>
																			 </tr>
																			 <tr>
																			  	 <td>
																			  	  	<a href="./inbox.action"><span class="badge pull-right">${homePageData.getMessagesUnread()}</span><span class="glyphicon glyphicon-folder-close"></span>&#160;Входящие</a>
																			     </td>
																			</tr>
																			<tr>
																			  	 <td>
																			  	   <a href="./outbox.action"><span class="glyphicon glyphicon-send"></span>&#160;Отправленные</a>
																			  	 </td>
																			</tr>
																			<tr>
																			  <td>
																			  	  <a href="./deletedBox.action"><span class="glyphicon glyphicon-trash"></span>&#160;Удаленные</a>
																			  </td>
																			</tr>
														   		</table>
													   		</div>
					   		
							   	   			 </c:when>
					  				   				<c:otherwise>
										  			<script type="text/javascript">
													        	 location="./signin.action" ;
													</script>
				  				 			 </c:otherwise>
				  			  		 </c:choose>
					  		</div>
			
		     			<div class="col-md-7">
						
							<c:choose>
									<c:when test="${!isAuthenticated}">
												Вы не можете создать проект т.к. неавторизованы
									</c:when>
							<c:otherwise>
		
							<form method="post">
								
									 <div style ="font-weight:800; border: #FFFFFF" align="center" >
										 <h4>НОВОЕ ПОМЕЩЕНИЕ</h4>
									</div>
									<table class="table table-hover table-condensed">
											<tr>
												<td align="right" style ="border: #FFFFFF; width: 170px;">Страна</td>
												<td style ="border: #FFFFFF">
													<select name="countryId">
														<option value="-1" <c:if test="${countryId == -1}">selected="selected"</c:if>>Выберете страну</option>
														<c:forEach var="each" items="${countriesList}">
															<option <c:if test="${each.getId() == countryId}">selected="selected" </c:if>value="${each.getId()}" >${each.getCountryNameRu()}</option>
														</c:forEach>
													</select>
											  </td>
											</tr>
											<tr>
												<c:if test="${countryId >-1}">
													<td align="right" style ="border: #FFFFFF" width="170">Регион</td>
													<td style ="border: #FFFFFF">
														<select name="regionId">
															<option value="-1" <c:if test="${regionId == -1}">selected="selected"</c:if>>Выберете  регион</option>
															<c:forEach var="each" items="${regionsList}">
																<option <c:if test="${each.getId() == regionId}">selected="selected" </c:if>value="${each.getId()}" >${each.getRegionNameRu()}</option>
															</c:forEach>
														</select>
													</td>
												</c:if>
											</tr>
											<tr>
												<c:if test="${regionId >-1}">
												<td align="right" style ="border: #FFFFFF" width="170">Город</td>
												<td style ="border: #FFFFFF">
													<select name="cityId">
															<option value="-1" <c:if test="${cityId == -1}">selected="selected"</c:if>>Выберете город</option>
															<c:forEach var="each" items="${cityList}">
																<option <c:if test="${each.getId() == cityId}">selected="selected" </c:if>value="${each.getId()}" >${each.getCityNameRu()}</option>
															</c:forEach>
													</select>
												</td>
											   </c:if>
											</tr>
											<tr>
										  		<c:if test="${	cityId >-1}">
										  		<c:if test="${isTitle}">
												<td align="right" style ="border: #FFFFFF" width="170">Название помещения</td>
												<td style ="border: #FFFFFF"><input type="text" name="title" value="${title}"
													maxlength="50" size="20"></td>
												</c:if>
												</c:if>
												<c:if test="${	cityId >-1}">
												<c:if test="${!isTitle}">
												<td align="right" style ="border: #FFFFFF" width="170">Название помещения</td>
												<td style ="border: #FFFFFF"><input type="text" name="title" 
													maxlength="50" size="20"></td>
												 </c:if>
												 </c:if>
											</tr>
											<tr>
												<c:if test="${	isTitle}">
												<td align="right" style ="border: #FFFFFF" width="170">Описание помещания</td>
												<td style ="border: #FFFFFF">
												<textarea class="form-control" rows="5" name="description" required="required" cols="70">
												</textarea>
											    </td>
												</c:if>
											</tr>
											<tr>
												<td style ="border: #FFFFFF"></td>
												<td style ="border: #FFFFFF"><button class="btn btn-primary btn-xs" type="submit">выбрать</button></td>
										    </tr>
								 </table>
							</form>
						</c:otherwise></c:choose>
					</div>
				 </div>
			   </div>
			</div>
		
			<div id="footer">
			     <div class="container">
			       <p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span> Александр Бондарев, 2014 г. </p>
			     </div>
		   </div>
		   
		 	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		   	<script src="./js/bootstrap.min.js"></script>  
	</body>
</html>