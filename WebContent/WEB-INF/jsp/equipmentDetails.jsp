<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Оборудование детально</title>
        <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/sticky-footer.css" rel="stylesheet">
        <link href="./css/my.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

		<body>
			<div id="wrap">
		    	<header>
						<nav class="navbar navbar-inverse navbar-static-top">
							<div class="navbar-header">
							    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							      <span class="sr-only">Toggle navigation</span>
							      <span class="icon-bar"></span>
							      <span class="icon-bar"></span>
							      <span class="icon-bar"></span>
							    </button>
							    <a class="navbar-brand" href="./index.action">Blink</a>
						  </div>
						  <div class="navbar-collapse collapse">
							    <ul class="nav navbar-nav">
								      <li ><a href="./project.action">Проекты</a></li>
								      <li class="active"><a href="./cars.action">Оборудование и машины</a></li>
								      <li><a href="./counselors.action">Консультанты</a></li>
								      <li><a href="./area.action">Помещения</a></li>
							      </ul>
							      <c:if test="${isAuthenticated}">
								     <div class="nav navbar-right" >
									      <ul class="nav navbar-nav">
											  <li>	      
													<div class="media">
														  <a class="pull-left" href="#">
														  <c:choose>
							  							 	<c:when test="${avatar != null}">
							  							 	<img class="media-object" src="./images/users/${avatar}" onerror="this.src ='./images/userDefault.jpg'"  width="50" height="50"/>	
							  							 	</c:when>
							  							 	<c:otherwise>
							  							 	<img class="media-object" src="./images/userDefault.jpg"  width="50" height="50"/>
							  							 	</c:otherwise>
				  							 	 	   	  </c:choose>
														  </a>
													</div>
											  </li>
											   <li class="dropdown">
									              <a href="counselorDetails.action" class="dropdown-toggle" data-toggle="dropdown">${userName}<b class="caret"></b></a>
									              <ul class="dropdown-menu">
									                <li><a href="./userRedact.action">Изменить данные</a></li>
									                <li><a href="./changePassword.action">Изменить пароль</a></li>
									                <li><a href="./changeAvatar.action">Изменить аватар</a></li>
									           	 </ul>
								               </li>
										      <li><a href="./signout.action" >Выход <span class="glyphicon glyphicon-log-out"></span></a></li>
									    </ul> 
									</div>
								</c:if>
							</div>
					   </nav>
				</header>
							
								
				<div class="container">
					 <div class="row">
						    <div class="col-md-2">
						    	<c:if test="${equipment == null}">
										<script  type="text/javascript">
								        	 location="./notFoundPage.action"
										</script>
								 </c:if>
						    	 <c:choose>
							  		 <c:when test="${isAuthenticated}">
							  		 
							   			<div class="panel panel-default">
							  				<div class="panel-heading">Мои объявления</div>
						
						   						<table class="table table-hover table-condensed">
													 <tr>
														  <td><a href="./personalProjects.action" >
									    					 <span class="badge pull-right">
									    					 ${homePageData.getProjectsCount()}
									    					 </span><span class="   glyphicon glyphicon-briefcase"></span>&#160;
														 	 Проекты
									    					</a></td>
													 </tr>
													 <tr>
														  <td><a href="./personalEquipments.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getEquipmentCount()}</span>
														 	Оборудование и машины
									    					</a></td>
													</tr>
													 <tr>
														  <td><a href="./personalAreas.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getAreasCount()}</span>
														 	Площади
									    					</a></td>
													 </tr>
													 <tr>
														  <td><a href="./personalCounselors.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getCounselorsCount()}</span>
														 	Консультации
									    					</a></td>
													</tr>
					   						</table>
						   				</div>
						   			
						   			
								   		<div class="panel panel-default">
									  		<div class="panel-heading">Мои сообщения</div>
								   		  		<table class="table table-hover table-condensed">
													<tr>
													     <td>
													   		<a href="./newPersonalMessage.action"><span class="glyphicon glyphicon-folder"></span>&#160;Новое сообщение</a>
													 	 </td>
													 </tr>
													 <tr>
													  	 <td>
													  	  	<a href="./inbox.action"><span class="badge pull-right">${homePageData.getMessagesUnread()}</span><span class="   glyphicon glyphicon-folder-close"></span>&#160;Входящие</a>
													     </td>
													</tr>
													<tr>
													  	 <td>
													  	   <a href="./outbox.action"><span class="glyphicon glyphicon-send"></span>&#160;Отправленные</a>
													  	 </td>
													</tr>
													<tr>
													  <td>
													  	  <a href="./deletedBox.action"><span class="glyphicon glyphicon-trash"></span>&#160;Удаленные</a>
													  </td>
													</tr>
								   			</table>
								   		</div>
						   		
							   		  </c:when>
					  				  <c:otherwise>
					  				  
					  				   <div class="panel panel-default">
										 <div class="panel-heading" align="center">Выполните вход</div>
										  		<table class="table table-hover table-condensed">
													<tr>
													     <td>
											        	<form method="post" class="form-signin">
														        <input type="text" name="email" class="form-control" placeholder="Email address" required autofocus>
														        <input type="password" name="password" class="form-control" placeholder="Password" required>
														        <button class="btn btn-xs btn-primary btn-block" type="submit">Sign in</button>
												       </form>
													</tr>
													 <tr>
														 <td>
													     	 <a href="./userRegistration.action" class="list-group-item">Регистрация</a>
													     </td>
											     	 </tr>
													<tr>
												   </tr>
											</table>
										</div>
					  				</c:otherwise>
					  			 </c:choose>
						  	</div>
					
						     <div class="col-md-8">
								<table class="table table-bordered table-striped">
									<tr>
										<td class="forTd" align="center">ID</td>
										<td class="forTd" align="center">СТРАНА</td>
										<td class="forTd" align="center">РЕГИОН</td>
										<td class="forTd" align="center">ГОРОД</td>
										<td class="forTd" align="center">НАЗВАНИЕ ОБОРУДОВАНИЯ </td>
										<td class="forTd" align="center">ДОБАВЛЕНО</td>
										<td class="forTd" align="center">ПРИГЛАСИТЬ В ПРОЕКТ</td>
							
									</tr>
									<tr>
										<td>${equipment.getId()}</td>
										<td>${equipment.getCountry().getCountryNameRu()}</td>
										<td>${equipment.getRegion().getRegionNameRu()}</td>
										<td>${equipment.getCity().getCityNameRu()}</td>
										<td>${equipment.getTitle()}</td>
										<td>${equipment.getCreationDate()}</td>
										<td>
											<c:choose>
								  		 		<c:when test="${isAuthenticated}">
													<a href="./inviteEquipment.action?equipmentId=${equipment.getId()}">Пригласить</a>
												</c:when>
												<c:otherwise>
													Необходима регистрация
												</c:otherwise>
											</c:choose>
										</td>
								</tr>
							</table>
								
							<table class="table table-hover table-condensed">
									<tr>
									  	<td>
										   	  <div class="media">
												  <a class="pull-left" href="#">
												    <img class="media-object" src="./images/users/${equipment.getUser().getAvatar()}" onerror="this.src ='images/userDefault.jpg'"  width="64" height="64">
												  </a>
												  <div class="media-body">
												    	<h5 class="media-heading"><dfn>Объявление опубликовал:</dfn> &#160;${equipment.getUser().getUsername()}<dfn>&#160; Дата:</dfn>&#160; ${equipment.getCreationDate()}</h5>
												  </div>
												  ${equipment.getDescription()}
											 </div>
							  		    </td>
								   </tr>
							</table>
							
							
						<div class="row">
							<div class="col-lg-6">
							    <div class="thumbnail">
						    		<c:choose>
						  		 		<c:when test="${imagesList.size()>0}">
									      <img src="./images/equipments/${imagesList.get(0).getFileName()}" width="400" height="50" />
									      <c:if test="${equipment.getUser().getId() == userId}">
									       		 <form method="POST">
							   		       			<button  class="btn btn-xs btn-danger btn-xs " type="submit" name="deleteId" value="${imagesList.get(0).getId()}"><span class="glyphicon glyphicon-trash"></span></button>
											    </form>
									      </c:if>
									     </c:when>
									     <c:otherwise>
											    <img data-src="./holder.js/400x300">
											    <c:if test="${equipment.getUser().getId() == userId}">
										 		<form method="POST" action="imagesUpload?equipmentId=${equipmentId}" enctype="multipart/form-data" >
									   		  		<input type="file" name="file" id="file" /> <br/>
							   		       			<button class="btn btn-xs btn-primary btn-block" type="submit" name="upload" id="upload">Загрузить</button>
											    </form>
										 	</c:if>
										 </c:otherwise>
									 </c:choose>
							 	</div>
						 	</div>
						 	
						 	<div class="col-lg-6">
							    <div class="thumbnail">
						    		<c:choose>
						  		 		<c:when test="${imagesList.size()>1}">
									      <img src="./images/equipments/${imagesList.get(1).getFileName()}"  data-alt="holder.js/400x300">
									      <c:if test="${equipment.getUser().getId() == userId}">
									        	<form method="POST" >
							           				<input type="hidden" name ="equipmentId" value ="${equipmentId}">
							   		       			<button class="btn btn-xs btn-danger btn-block" type="submit" name="deleteId" value="${imagesList.get(1).getId()}">Удалить</button>
											    </form>
									        </c:if>
									     </c:when>
									     <c:otherwise>
										    <img data-src="./holder.js/400x300">
										    <c:if test="${equipment.getUser().getId() == userId}">
											  <div class="caption">
											 		<form method="POST" action="./imagesUpload?equipmentId=${equipmentId}" enctype="multipart/form-data" >
										   		  		<input type="file" name="file" id="file" /> <br/>
								   		       			<button class="btn btn-xs btn-primary btn-block" type="submit" name="upload" id="upload">Загрузить</button>
													 </form>
										 		</div>
										 	</c:if>
										 </c:otherwise>
									 </c:choose>
							 	</div>
						 	</div>
						 </div>
					 </div>
				</div>
			</div>  
		</div>
						
		
			
	
		   <div id="footer" >
			     <div class="container">
			       	<p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span> Александр Бондарев, 2014 г.</p>
			     </div>
		   </div>
	
			
	  	  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	      <script src="./js/bootstrap.min.js"></script>	
	      <script src="./js/holder.js"></script>
    </body>
</html>