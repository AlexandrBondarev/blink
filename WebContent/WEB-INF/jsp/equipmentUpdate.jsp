<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Редактирование объявления</title>
        <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/sticky-footer.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    
	<div id="wrap">
	    <header>
				<nav class="navbar navbar-inverse navbar-static-top">
					<div class="navbar-header">
					    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					      <span class="sr-only">Toggle navigation</span>
					      <span class="icon-bar"></span>
					      <span class="icon-bar"></span>
					      <span class="icon-bar"></span>
					    </button>
					    <a class="navbar-brand" href="./index.action">Blink</a>
				  </div>
				  <div class="navbar-collapse collapse">
					    <ul class="nav navbar-nav">
						      <li ><a href="./project.action">Проекты</a></li>
						      <li class="active"><a href="./cars.action">Оборудование и машины</a></li>
						      <li><a href="./counselors.action">Консультанты</a></li>
						      <li><a href="./area.action">Помещения</a></li>
					      </ul>
					      <c:if test="${isAuthenticated}">
						     <div class="nav navbar-right" >
							      <ul class="nav navbar-nav">
									  <li>	      
										<div class="media">
											  <a class="pull-left" href="#">
											    <c:choose>
					  							 	<c:when test="${avatar != null}">
					  							 	<img class="media-object" src="./images/users/${avatar}" onerror="this.src ='./images/userDefault.jpg'"  width="50" height="50"/>	
					  							 	</c:when>
					  							 	<c:otherwise>
					  							 	<img class="media-object" src="./images/userDefault.jpg"  width="50" height="50"/>
					  							 	</c:otherwise>
	  							 	 	   	    </c:choose>
											  </a>
										</div>
								      </li>
								      <li class="dropdown">
							              <a href="./equipmentUpdate.action" class="dropdown-toggle" data-toggle="dropdown">${userName}<b class="caret"></b></a>
							              <ul class="dropdown-menu">
							                <li><a href="./userRedact.action">Изменить данные</a></li>
							                <li><a href="./changePassword.action">Изменить пароль</a></li>
							                <li><a href="./changeAvatar.action">Изменить аватар</a></li>
							              </ul>
						                </li>
								      <li><a href="./signout.action" >Выход <span class="glyphicon glyphicon-log-out"></span></a></li>
							    </ul> 
							</div>
						</c:if>
					</div>
			   </nav>
			</header>
			
		
					
							
			<div class="container">
					 <div class="row">
					    <div class="col-md-2">
					     	<c:choose>
						   		<c:when test="${isAuthenticated}">
								 	<div class="panel panel-default">
									  	<div class="panel-heading" align="center">Мои объявления</div>
									   		<table class="table table-hover table-condensed">
													<tr>
														  <td><a href="./personalProjects.action" >
									    					 <span class="badge pull-right">
									    					 ${homePageData.getProjectsCount()}
									    					 </span><span class="glyphicon glyphicon-briefcase"></span>&#160;
														 	 Проекты
									    					</a></td>
													 </tr>
													 <tr>
														  <td><a href="./personalEquipments.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getEquipmentCount()}</span>
														 	Оборудование и машины
									    					</a></td>
													</tr>
													 <tr>
														  <td><a href="./personalAreas.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getAreasCount()}</span>
														 	Площади
									    					</a></td>
													 </tr>
													 <tr>
														  <td><a href="./personalCounselors.action">
									    					 <span class="badge pull-right">
									    					 ${homePageData.getCounselorsCount()}</span>
														 	Консультации
									    					</a></td>
													</tr>
									   		</table>
							   		</div>
					   		
				   					  <div class="panel panel-default">
								  			 <div class="panel-heading" align="center">Мои сообщения</div>
								   		  		<table class="table table-hover table-condensed">
													<tr>
													     <td>
													   		<a href="./newPersonalMessage.action"><span class="glyphicon glyphicon-folder"></span>&#160;Новое сообщение</a>
													 	 </td>
													 </tr>
													 <tr>
													  	 <td>
													  	  	<a href="./inbox.action"><span class="badge pull-right">${homePageData.getMessagesUnread()}</span><span class="glyphicon glyphicon-folder-close"></span>&#160;Входящие</a>
													     </td>
													</tr>
													<tr>
													  	 <td>
													  	   <a href="./outbox.action"><span class="glyphicon glyphicon-send"></span>&#160;Отправленные</a>
													  	 </td>
													</tr>
													<tr>
													  <td>
													  	  <a href="./deletedBox.action"><span class="glyphicon glyphicon-trash"></span>&#160;Удаленные</a>
													  </td>
													</tr>
								   		 </table>
							   		</div>
			   		
				   				</c:when>
			  						  <c:otherwise>
				  						  <div class="panel panel-default">
									  			 <div class="panel-heading" align="center">Выполните вход</div>
										  			<table class="table table-hover table-condensed">
															<tr>
															     <td>
													        	<form method="post" class="form-signin">
																        <input type="text" name="email" class="form-control" placeholder="Email address" required autofocus>
																        <input type="password" name="password" class="form-control" placeholder="Password" required>
																        <button class="btn btn-xs btn-primary btn-block" type="submit">Sign in</button>
														     	 </form>
															</tr>
															 <tr>
																 <td>
															      <a href="./userRegistration.action" class="list-group-item">Регистрация</a>
															     </td>
													     	</tr>
															<tr>
														   </tr>
													</table>
												</div>
			  						</c:otherwise>
		  				 	</c:choose>
					  </div>
		
					  <div class="col-md-7">
					
						  <c:if test ="${equipment.getUser().getId() != userId}">
						  	<script type="text/javascript">
					        	 location="./notFoundPage.action"; 
							</script>
						  </c:if>
						  	<c:if test="${!isAuthenticated}">
									 <script type="text/javascript">
							        	 location="http:./signin.action"; 
									</script>
								</c:if>
						<c:choose>
								<c:when test="${!isAuthenticated}">
									 <script type="text/javascript">
							        	 location="./signin.action"; 
									</script>
								</c:when>
						<c:otherwise>
	
						<form method="post">
						
								<div style ="font-weight:800; border: #FFFFFF" align="center" >
								<h4>РЕДАКТИРОВАТЬ ОБЪЯВЛЕНИЕ</h4>
								</div>
							
								<table class="table table-hover table-condensed">
										<tr>
											<td align="right" style ="border: #FFFFFF" width="170">Страна</td>
											<td style ="border: #FFFFFF"><input class="form-control"  id="disabledInput" type="text" value="${equipment.getCountry().getCountryNameRu()}"
											maxlength="50" size="30" disabled></td>
										</tr>
										<tr>
											<td align="right" style ="border: #FFFFFF" width="170">Регион</td>
											<td style ="border: #FFFFFF"><input  class="form-control" id="disabledInput"  type="text" value="${equipment.getRegion().getRegionNameRu()}"
											maxlength="50" size="30" disabled></td>
										</tr>
										<tr>
											<td align="right" style ="border: #FFFFFF" width="170">Город</td>
											<td style ="border: #FFFFFF"><input class="form-control" id="disabledInput" type="text" value="${equipment.getCity().getCityNameRu()}"
											maxlength="50" size="30" disabled></td>
										</tr>
										<tr>
												<td align="right" style ="border: #FFFFFF" width="170">Название оборудования</td>
												<td style ="border: #FFFFFF"><input type="text" name="title" value="${equipment.getTitle()}"
												maxlength="50" size="30"></td>
										</tr>
										<tr>
												<td align="right" style ="border: #FFFFFF" width="170">Описание оборудования</td>
												<td style ="border: #FFFFFF"><textarea   name="description"  cols="70" rows="5">${equipment.getDescription()}</textarea></td>
										</tr>
							 </table>
							<div align="right" style="margin-bottom: 1em;">
		      					<button type="submit"  class="btn btn-primary btn-xs" > изменить</button>
					  		</div>
						</form>
						</c:otherwise></c:choose>
					</div>
				</div>
			</div>
		</div>
		
	   <div id="footer">
	     <div class="container">
	       <p class="text-muted credit"><span class="glyphicon glyphicon-copyright-mark"></span> Александр Бондарев, 2014 г. </p>
	     </div>
	   </div>
	   
	   <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
       <script src="./js/bootstrap.min.js"></script>
	
	</body>
</html>