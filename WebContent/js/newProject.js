function countrySelected() {
	var countrySelect = document.getElementById("countryId");
	var selectedCountryIndex = countrySelect.options[countrySelect.selectedIndex].value;
	$.ajax({
		// the URL for the request
		url : "./regionsList.action",

		// the data to send (will be converted to a query string)
		data : {
			countryId: selectedCountryIndex,
		},

		// whether this is a POST or GET request
		type : "POST",

		// the type of data we expect back
		dataType : "text",

		// code to run if the request succeeds;
		// the response is passed to the function
		success : function(text) {
			//alert(text);
			$("#regionId").html(text);
			//document.getElementById("cityId").innerHTML = str;
		},

		// code to run if the request fails; the raw request and
		// status codes are passed to the function
		error : function(xhr, status) {
			alert("Sorry, there was a problem!");
		},

		// code to run regardless of success or failure
		complete : function(xhr, status) {
			//alert("The request is complete!");
		}
	});
	
	document.getElementById("tdCity").style.display = 'none';
	document.getElementById("cityId").style.display = 'none';
	document.getElementById("title").style.display = 'none';
	document.getElementById("description").style.display = 'none';
	document.getElementById("submit").style.display = 'none';
	document.getElementById("tdRegion").style.display = '';
}

function regionSelected() {
	var regionSelect = document.getElementById("regionId");
	var selectedRegionIndex = regionSelect.options[regionSelect.selectedIndex].value;
	$.ajax({
		// the URL for the request
		url : "./citiesList.action",

		// the data to send (will be converted to a query string)
		data : {
			regionId: selectedRegionIndex,
		},

		// whether this is a POST or GET request
		type : "POST",

		// the type of data we expect back
		dataType : "text",

		// code to run if the request succeeds;
		// the response is passed to the function
		success : function(text) {
			//alert(text);
			$("#cityId").html(text);
			//document.getElementById("cityId").innerHTML = str;
		},

		// code to run if the request fails; the raw request and
		// status codes are passed to the function
		error : function(xhr, status) {
			alert("Sorry, there was a problem!");
		},

		// code to run regardless of success or failure
		complete : function(xhr, status) {
			//alert("The request is complete!");
		}
	});
	
	document.getElementById("tdCity").style.display = '';
	document.getElementById("cityId").style.display = '';
	document.getElementById("title").style.display = 'none';
	document.getElementById("description").style.display = 'none';
	document.getElementById("submit").style.display = 'none';
}

function citySelected() {
	document.getElementById("title").style.display = '';
	document.getElementById("description").style.display = '';
	document.getElementById("submit").style.display = '';
}



