function ready() {
	$.ajax({
		// the URL for the request
		url : "./application.action",

		// the data to send (will be converted to a query string)
		data : {
			countryId : "t",
		},

		// whether this is a POST or GET request
		type : "POST",

		// the type of data we expect back
		dataType : "text",

		// code to run if the request succeeds;
		// the response is passed to the function
		success : function(text) {
			//alert(text);
			$("#key").html(text);
		},

		// code to run if the request fails; the raw request and
		// status codes are passed to the function
		error : function(xhr, status) {
			alert("Sorry, there was a problem!");
		},

		// code to run regardless of success or failure
		complete : function(xhr, status) {
			// alert("The request is complete!");
		}
	});

	document.getElementById("key").style.display = '';
	document.getElementById("dander").style.display = 'none';
	document.getElementById("category").style.display = 'none';
	document.getElementById("key4").style.display = 'none';
	document.getElementById("submit").style.display = 'none';
}

function keySelected() {
	var key = document.getElementById("key");
	var keyIndex = key.options[key.selectedIndex].value;

	if (keyIndex == 4) {
		document.getElementById("key").style.display = '';
		document.getElementById("dander").style.display = 'none';
		document.getElementById("key4").style.display = '';
		document.getElementById("submit").style.display = '';
		document.getElementById("category").style.display = 'none';

	}

	if (keyIndex != 4) {

		$.ajax({

			url : "./showCategory.action",

			data : {
				key : keyIndex,
			},
			type : "POST",
			dataType : "text",

			success : function(text) {
			//	alert(text);
				$("#category").html(text);
			},

			error : function(xhr, status) {
				alert("Sorry, there was a problem!");
			},

			complete : function(xhr, status) {
			}
		});
		document.getElementById("key").style.display = '';
		document.getElementById("dander").style.display = 'none';
		document.getElementById("category").style.display = '';
		document.getElementById("submit").style.display = 'none';
		document.getElementById("key4").style.display = 'none';

	}
	
	
}
function categorySelected(){
		document.getElementById("key").style.display = '';
		document.getElementById("dander").style.display = 'none';
		document.getElementById("key4").style.display = '';
		document.getElementById("submit").style.display = '';
		document.getElementById("category").style.display = '';

}